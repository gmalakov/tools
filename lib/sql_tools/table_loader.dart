part of sqlTools;

class TableLoader {
  static bool buildMappers = true;

  Map<String,DefMapper> _mappers = new Map();
  Map<String, dynamic> tMap = null;
  bool _buildMappers;

  TableLoader(String fName, {String fPath:"."}) {
    _buildMappers = buildMappers;
    try {
      String source = new File(fPath+"/"+fName).readAsStringSync();
      var tmpMap = Yaml.readYaml(source);
      tMap = new Map();
      tmpMap.forEach((n,v) => tMap[n.toString()] = v);
      walkTab();
    } catch (err) { LogError(err.toString()); }
  }

  void walkTab() {
    //Name | inner map
    /*
     * parent = tName
     * fields: .....
     * defKey: .....
     * keys: .....
     * autoInc: true/false
     */

    tMap.forEach((String name, dynamic val) {
      try { val = val as Map; } catch (err) { LogError("Error casting val as Map: "+err.toString()); val = new Map<String, dynamic>(); }
      String keys;
      bool autoInc;
      String defKey;

      if (val["keys"] == null) keys = "";
      else keys = val["keys"] as String;

      //Auto increment will be on by default
      if (val["autoInc"] == null) autoInc = true;
      else autoInc = val["autoInc"] as bool;

      if ((val["defKey"] == null) || (val["defKey"] == "")) defKey = "id";
      else defKey = val["defKey"] as String;

      if ((val["fields"] != null) && ((val["fields"] as String).isNotEmpty) && _buildMappers) {
        _mappers[name] = new DefMapper(val["fields"] as String,name, defKey: defKey,
            keys:keys, autoInc: autoInc);

        //Create an empty table if not exists in database
        _mappers[name].initTable();

      } else if (_buildMappers) LogError("Error loading table $name!");
    });
  }

  void setMapper(String fields, String name, String defKey, String keys, bool autoInc) {
    _mappers[name] = new DefMapper(fields,name, defKey: defKey,
        keys:keys, autoInc: autoInc);
  }

  DefMapper getMapper(String name) {
    return _mappers[name];
  }

  static TableLoader _tLoad = new TableLoader("tables.yaml");
  static TableLoader get instance => _tLoad;
}
