part of sqlTools;

enum SqlType { mysql, postgre }

class TableBuilder {
  List<String> _fNames = new List();
  List<String> get fNames => _fNames;
  List<String> _fTypes = new List();
  List<String> get fTypes => _fTypes;
  List<String> _keys = new List();
  String pKey;
  String tName;
  SqlType sqType;
  bool autoInc;
  DefMapper _dm;

  static const String statementPre = "create table if not exists";

  static const Map<String, String> typesPostgre = const {
    "datetime": "timestamp",
    "int": "int",
    "num": "double precision", //"numeric"
    "string": "text",
    "list": "jsonb",
    "map": "jsonb",
    "autoinc": "serial",
    "bool": "boolean"
  };

  static const Map<String, String> typesMysql = const {
    "datetime": "datetime",
    "int": "mediumint",
    "num": "float",
    "string": "text",
    "map": "text",
    "list": "text",
    "autoinc": "bigint unsigned auto_increment",
    "bool": "boolean"
  };

  static String getValue(String name, SqlType sqType) {
    String ret = "text";
    switch (sqType) {
      case SqlType.mysql: ret = typesMysql[name];
       break;
      case SqlType.postgre: ret = typesPostgre[name];
       break;
    }
    return ret ?? "text";
  }

  TableBuilder(Map inMap, this.pKey, this.tName, this.autoInc,  { String keys: "", SqlType sqType }) {
    //Get SqlDriver if sqltype is not set
    sqType ??= QExecutor.sqlDriver;
    this.sqType = sqType;
    //Init keys list

    List<String> lkeys = keys.split(" ");
    for (int i = 0; i < lkeys.length; i++)
     if (lkeys[i] != "") this._keys.add(lkeys[i].trim());

    parseMap(inMap, autoInc, sqType: sqType);
  }

  void parseMap(Map inMap, bool autoInc, { SqlType sqType }) {
    //Set to sqldriver if not set
    sqType ??= QExecutor.sqlDriver;

    //inMap -> Map to parse
    // pKey -> Primary key name
    // autoInc -> is primary key auto increment

    for(dynamic name in inMap.keys)
     if (inMap[name] != null) {
       dynamic value = inMap[name as String]; //Get value

       //Find String manifestation of type
       String vtype = "text";
       if (value is DateTime) vtype = "datetime";
       if (value is num) vtype = "num";
       if (value is int) vtype = "int";
       if (value is Map) vtype = "map";
       if (value is List) vtype = "list";
       if (value is bool) vtype = "bool";
       if (value is  String) {
         vtype = "string";
         if (value == "now()") vtype = "datetime";
       }

         if (vtype != null) { //sql type is found
           //Find sql type value and add it to list
           _fNames.add(name as String); //add name to list
           _fTypes.add(getValue(vtype, sqType));
         }
       }


    bool pKeyFound = false;
    for (int i = 0; i < _fNames.length; i++)
        if (_fNames[i] == pKey) {
          pKeyFound = true; //Set found flag
          if (autoInc) _fTypes[i] = getValue("autoinc", sqType); //if auto increment set proper type
        }

    //If not found in map add it
    if (!pKeyFound) {
      _fNames.add(pKey); //Primary key name
      _fTypes.add(getValue("autoinc", sqType)); //Primary key type
    }

  }

  String get query {
    String ret = "";
    for (int i = 0; i < _fNames.length;i++) {
      if (ret != "") ret += ", "; //Add , if not empty
      ret += _fNames[i]+" "+_fTypes[i];
    }

    ret = statementPre+" $tName("+ret+", primary key($pKey)";
    //Add keys

    if (sqType == SqlType.mysql)
      for (int i = 0; i < _keys.length; i++) ret += ", key("+_keys[i]+")";

    //Postgres >>> CONSTRAINT <field>_unique_<tname> UNIQUE (<field>)
    if (sqType == SqlType.postgre)
      for (int i = 0; i < _keys.length; i++)
        if (_keys[i] != pKey)
          ret += ", constraint "+_keys[i]+"_unique_"+tName+" unique ("+
              _keys[i]+")";

    ret += ")"; //Already set for postgre try
    if (sqType == SqlType.mysql) {
      ret += " CHARACTER SET=";
      if (QCreator.noConv) ret+= "utf8";
        else ret+= "latin1";
    }
    ret += ";";

    if (sqType == SqlType.postgre)
       for (int i = 0; i < _keys.length; i++)
         ret += "create index ${tName}_fkey$i on $tName using btree (\"${_keys[i]}\");";

    return ret;
  }

  String get fields {
    String ret = "";
    for (int i = 0; i < _fNames.length;i++) {
      if (ret != "") ret += ", ";
      ret += _fNames[i];
      ret += ":"+_fTypes[i];
    }

    return ret;
  }

  List<String> get lkeys => _keys;
  String get keys {
    String res = "";
    for (int i = 0; i < _keys.length;i++) {
      if (res != "") res += ", ";
      res += _keys[i];
    }

    return res;
  }

  DefMapper getMapper() {
    if (_dm != null) return _dm;
    _dm = new DefMapper(fields, tName,keys: keys, autoInc: autoInc, defKey: pKey);
    return _dm;
  }

  String toString() => query;

}