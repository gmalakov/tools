part of sqlTools;

//SQL Queries
class QCreator {
  static const String zeroDate = '1970-01-01';
  static const String zeroDateTime = '1970-01-01 00:00:00';

  static bool noConv = false;

  List <String> _what = new List();
  List <String> sName = new List();
  List <String> _orWhere = new List();
  List <String> _andWhere = new List();
  Map<String,String> _dictionary = new Map();

  Map<String,String> _set = new Map();

  List<String> get what => _what;
  List<String> get orWhere => _orWhere;
  List<String> get andWhere => _andWhere;
  Map<String,String> get gset => _set;

  int limit = 0;
  int page = 1;

  //Recursively escpe json values for postgre
  dynamic jsonRmUnSafe(dynamic value) {
    //Clone object to avoid escaping multiple times and accumulating shit
    var valueOut = cloneAny<dynamic>(value);
    if (valueOut is List)
      for (int i = 0; i < valueOut.length; i++) {
        if (valueOut[i] is String) valueOut[i] = rmUnSafe(valueOut[i] as String).replaceAll("()", "");
        if (valueOut[i] is Map) valueOut[i] = jsonRmUnSafe(valueOut[i]).cast<String,dynamic>();
        if (valueOut[i] is List) valueOut[i] = jsonRmUnSafe(valueOut[i]).cast<dynamic>();
      }

    if (valueOut is Map)
      for (dynamic key in valueOut.keys) {
        if (valueOut[key] is String) valueOut[key] = rmUnSafe(valueOut[key] as String).replaceAll("()", "");
        if (valueOut[key] is Map) valueOut[key] = jsonRmUnSafe(valueOut[key]).cast<String,dynamic>();
        if (valueOut[key] is List) valueOut[key] = jsonRmUnSafe(valueOut[key]).cast<dynamic>();
      }

    return valueOut;
  }

  void addSet(String name, dynamic value, {String type : "text"}) {
    if ((type == "map") || (type == "list")) {
      //Copy to another val
      value = jsonRmUnSafe(value);

      if (GenMapper.sqType == SqlType.mysql) {
          type = "text";
          _set[rmUnSafe(name)] = rmUnSafe(json.encode(value));
        }
       else _set[rmUnSafe(name)] = json.encode(value);
      }
     else {
      //Mysql date problems
      if (value == '3940-02-03 04:00:00' || value == '0000-00-00 00:00:00') value = '';

      if ((value == null || value == '' || value == 'null') && type == 'date') value = QCreator.zeroDate;
      if ((value == null || value == '' || value == 'null') && type == 'datetime') value = QCreator.zeroDateTime;

      _set[rmUnSafe(name)] = rmUnSafe(value.toString());
     }
    _dictionary[name] = type; //Set dictionary type
  }

  void clear() {
    _what.clear();
    _orWhere.clear();
    _andWhere.clear();
    _set.clear();
    limit = 0;
    page = 1;
  }

  void set addWhat(String what) {
    what = what.trim();
    int idx = what.indexOf(":");

    String type;
    if (idx > -1) {
      type = what.substring(idx+1, what.length);
      what = what.substring(0, idx);
      what = what.trim();
      type = type.trim().toLowerCase(); //Type to lower case
    }

    //Split by _ - get short name if exists (remove as statement)
    List<String> names = what.replaceAll(" as ", " ").split(" ");
    String name = "";
    if (names.length > 1) name = names[1];
    else name = names[0];

    //Add name + type to dictionary
    if (idx > -1)  _dictionary[name] = type;

    //Add alias name/short name to list
    sName.add(name);

    _what.add(what);
  }

  void set addAndWhere(String where) => _andWhere.add(where);
  void set addOrWhere(String where) => _orWhere.add(where);

  String getType(String val) {
    String ret;
    (_dictionary[val] == null) ? ret = "text" : ret = _dictionary[val];
    return ret;
  }

  String addElem(List<String> elem) {
    String retStr = "";
    elem.forEach((elem) => retStr += elem+",");
    //Remove last , if string not empty
    if (retStr != "") retStr = retStr.substring(0,retStr.lastIndexOf(","));
    return retStr;
  }

  String addWhere(String clause, List<String> elem) {
    String retStr = "";
    elem.forEach((elem) => retStr += clause+"("+elem+")");
    return retStr;
  }

}
