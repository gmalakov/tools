part of sqlTools;

class SQLQueue {
  String sql;
  List<String> par;
  Function callBack;

  SQLQueue(this.sql, this.par, this.callBack);
}

abstract class QExecutorPrototype<E> {
  static const TimerPeriod = const Duration(minutes: 10);

  bool initPool();

  Future<bool> remove();

  Future<List> select(SelectCreator sC, ErrFunc onError, [List<String> par]);

  Future<int> update(QCreator qC, ErrFunc onError, [List<String> par]);

  Future<E> query(String sql, ErrFunc onError, [List<String> par]) async {
    //Return null on empty string
    if ((sql == null) || (sql.isEmpty)) return null;

    E res;
    sql = sql.trim();

    //Disallow multi operator for update, insert, select operations due
    //lexical problems 'dasdsa;dsdasda' and so on
    bool allowMulti = false;
    String operator = sql.substring(0, sql.indexOf(" ")).toLowerCase();
    if (operator != "update" && operator != "insert" && operator != "select")
      allowMulti = true;

    if (sql.indexOf(";") < sql.length - 1 && allowMulti) {
      List<String> sqlData = sql.split(";");
      //Execute every statement separately
      for (int i = 0; i < sqlData.length; i++) {
        String curSql = sqlData[i].trim();
        if (curSql.isNotEmpty) {
          curSql += ";"; //Mainatain SQL syntax
          res = await _query(sqlData[i], onError, par);
        }
      }
    } else
      res = await _query(sql, onError, par);

    return res;
  }

  Future<E> _query(String sql, ErrFunc onError, [List<String> par]);
}

class QExecutorPGRe extends QExecutorPrototype<List<List<dynamic>>> {
  static Map vars;
  static const maxConn = 3;

  PgPool _pool;

  QExecutorPGRe() {
//      _uri = "postgres://"+vars["pgreUser"].toString()+":"+
//         vars["pgrePass"].toString()+"@"+vars["pgreHost"].toString()+":"+
//         vars["pgrePort"].toString()+"/"+vars["pgreDb"].toString();

    initPool();
  }

  bool initPool() {
    //Pool(this._url, this._port, this._dbName, this._user, this._password, [ this.maxConn = 3 ]);
    if (_pool == null) {
      PgPool backPool;
      if (vars['backup'] != null) {
        var varsBack = vars['backup'];
        backPool = new PgPool(
            varsBack["pgreHost"].toString(),
            varsBack["pgrePort"] as int,
            varsBack["pgreDb"].toString(),
            varsBack["pgreUser"].toString(),
            varsBack["pgrePass"].toString(),
            maxConn);
      }

      _pool = new PgPool(
          vars["pgreHost"].toString(),
          vars["pgrePort"] as int,
          vars["pgreDb"].toString(),
          vars["pgreUser"].toString(),
          vars["pgrePass"].toString(),
          maxConn,
          backPool);
    }
    return true;
  }

  Future<bool> remove() async {
    await _pool.stop();
    return true;
  }

  Future<List<List<dynamic>>> _query(String sql, ErrFunc onError,
      [List<String> par]) async {
    List<String> words = sql.split(" ");
    List<String> labels = [];

    //Find substitution labels
    for (int i = 0; i < words.length; i++)
      if (words[i].isNotEmpty && words[i][0] == "@")
        labels.add(words[i].substring(1)); //Remove @

    Map<String, dynamic> subPar;
    if (par != null) {
      //Remove substitution types from labels
      subPar = new Map();
      if (par.length == labels.length) //We have enough parameters ??
        for (int i = 0; i < labels.length; i++) {
          int idx = labels[i].indexOf(":");
          if (idx > -1) labels[i] = labels[i].substring(0, idx);

          //Create substitution map
          subPar[labels[i]] = par[i];
        }
    }

    return await _pool.query(sql, onError, subPar);
  }

  Future<List> select(SelectCreator sC, ErrFunc onError,
      [List<String> par]) async {
    List<List<dynamic>> lRes =
        await (await query(sC.query, onError, par))?.toList();
    return lRes;
  }

  Future<int> update(QCreator qC, ErrFunc onError, [List<String> par]) async {
    bool isInsert = (qC is InsertCreator);
    String sql;
    if (isInsert)
      sql = (qC as InsertCreator).query;
    else
      sql = (qC as UpdateCreator).query;

    List<List<dynamic>> res = await query(sql, onError, par);

    //If is and insert operation and is defined returning id then return right id
    if (isInsert) {
      if (res != null && res.length > 0)
        try {
          List<dynamic> row = await res.first; //Get first row and fetch id;
          String retId = (qC as InsertCreator).retId;
          if ((retId != null) && (retId.isNotEmpty)) if (row.length == 0)
            return 0;
          else
            return row.last
                as int; //Get last element (should be the returning id)
        } catch (err) {
          String sql = (qC as InsertCreator).query;
          LogError("Error inserting row:" + err.toString() + "\n" + sql);
        }
    }

    return 0;
  }
}

class QExecutorMysql extends QExecutorPrototype<mysql.StreamedResults> {
  MyPool pool = null;

  static Map vars; //Must set variables to map

  bool initPool() {
    if (vars == null) return false;

    if (pool != null) {
      try {
        pool.stop();
      } catch (err) {
        //Using older version sqljocky
        // try { pool.close(); } catch(err) { //Error closing connections }
      }
      pool = null;
    }
    try {
      MyPool backPool;
      if (vars['backup'] != null) {
        var varsBack = vars['backup'];
        backPool = new MyPool(
            varsBack["sqlhost"] as String,
            varsBack["sqlport"] as int,
            varsBack["sqldb"] as String,
            varsBack["sqluser"] as String,
            varsBack["sqlpass"] as String,
            5);
      }

      pool = new MyPool(
          vars["sqlhost"] as String,
          vars["sqlport"] as int,
          vars["sqldb"] as String,
          vars["sqluser"] as String,
          vars["sqlpass"] as String,
          5,
          backPool);
    } catch (err) {
      LogError("SQL сървъра не е намерен! " + err.toString());
      pool = null;
      return false;
    }

    return true;
  }

  Future<mysql.StreamedResults> _query(String sql, ErrFunc onError,
          [List<String> par]) =>
      pool
          .prepareExecute(sql, onError, par)
          .catchError((dynamic err, dynamic trace) async {
        LogError("SQL Error: $err \n $trace \n $sql");
        if (onError != null) await onError(err.toString());
      });

  Future<List> select(SelectCreator sC, ErrFunc onError,
      [List<String> par]) async {
    List<dynamic> oL;
    try {
      mysql.StreamedResults res = await query(sC.query, onError, par);
      oL = await res.toList();
    } catch (err) {
      print('Select:' + err.toString() + '\n' + sC.query);
    }
    return oL;
  }

  Future<int> update(QCreator qC, ErrFunc onError, [List<String> par]) async {
    //Is insert operation
    bool isInsert = (qC is InsertCreator);
    String sql;
    if (isInsert)
      sql = (qC as InsertCreator).query;
    else
      sql = (qC as UpdateCreator).query;

    mysql.StreamedResults res = await query(sql, onError, par);
    if ((isInsert) && (res != null))
      return res.insertId;
    else
      return 0;
  }

  Future<bool> remove() async {
    await pool.stop();
    return true;
  }

  QExecutorMysql() {
    initPool();
  }
}

class QExecutor extends QExecutorPrototype<dynamic> {
  //Init default driver to be mysql
  static SqlType sqlDriver = SqlType.mysql;

  static QExecutor _qexec;

  static QExecutor get instance {
    if (_qexec == null) _qexec = new QExecutor(drv: sqlDriver);
    return _qexec;
  }

  //Remove instance
  static void reset() {
    _qexec = null;
  }

  static Map vars;
  SqlType _drv;
  QExecutorMysql _mysqldrv;
  QExecutorPGRe _postgredrv;

  //Needed to mysql QExecutor backwards compatibility
  //mysql.ConnectionPool get pool => _mysqldrv.pool;
  //Needed for postgresql compatibility
  //Future<pgre.Connection> get pgConn => _postgredrv.getConn;

  QExecutor({SqlType drv}) {
    drv ??= QExecutor.sqlDriver;

    _drv = drv; //Save witch driver we use
    //Set datamap to drivers
    QExecutorMysql.vars = vars;
    QExecutorPGRe.vars = vars;

    switch (_drv) {
      case SqlType.mysql:
        _mysqldrv = new QExecutorMysql();
        break;
      case SqlType.postgre:
        _postgredrv = new QExecutorPGRe();
        break;
    }
  }

  Future<bool> remove() {
    switch (_drv) {
      case SqlType.mysql:
        return _mysqldrv.remove();
        break;
      case SqlType.postgre:
        return _postgredrv.remove();
        break;
    }
    return new Future.value(false);
  }

  Future<List> select(SelectCreator sC, ErrFunc onError, [List<String> par]) {
    switch (_drv) {
      case SqlType.mysql:
        return _mysqldrv.select(sC, onError, par);
        break;
      case SqlType.postgre:
        return _postgredrv.select(sC, onError, par);
        break;
    }
    return null;
  }

  Future<int> update(QCreator qC, ErrFunc onError, [List<String> par]) {
    switch (_drv) {
      case SqlType.mysql:
        return _mysqldrv.update(qC, onError, par);
        break;
      case SqlType.postgre:
        return _postgredrv.update(qC, onError, par);
        break;
    }
    return null;
  }

  Future<dynamic> _query(String sql, ErrFunc onError, [List<String> par]) {
    switch (_drv) {
      case SqlType.mysql:
        return _mysqldrv.query(sql, onError, par);
        break;
      case SqlType.postgre:
        return _postgredrv.query(sql, onError, par);
        break;
    }
    return null;
  }

  bool initPool() {
    switch (_drv) {
      case SqlType.mysql:
        return _mysqldrv.initPool();
        break;
      case SqlType.postgre:
        return _postgredrv.initPool();
        break;
    }
    return false;
  }
}
