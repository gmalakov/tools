part of sqlTools;

class DefMapper extends GenMapper {
  String defKey = "id";
  String _tName = "tmpTable";
  List<String> _keys = [];
  bool _autoInc = true;

  void set addKey(String key) {
    _keys.add(key);
  }

  void clearKeys() => (_keys.clear());

  void fillKeys(String keys) {
    if ((keys == null) || (keys == "")) return;

    //keys = keys.toLowerCase(); //wont be lowercase to not lose map parameters
    List<String> lkeys = keys.split(",");
    lkeys.forEach((key) {
      key = key.trim();
      addKey = key;
    });
  }

  DefMapper(String fields, String tName,
      {String defKey: "id",
      String keys: "",
      bool autoInc: true,
      bool init: false}) {
    this.fields = fields;
    if ((tName != null) && (tName != "")) _tName = tName;
    if (defKey != null) this.defKey = defKey;
    _autoInc = autoInc;

    fillKeys(keys);
    if (init) initTable();
  }

  void initTable() => sInitTable(_tName, _fNames, defKey, _keys, _autoInc);

  static void sInitTable(String tName, Map<String, String> fNames,
      String defKey, List<String> _keys, bool autoInc,
      {ErrFunc onError}) {
    String crCmd = "create table if not exists $tName (";
    fNames.forEach((n, v) {
      crCmd += "$n $v";
      if ((defKey == n) && (v.indexOf("int") > -1) && (autoInc))
        crCmd += " auto_increment";
      crCmd += ", ";
    });

    //Add primary key
    crCmd += "primary key($defKey), ";
    //Add subsequent keys
    _keys.forEach((key) => crCmd += "key ($key), ");

    crCmd = crCmd.trim();
    //Delete last ,
    crCmd = crCmd.substring(0, crCmd.length - 1);

    crCmd += ")";
    if (QExecutor.sqlDriver == SqlType.mysql) {
      crCmd += " CHARACTER SET=";
      if (QCreator.noConv)
        crCmd += "utf8";
      else
        crCmd += "latin1";
    }
    crCmd += ";";

    QExecutor.instance.query(crCmd, onError, <String>[]);
  }

  Future<bool> saveDump(List<Map> data,
      {int fraction: 2, ErrFunc onError}) async {
    int limit = 100;
    if (data == null) return false;
    int start = 0;
    int end = data.length;

    while (start < end) {
      InsertCreatorMulti iM = new InsertCreatorMulti()..into = _tName;
      if ((start + limit) > end) limit = end - start; //Adjust limit
      for (int i = start; i < start + limit; i++) {
        InsertCreator iC = new InsertCreator(defKey)..into = _tName;

        _fNames.forEach((n, v) {
          if ((data[i][n] is Map) || (data[i][n] is List))
            iC.addSet(n, data[i][n], type: "map");
          else {
            String dSet;

            if (((data[i][n] is num) || (data[i][n] is double)) &&
                (data[i][n] is! int))
              dSet = (data[i][n] as num).toStringAsFixed(fraction);
            else
              dSet = data[i][n].toString();

            //Get field type is Date ???
            String iType = (_fNames[n] ?? "").toLowerCase();
            String type = 'text';
            if (iType.indexOf('date') > -1) type = 'date';
            if (iType.indexOf('datetime') > -1) type = 'datetime';
            if (iType.indexOf('timestamp') > -1) type = 'datetime';

            //Add data set to set clause
            iC.addSet(n, dSet, type: type);
          }
        });

        iM.addRow(iC);
      }

      //print(iM.query);
      await QExecutor.instance.query(iM.query, onError);
      start += limit;
    }
    return true;
  }

  Future<int> saveData(Map data,
      {String andWhere,
      bool zeroNull: true,
      bool checkUpdate: false,
      bool forceInsert: false,
      int fraction: 2, ErrFunc onError }) {
    Completer<int> comp = new Completer();

    Future<int> iSaveData(
        Map data, String andWhere, bool zeroNull, bool forceInsert,
        ErrFunc onError) async {
      int cId = data[defKey] as int;
      if (cId == null) cId = 0;

      QCreator mQ;
      if (((cId > 0) || (andWhere != null)) && (!forceInsert)) {
        //Update creator
        mQ = new UpdateCreator();
        (mQ as UpdateCreator).addWhat = _tName;

        //if andWHere is set do not add defKey = cId search pattern
        if (andWhere == null)
          (mQ as UpdateCreator).addAndWhere = "$defKey = $cId";
        else
          (mQ as UpdateCreator).addAndWhere = andWhere;
      } else {
        //Insert creator

        //If insert is and postgresql insert then we need a returning id insted of insertId
        String retId;
        if (GenMapper.sqType == SqlType.postgre) retId = defKey;

        mQ = new InsertCreator(retId);
        (mQ as InsertCreator)..into = _tName;

        //Inserting with previous id and forceInsert turned on!
        //Insert id too
        if (cId > 0) (mQ as InsertCreator).addSet(defKey, cId);
      }

      //Populate query - insert only fields set in _fNames
      _fNames.forEach((n, v) {
        // inf andWhere is set do not add id=0 key

        //convert null fields to empty fields if zeroNull is set (default yes)
        if ((data[n] == null) && (zeroNull)) data[n] = "";

        //id data[n] is not null add it to set
        //if data[n] is default key and autoInc = true -> dont add data to set (it's automatically  set)
        //n != defKey || !_autoInc -> name is not default key or auto increment is not set -> primary key
        //must be set
        if (((n != defKey) || (!_autoInc)) && (data[n] != null)) {
          if ((data[n] is Map) || (data[n] is List))
            mQ.addSet(n, data[n], type: "map");
          else {
            //Recalc number precision if needed here
            String dSet;

            if (((data[n] is num) || (data[n] is double)) && (data[n] is! int))
              dSet = (data[n] as num).toStringAsFixed(fraction);
            else
              dSet = data[n].toString();

            //Get field type is Date ???
            String iType = (_fNames[n] ?? "").toLowerCase();
            String type = 'text';
            if (iType.indexOf('date') > -1) type = 'date';
            if (iType.indexOf('datetime') > -1) type = 'datetime';
            if (iType.indexOf('timestamp') > -1) type = 'datetime';

            //Add data set to set clause
            mQ.addSet(n, dSet, type: type);
          }
        }
      });

      int res = await QExecutor.instance.update(mQ, onError, <String>[]);
      if ((cId == 0) && (mQ is InsertCreator))
        cId = res; //If we'e just inserted a row then get the row id

      return cId;
    }

    if (checkUpdate) {
      //if default key is null or 0 -> insert new value
      getData(data[defKey] as int, newMap: false, andWhere: andWhere)
          .then((Map oData) {
        bool insert = true;
        //Check if record exists
        if (oData != null) insert = false;
        iSaveData(data, andWhere, zeroNull, insert, onError).then((int i) {
          comp.complete(i);
        });
      });
    } else
      iSaveData(data, andWhere, zeroNull, forceInsert, onError).then((int i) {
        comp.complete(i);
      });

    return comp.future;
  }

  Future<String> delWhere(String val, String fName,
      {String method: "=", ErrFunc onError}) {
    Completer<String> comp = new Completer();
    if ((fName == null) || (fName == "")) {
      comp.complete("Error! field name missing!");
      return comp.future;
    }

    if ((val == null) || (val == "")) {
      comp.complete("Error! field value missing!");
      return comp.future;
    }

    String qry = "delete from $_tName where $fName $method $val;";

    QExecutor.instance
        .query(qry, onError)
        .then((dynamic res) => comp.complete("OK"))
        .catchError((dynamic err) async {
      comp.complete("Error: " + err.toString());
      if (onError != null) await onError(err.toString());
      comp.complete(err.toString());
      QExecutor.instance.initPool();
      LogError("Error:" + err.toString() + " -> " + qry);
    });

    return comp.future;
  }

  Future<String> delKey(int id) => delWhere(id.toString(), defKey);

  Future<Map> getData(int id,
      {bool newMap: true, String andWhere, ErrFunc onError}) {
    Completer<Map> comp = new Completer();
    SelectCreator sQ = new SelectCreator();
    sQ.addFrom = _tName;

    List<String> names = [];
    _fNames.forEach((n, v) {
      sQ.addWhat = n;
      names.add(n);
    });

    if ((andWhere != null) && (andWhere != "")) sQ.addAndWhere = andWhere;
    if ((id != null) && (id > 0)) sQ.addAndWhere = "$defKey = $id";

    QExecutor.instance.select(sQ, onError).then((List res) {
      if (res.length > 0)
        comp.complete(parseRow(res[0], names));
      else if (newMap)
        comp.complete(new Map<String, dynamic>());
      else
        comp.complete(null);
    }).catchError((dynamic err) async {
      QExecutor.instance.initPool();
      LogError("Error: " + err.toString() + "\n" + sQ.query);
      if (onError != null) await onError(err.toString());
      if (newMap)
        comp.complete(<String, dynamic>{});
      else
        comp.complete(null);
    });

    return comp.future;
  }

  Future<List<Map>> findKey(List<String> keys) {
    SelectCreator sQ = new SelectCreator();
    sQ.addFrom = _tName;

    List<String> names = [];
    _fNames.forEach((n, v) {
      sQ.addWhat = n + ":" + _fNames[n];
      names.add(n);
    });

    keys.forEach((el) => sQ.addAndWhere = el);
    return GenMapper.select(sQ);
  }

  Future<List<Map>> findBy(String name, String val, {String method: "="}) {
    //Remove unsafe characters
    name = rmUnSafe(name);
    val = rmUnSafe(val);

    SelectCreator sQ = new SelectCreator();
    sQ.addFrom = _tName;

    List<String> names = [];
    _fNames.forEach((n, v) {
      sQ.addWhat = n + ":" + _fNames[n];
      names.add(n);
    });

    if ((name != null) && (name != "") && (val != null) && (val != ""))
      sQ.addAndWhere = "$name $method '$val'";

    return GenMapper.select(sQ);
  }

  Future<List<Map>> findAll() {
    return findBy("", "");
  }
}
