part of sqlTools;

class AlterBuilder {
  static const pre = "alter table";
  static const add = "add column";
  static const chg = "alter column"; // .. type $type
  static const drop = "drop column";

  String _table;
  List<String> _newFields = new List();
  List<String> _newTypes = new List();
  List<String> _chgFields = new List();
  List<String> _chgTypes = new List();
  List<String> _dropFields = new List();

  AlterBuilder(this._table);

  void addField(String name, String type) {
    //Reset to text if type is not recognized
    if (TableBuilder.typesMysql.keys.contains(type) == false) type = "text";
    _newFields.add(name);
    _newTypes.add(type);
  }

  void changeField(String name, String type) {
    //Reset to text if type is not recognized
    if (TableBuilder.typesMysql.keys.contains(type) == false) type = "text";
    _chgFields.add(name);
    _chgTypes.add(type);
  }

  void dropField(String name) => _dropFields.add(name);

  String get adds {
    String retS = "";
    for (int i = 0; i < _newFields.length; i++) {
      if (retS.isNotEmpty) retS += ", ";
      retS += add + " " + _newFields[i] + " " +
          TableBuilder.getValue(_newTypes[i], QExecutor.sqlDriver);
    }

    return retS;
  }

  String get changes {
    String retS = "";
    for (int i = 0; i < _chgFields.length; i++) {
      if (retS.isNotEmpty) retS += ", ";
      retS += chg + " " + _chgFields[i] + " type " +
          TableBuilder.getValue(_chgTypes[i], QExecutor.sqlDriver);
    }

    return retS;
  }

  String get drops {
    String retS = "";
    for (int i = 0; i < _dropFields.length; i++) {
      if (retS.isNotEmpty) retS += ", ";
      retS += drop + " " + _dropFields[i];
    }

    return retS;
  }

  String get query {
    String retS = ([adds, changes, drops]
      ..removeWhere((el) => el.isEmpty)).join(", ");

    //Build query -> will return empty string if nothing to do
    if (retS.isNotEmpty) retS = pre + " " +_table+" "+retS+";";

    return retS;
  }

  String toString() => query;

}
