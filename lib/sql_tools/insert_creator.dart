part of sqlTools;

class InsertCreator extends QCreator {
  String into = "";
  String retId;

  void clear() {
    super.clear();
    into = "";
  }

  InsertCreator([this.retId]);

  List<String> genSetList(Map<String,String> cSet) {
    String retStr = "("; String ret2Str = "(";

    cSet.forEach((name,value) {
      retStr += name+",";

      //no '' if statement is a function
      //String type = getType(name);
      if (value.indexOf("()") > -1) ret2Str += value+",";
      else ret2Str += "'"+value+"',";
    });

    try {
      if ((retStr != "") && (retStr.lastIndexOf(',') > -1))
        retStr = retStr.substring(0, retStr.lastIndexOf(",")) + ")";
      if ((ret2Str != "") && (ret2Str.lastIndexOf(',') > -1))
        ret2Str = ret2Str.substring(0, ret2Str.lastIndexOf(",")) + ")";
    } catch (err) { LogError(err.toString()); }

    return [retStr, ret2Str];
  }

  String genSet(Map<String,String> cSet) {
    List<String> setVal = genSetList(cSet);
    return setVal[0]+" values "+setVal[1];
  }

  String get query {
    String myQuery = "";
    String cElem = "";

    if (into != "") myQuery += "insert into "+into;
    else return "missing into clause";

    cElem = genSet(_set);
    if (cElem != "") myQuery += " "+cElem;
    else return "missing set clauses";

    if (!QCreator.noConv) myQuery = StrConv.utf2Cp(myQuery);

    //Postgre returning id
    if ((retId != null) && (retId.isNotEmpty)) {
      //myQuery += " ON CONFLICT DO NOTHING";
      myQuery += " returning "+retId;
    }

    return myQuery+";";
  }

}

class InsertCreatorMulti extends InsertCreator {
    List<InsertCreator> rows = [];

    void addRow(InsertCreator row) => rows.add(row);

    String get query {
      String myQuery = "";

      if (into != "") myQuery += "insert into "+into;
      else return "missing into clause";

      String labels;
      String values = "";
      for (int i = 0; i < rows.length; i++) {
        List<String> cElem = rows[i].genSetList(rows[i].gset);
        if (i == 0) labels = cElem[0]; //Labels list

        if (values.isNotEmpty) values += ',';
        values += cElem[1];
      }

      if (values != "") myQuery += " "+labels+" values "+values;
      else return "missing set clauses";

      if (!QCreator.noConv) myQuery = StrConv.utf2Cp(myQuery);

      //Postgre returning id
      if ((retId != null) && (retId.isNotEmpty)) {
        //myQuery += " ON CONFLICT DO NOTHING";
        myQuery += " returning "+retId;
      }

      return myQuery+";";
    }
}