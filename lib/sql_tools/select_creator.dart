part of sqlTools;

class SelectCreator extends QCreator {
  List <String> _from = new List();
  List <String> _order = new List();
  List <bool> _orderPos = new List();

  Map<String,String> _ljoin = new Map();
  Map<String,String> _rjoin = new Map();
  Map<String,String> _ijoin = new Map();

  Map<String,String> get leftjoin => _ljoin;
  Map<String,String> get rightjoin => _rjoin;
  Map<String,String> get innerjoin => _ijoin;

  String _group = "";

  void set orderPos(bool order) {
    _orderPos.forEach((el) => el = order);
  }

  String addOrders() {

    String retStr = "";
    for (int i = 0; i < _order.length; i ++) {
      String sOrder = "";
      if (_orderPos[i]) sOrder = "asc";
      else sOrder = "desc";

      retStr += _order[i]+" "+sOrder+",";
    }
    if (retStr != "") retStr = retStr.substring(0,retStr.lastIndexOf(","));
    return retStr;
  }


  void clear() {
    super.clear();
    _from.clear();
    _order.clear();
    _orderPos.clear();
    _ljoin.clear();
    _rjoin.clear();
    _ijoin.clear();
    _dictionary.clear();
    sName.clear();
    _group = "";
  }

  void set addFrom(String from) => _from.add(rmUnSafe(from));

  void set addOrder(String order) {
    _order.add(rmUnSafe(order));
    _orderPos.add(false);
  }

  void set addOrderPos(String order) {
    _order.add(rmUnSafe(order));
    _orderPos.add(true);
  }

  void set group(String group) {
    _group = rmUnSafe(group); }

  void lJoin(String name, String clause) {
    _ljoin[rmUnSafe(name)] = clause;
  }

  void rJoin(String name, String clause) {
    _rjoin[rmUnSafe(name)] = clause;
  }

  void iJoin(String name, String clause) {
    _ijoin[rmUnSafe(name)] = clause;
  }


  String addJoin(String jMethod,Map<String,String> join) {
    String retStr = "";
    join.forEach((name,clause) => retStr += jMethod+" "
        +rmUnSafe(name)+" on "+clause);
    return retStr;
  }

  String get query {
    String myQuery = "";
    String cElem = "";

    cElem = addElem(_what);
    if (cElem != "") myQuery += "select "+cElem;
    else return "missing what clauses";

    cElem = addElem(_from);
    if (cElem != "") myQuery += " from "+cElem;
    else return "missing from clauses";

    cElem = addJoin(" inner join ",_ijoin);
    if (cElem != "") myQuery += cElem;

    cElem = addJoin(" left outer join ",_ljoin);
    if (cElem != "") myQuery += cElem;

    cElem = addJoin(" right outer join ",_rjoin);
    if (cElem != "") myQuery += cElem;

    cElem = addWhere(" and ",_andWhere) + addWhere(" or ",_orWhere);
    if (cElem != "") {
      //Remove first and or first or clause
      if (cElem.substring(0,4) == " and") cElem = cElem.substring(4,cElem.length);
      else cElem.substring(3,cElem.length);
      myQuery += " where "+cElem;
    }

    //Add only if cElem is not empty
    cElem = addOrders();
    if (cElem != "") {
      myQuery += " order by "+cElem;
    }

    int rstart = (page-1)*limit;
    if (limit > 0) myQuery += " limit $rstart, $limit";

    if (_group != "") myQuery += " group by "+_group;

    if (!QCreator.noConv) myQuery = StrConv.utf2Cp(myQuery);
    return myQuery+";";
  }

}

