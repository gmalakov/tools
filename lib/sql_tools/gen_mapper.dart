part of sqlTools;

abstract class GenMapper {
  static SqlType get sqType => QExecutor.sqlDriver; // Get driver from QExecutor
  static bool parseDates = false; //return DateTime or String on sParseRes
  Map<String,String> _fNames = new Map();

  static dynamic sParseRes(String val, dynamic inVar,{bool noConv:false}) {

    if (inVar is DateTime) {
      String outVar = inVar.toString();
      if (outVar == QCreator.zeroDateTime+'.000') outVar = null;
      if (parseDates && outVar != null) return inVar; // No parsing needed
      if (parseDates && outVar==null) return null; //Zero datetime found

      if (parseDates) return inVar;
      else
        return outVar;
    }

    //json jsonb fileds are not being parsed - postgres
    if ((inVar is List) || (inVar is Map)) return inVar;

    dynamic outVar = null;

    if ((val.indexOf("int") >= 0) || (val.indexOf("serial") >= 0)) {
      //First try to detect if inVar is integer if not parse it and finally if parsing fails return zero value
      if (inVar is int) outVar = inVar;
       else try { outVar = int.parse(inVar.toString()); } catch (err) { outVar = 0; }
    }

    if (val.indexOf("bool") >= 0) {
      //Boolean
      if (inVar is bool) outVar = inVar;
      if (inVar is String)
        if (inVar == "true") outVar = true;
         else outVar = false;
      if (inVar is int)
        if (inVar == 1) outVar = true;
          else outVar = false;
    }

    if ((val.indexOf("float") >= 0) || (val.indexOf("real") >= 0) || (val.indexOf("double") >= 0) ||
        (val.indexOf("numeric") >= 0)) {
    //First try to detect if inVar is numeric or double and assign it to outvar if true
    //If not parse value
      if (inVar is num) outVar = inVar.toDouble();
        else
         if (inVar is double) outVar = inVar;
          else
           try { outVar = double.parse(inVar.toString()); } catch (err) { outVar = 0; }
    }

    //Mysql list or map variable - decode
    if (((val.indexOf("list") > -1) || (val.indexOf("map") > -1)) && (inVar is String))
      return json.decode(inVar.toString());


    //If returning result should be text or blob or whatever like this. Check if it should be converted to utf or not
    //And do whatever is necessary
    if ((val.indexOf("text") >= 0) || (val.indexOf("varchar") >= 0) ||
        (val.indexOf("blob") >= 0) || (val.indexOf("string") >= 0)) {
      if (inVar != null) {
        if (noConv) outVar = inVar.toString();
        else outVar = StrConv.cp2Utf(inVar.toString());
      }
      else outVar = "";
    }

     if (val.indexOf("date") >= 0 && outVar is String) {
       if (outVar == QCreator.zeroDateTime || outVar == QCreator.zeroDate) outVar = null;
        else {
         outVar = Dates.validDate(inVar.toString());
         if (parseDates) outVar = Dates.parseDate(outVar as String);
       }
     }

     return outVar;
  }


  dynamic parseRes(String val, dynamic inVar, {bool noConv : false}) => sParseRes(val,inVar,noConv: noConv);

  static Future<List<Map>> select(SelectCreator sQ,{bool noConv, ErrFunc onError }) {
    Completer<List<Map>> comp = new Completer();
    noConv = noConv ?? QCreator.noConv;

    QExecutor.instance.select(sQ, onError)
     .then((List res) {
       if (res != null) {
         List<Map> oList = [];
         res.forEach((dynamic row) {
           //Parse Mysql or postgre row
           //if (sqType == SqlType.mysql) {
           Map<String, dynamic> oMap = new Map<String, dynamic>();
           for (int i = 0; i < sQ.what.length; i++) {
             String sName = sQ.sName[i];
             if (sqType == SqlType.mysql)
               oMap[sName] = sParseRes(sQ.getType(sName),
                   (row as mysql.Row)[i], noConv: noConv);
             else
               //Make sure result is passed correctly
               oMap[sName] = sParseRes(
                   sQ.getType(sName), (row as List<dynamic>)[i],
                   noConv: noConv);
           }
           oList.add(oMap);
           //}

         });
         comp.complete(oList);
       }
       else comp.complete([]);
    })
    .catchError((dynamic err, dynamic trace) async {
      QExecutor.instance.initPool();
      LogError ("Error: "+err.toString());
      if (onError != null) await onError(err.toString());
      if (!comp.isCompleted) comp.complete([]);
      LogError (trace.toString());
      LogError (sQ.query);
    });

    return comp.future;
  }

  Map parseRow(dynamic row, List<String> names) {
    Map oMap = new Map<String, dynamic>();
    int len;

    if (sqType == SqlType.mysql) {
      row = row as mysql.Row;
      len = (row as mysql.Row).length;

      for (int i = 0; i < len; i++)
        oMap[names[i]] = parseRes(_fNames[names[i]], row[i], noConv: QCreator.noConv);
    }

    if (sqType == SqlType.postgre) {
      row = row as List<dynamic>;
      len = (row as List<dynamic>).length;
      for (int i = 0; i < len; i++)
        oMap[names[i]] = parseRes(_fNames[names[i]], row[i], noConv: QCreator.noConv);

      }

    return oMap;
   }

  Map<String, dynamic> get zeroMap {
    Map oMap = new Map<String, dynamic>();

    _fNames.forEach((cName, val) {
      if (val.indexOf("int") >= 0)
        oMap[cName] = 0;

      if ((val.indexOf("text") >= 0) || (val.indexOf("varchar") >= 0))
        oMap[cName] = "";

      if (val.indexOf("date") >= 0) oMap[cName] = "";
    });

    return oMap;
  }

  //set field names
  void set fields(String fld) {
    if ((fld == null) || (fld == "")) return;
    //fld = fld.toLowerCase(); wont be lowercase
    fld = fld.trim();

    List<String> fNames = fld.split(",");
    fNames.forEach((el) {
      el = el.trim();
      if (el != "") {
        List<String> fL = el.split(":");
        try {
          fL[0] = fL[0].trim();
          fL[1] = fL[1].trim();

          //Assign data type to data name map
          _fNames[fL[0]] = fL[1];
        } catch (err) { LogError (err.toString()+" "+el); fL[0] = fL[0].trim(); }
      }
    });
  }

}
