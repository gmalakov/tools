part of sqlTools;

class UpdateCreator extends QCreator {

  String genSet(Map<String,String> cSet) {
    String retStr = "";
    cSet.forEach((name,value) {
      //String type = getType(name);
      if ((value.indexOf("()") > -1) || (value.indexOf("current_date") > -1))
        retStr += name+"="+value+",";
      else retStr += name+"='"+value+"',";
    });
    if (retStr != "") retStr = retStr.substring(0,retStr.lastIndexOf(","));
    return retStr;
  }

  String get query {
    String myQuery = "";
    String cElem = "";

    cElem = addElem(_what);
    if (cElem != "") myQuery += "update "+cElem;
    else return "missing what clauses";

    cElem = genSet(_set);
    if (cElem != "") myQuery += " set "+cElem;
    else return "missing set clauses";

    cElem = addWhere(" and ",_andWhere) + addWhere(" or ",_orWhere);
    if (cElem != "") {
      //Remove first and or first or clause
      if (cElem.substring(0,4) == " and") cElem = cElem.substring(4,cElem.length);
      else cElem.substring(3,cElem.length);
      myQuery += " where "+cElem;
    }

    if (!QCreator.noConv) myQuery = StrConv.utf2Cp(myQuery);
    return myQuery+";";
  }
}
