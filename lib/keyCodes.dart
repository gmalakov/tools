library keyCodes;
import "dart:html";

class KeyCodes {
  /*
   * KeyCode = (SimpKey.keyCode < 3) SAC(b)
   *                                 |Shift
   *                                  |Alt
   *                                   |Ctrl (Pressed)
   */

  //Examples
  static const int CTRL_F = (KeyCode.F << 3) + 1;
  static const int CTRL_S = (KeyCode.S << 3) + 1;
  static const int CTRL_P = (KeyCode.P << 3) + 1;
  static const int CTRL_G = (KeyCode.G << 3) + 1;
  static const int CTRL_Y = (KeyCode.Y << 3) + 1;
  static const int CTRL_O = (KeyCode.O << 3) + 1;
  static const int CTRL_D = (KeyCode.D << 3) + 1;
  static const int CTRL_R = (KeyCode.R << 3) + 1;
  static const int CTRL_W = (KeyCode.W << 3) + 1;
  static const int CTRL_V = (KeyCode.V << 3) + 1;
  static const int CTRL_Z = (KeyCode.Z << 3) + 1;

  static const int ALT_F1 = (KeyCode.F1 << 3) + 2;
  static const int SHIFT_F1 = (KeyCode.F1 << 3) + 4;
  static const int ALT_CTRL_INS = (KeyCode.INSERT << 3) + 3;

  static int keyCode(KeyEvent kEv) {
    //Shift left 3 bits keyCode
    int sKey = kEv.keyCode << 3;

    if (kEv.ctrlKey) sKey = sKey | 1;
    if (kEv.altKey) sKey = sKey | 2;
    if (kEv.shiftKey) sKey = sKey | 4;

    return sKey;
  }

  static bool isKey(int keyCode, KeyEvent kEv) {
    int sKey = keyCode >> 3;
    bool shiftCheck, altCheck, ctrlCheck;

    (keyCode & 1) == 1 ? ctrlCheck = true : ctrlCheck = false;
    (keyCode & 2) == 2 ? altCheck = true : altCheck = false;
    (keyCode & 4) == 4 ? shiftCheck = true : shiftCheck = false;

    if ((ctrlCheck) && (!kEv.ctrlKey)) return false;
    if ((altCheck) && (!kEv.altKey)) return false;
    if ((shiftCheck) && (!kEv.shiftKey)) return false;

    if (sKey == kEv.keyCode) return true;
    else return false;
  }
}
