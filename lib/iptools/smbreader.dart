part of conf_reader;

class SMBReader {
  String _name;
  Map<String, Map<String, dynamic>> sections = new Map();

  SMBReader(this._name);

  Future<bool> readFile() async {
    String content;
    try { content = await (new File(_name).readAsString()); }
    catch (err) { print('SmbReader:'+err.toString()); return false; }

    content = content
        .replaceAll("\r\n", "\n")
        .replaceAll("\t", "")
        .replaceAll("; ", ";\n")
        .replaceAll(";\t", ";\n")
        .replaceAll("  ", " ");

    List<String> lines = content.split("\n"); //SPlit by new line
    //print(lines);
    decodeLines(lines);

    return true;
  }

  void decodeLines(List<String> lines) {
    String section = ""; //Start with empty section

    for (int i = 0; i < lines.length; i++) {
      String cL = lines[i].trim();

      if (cL.isNotEmpty)
        if (cL.indexOf(new RegExp(r"\[\w+\]")) == 0) {
          //New section is found
          section = cL.substring(1,cL.length-1);

          sections[section] = new Map<String, dynamic>();
        } else {
          //Add parameter to old section
          if (section.isNotEmpty) {
            List<String> cLS = cL.split("=");
            cLS[0] = cLS[0].trim();

            if (cLS.length == 2) {
              String cVal = cLS[1].trim();;
              if (cVal.indexOf(" ") > -1) {
                //It's an array.
                List<String> cVS = cVal.split(' ');
                List<String> vals = new List();
                for (int j = 0; j < cVS.length; j++)
                  if (cVS[j].isNotEmpty) vals.add(cVS[j].trim());

                sections[section][cLS[0]] = vals;
              } else
                sections[section][cLS[0]] = cVal;

            }
          }
        }
    }
  }

  String encodeLines() {
    String encodeSection(Map cS) {
      String oS = "";
      for (dynamic n in cS.keys) {
        oS += "\t"+(n as String)+" = ";
        if (cS[n] is List) oS += (cS[n] as List).join(" ");
         else oS += cS[n].toString();

        oS += "\n";
      }

      return oS;
    }

    String oS = "";
    oS += "[global]\n";
    oS += encodeSection(sections["global"]);
    for (String cSN in sections.keys)
      if (cSN != "global") {
        oS += "["+cSN+"]\n";
        oS += encodeSection(sections[cSN]);
      }
        return oS;
    }


    Future<bool> writeFile([String name]) async {
      if (name == null) name = _name;
      try {
       await new File(name).writeAsString(encodeLines());
      } catch (err) {
        print(err); return false;
      }
      return true;
    }

}