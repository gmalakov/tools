part of conf_reader;

class ConfReader {
  String _name;
  Map<String, dynamic> _params = new Map<String, dynamic>();
  Map<String, dynamic> get params => _params;

  ConfReader(this._name);

  void addMap(Map inMap, String name, dynamic val) {
    if (inMap == null) return; //Map is not defined
    if (inMap[name] == null) inMap[name] = val; //not defined -> just add new value
    else {
      if (inMap[name] is List) inMap[name].add(val); //defined and already a list -> add new value
      else { //Defined but it's not a list -> make it a list and add old value and new value
        dynamic oldVal = inMap[name];
        inMap[name] = new List<dynamic>();
        inMap[name]
          ..add(oldVal)
          ..add(val);
      }
    }
  }

  int readSection(List<String> input, {int start = 0, Map cMap, int level = 0}) {
    List<String> leftOver = input; //Initilize string
    level++;

    if (cMap == null) cMap = _params;
    //Start from beginning if not specified where to start

    int i = start;
    String comment = "";

    while (i < leftOver.length) {
      String cS = input[i].trim();
      bool dive = false;
      String name;
      dynamic value = "";

      //Remove comment if any
      if (cS.indexOf("#") > -1) {
        comment += cS.substring(cS.indexOf("#")+1).trim()+"\n"; //Add comment
        if (cS.indexOf("#") > 0)
          cS = cS.substring(0, cS.indexOf("#") - 1); //Trim comment
        else cS = ""; //If # is on 0 position remove string leave comment only
      }

      if (cS.length > 0)  {
        //String is not empty and is not a comment

        dive = null; //remove dive status
        //Will we dive further or it's end of the row
        if (cS.substring(cS.length-1) == "{") dive = true;
        if (cS.substring(cS.length-1) == ";") dive = false;

        if (dive != null) cS = cS.substring(0, cS.length-1); //Remove last character
        if (dive == null) dive = false; // Reset dive status

        List<String> cSS = cS.split(" ");
        if (cSS.length == 1) name = cS.trim();
        else {
          name = cSS[0].trim();
          value = cS.substring(name.length, cS.length).trim();

          //Its and operator -> looking for =
          String nL = name.toLowerCase();
          int idx = (value as String).indexOf('=');
          if ((nL == "if" || nL == "elsif") && (idx > -1) ){
            name += " "+(value as String).substring(0, idx).trim();
            value = (value as String).substring(idx, (value as String).length);
          }

          //Try to parse numeric data
          try {
            if ((value as String).indexOf(".") > -1) value = double.parse(value as String);
             else value = int.parse(value as String);
          } catch (err) { //value is a string data
             }
        }

        //print(name+"=>"+value);
        if ((name == "}") && (level > 1)) return (i+1); //If } is reached exit current level and return position + 1 for next row
        else addMap(cMap, name, value); // Or enter current name
      }
      i++;

      //if dive and name is initialized (must be if we dive or some shit is happened)
      //dive further
      if (comment.isNotEmpty) cMap["###comment"] = comment; //if comment is not empty --> add it

      if ((dive) && (name != null)) {
        //Default name is the name of the property
        String cId = name;
        //If we already have value add design for dhcpd.conf
        if ((value != null) && ((value as String).isNotEmpty))
          cId = "###"+name+"###"+(value as String);

        cMap[cId] = new Map<String, dynamic>();
        i = readSection(input, start:i, cMap: cMap[cId] as Map, level: level);
      }
    }

    return i;
  }


  List<String> preProcess(List<String> inS) {
    List<String> outS = new List();
    for (int i = 0; i < inS.length; i++) {
      inS[i] = inS[i].trim(); //Trim elements
      if (inS[i].isNotEmpty) outS.add(inS[i]); //Add non empty elements
    }

    for (int i = 0; i < outS.length; i++) {
      int len = outS[i].length;
      String st = "";
      if (len > 0) st = outS[i].substring(0,1);

      if ((st == "{") && (i > 0)) {  //Row starts with {
        outS[i-1] += "{"; // Move to upper row
        outS[i] = outS[i].substring(1); //Remove from current row
      }

      //If row starts with } and it's not empty. Add other part to new row
      if (len > 1 && st == "}") {
        String tmp = outS[i].substring(1, outS[i].length);
        outS[i] = "}";
        outS.insert(i+1, tmp);
      }

      if ((len > 1) && (st != "#") &&
          (outS[i].substring(outS[i].length-1) == "}")) { //Row finishes with } and it's not a comment
        outS[i] = outS[i].substring(0, outS[i].length-2); //Remove }
        outS.insert(i+1, "}"); //Add row with only }
      }

      // xxxx } xxx
      int idx = outS[i].indexOf("}");
      if (len > 1 && idx > 0) {
        String tmp = outS[i];
        outS[i] = tmp.substring(0, idx);
        outS.insert(i+1, "}");
        outS.insert(i+2, tmp.substring(idx+1, tmp.length));
      }

      // xxxx { xxx
      idx = outS[i].indexOf("{");
      if (len > 1 && idx > 0) {
        String tmp = outS[i];
        outS[i] = tmp.substring(0, idx+1);
        outS.insert(i+1, tmp.substring(idx+1, tmp.length));
      }
    }

    return outS;
  }

  bool readContent(String content) {
    content = content
        .replaceAll("\r\n", "\n")
        .replaceAll("\t", "")
        .replaceAll("; ", ";\n")
        .replaceAll(";\t", ";\n")
        .replaceAll("  ", " ");

    List<String> contSplit = content.split("\n"); //SPlit by new line
    contSplit = preProcess(contSplit); //Repair punctual errors if any
    int i = readSection(contSplit);
    //print("$i ${contSplit.length}");
    return (i == contSplit.length) ? true : false;
  }

  Future<bool> readFile() async {
    String content;
    try {
      content = await new File(_name).readAsString();
    } catch(err) {
      print("File $_name not found!"); content = '';//Return empty string
    }

    return readContent(content);
  }

  bool readFileSync() {
    String content;
    try {
      content = new File(_name).readAsStringSync();
    } catch(err) {
      print("File $_name not found!"); content = '';//Return empty string
    }

    return readContent(content);
  }

  String renderMap(Map<String, dynamic> inMap, {int level = 0}) {

    String makeDef(String name, dynamic val) {
      String outStr = "";
      //add necessarry tabs
      for (int i = 1; i < level; i++) outStr += "\t";

      if (val is Map) {
         outStr += name + " {\n";
         outStr += renderMap(val as Map<String,dynamic>, level: level);
      } else {
        if (!(val is String) || ((val as String).length > 0))
          outStr += name + " " + val.toString();
        else outStr += name; //Do not add interval if no value

        String cId = "###"+name+"###"+val.toString();
        if (inMap[cId] != null) {
          //Dive further
          outStr += " {\n";
          outStr += renderMap(inMap[cId] as Map<String,dynamic>, level: level);
        } else outStr += ";\n"; //not diving add semicolon and new line
      }

      return outStr;
    }

    String outStr = ""; //init empty string
    level++;

    void addTabs() {
      for (int i = 1; i < level; i++) outStr += "\t";
    }

    //Check for comments
    if (inMap["###comment"] != null) {
      //print(inMap["###comment"]);
      List<String> splComments = (inMap["###comment"] as String).split("\n");
      for (int i = 0; i < splComments.length; i++)
        if (splComments[i].trim().length > 0) {
          addTabs();
          outStr += "#"+splComments[i].trim()+"\n";
        }
      //Add final new line
      outStr += "\n";
    }

    for (String cKey in inMap.keys) {
      if ((cKey.length > 3) && (cKey.substring(0,3) != "###")) {
        //if it's not a addon key


        if (inMap[cKey] is List)
          (inMap[cKey] as List).forEach((dynamic el) {
            outStr += makeDef(cKey, el);
          });
        else outStr += makeDef(cKey, inMap[cKey]);
      }
    }

    //Add necesarry tabs
    addTabs();
    if (level > 1) outStr += "}\n"; //Add closing bracket and new line
    return outStr;
  }

  Future<bool> writeFile([String name]) {
    if (name == null) name = _name;
    //print(params);
    return new File(name).writeAsString(renderMap(params)).catchError((dynamic err) {
      return false;
    })
    .then((File f) {
      return true;
    });
  }

}