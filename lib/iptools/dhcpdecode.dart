part of conf_reader;

class Host {

  static int ipnum(String ip) {
    if (ip.isEmpty) return null;
    List<String> splIp = ip.trim().split(".");
    try {
      return int.parse(splIp[3]);
    } catch (err) { //Error parsing ip
      print(err); return null;
    }
  }

  String name;
  String ip;
  String _mac;
  String comment;

  String get mac => _mac;
  set mac(String inMac) => (_mac = inMac.toUpperCase());

  int get num => (ipnum(ip));
  Map initMap;

  Host(this.name, this.ip, String mac) {
    this.mac = mac;
  }

  Host.decode(this.name, this.initMap) {
    ip = initMap["fixed-address"] as String;
    if (initMap["hardware"]!= null) {
      List<String> splHw = (initMap["hardware"] as String).trim().split(" ");
      if (splHw.length == 2) mac = splHw[1].toUpperCase();
    }

    comment = initMap["###comment"] as String;
  }

  Map toMap() {
    if (initMap == null) initMap = new Map<String, dynamic>();
    initMap["fixed-address"] = ip;
    if (mac != null) initMap["hardware"] = "ethernet "+mac;
    if ((comment != null) && (comment.isNotEmpty)) initMap["###comment"] = comment;
    //Fill in arguments which can change
    return initMap; //Return modified map
  }

  String toString() => toMap().toString();
}

class SubNet {
  /*
  int cidrMask = 23;
long bits = 0;
bits = 0xffffffff ^ (1 << 32 - cidrMask) - 1;
String mask = String.format("%d.%d.%d.%d", (bits & 0x0000000000ff000000L) >> 24, (bits & 0x0000000000ff0000) >> 16, (bits & 0x0000000000ff00) >> 8, bits & 0xff);

>>255.255.254.0
   */

  static int bitMask(String mask) {
    List<String> spl = mask.trim().split(".");
    int bitMask = 0;
    for (int i = 0; i < spl.length; i++)
      switch(spl[i]) {
        case "255": bitMask += 8;
        break;
        case "254": bitMask += 7;
        break;
        case "252": bitMask += 6;
        break;
        case "248": bitMask += 5;
        break;
        case "240": bitMask += 4;
        break;
        case "224": bitMask += 3;
        break;
        case "192": bitMask += 2;
        break;
        case "128": bitMask += 1;
        break;
        case "0": bitMask += 0;
        break;
      }

    return bitMask;
  }

  static int digitMask(String mask) {
    int bm = bitMask(mask);
    int res = 0;
    int i = 0;
    while (i < bm) {
      res = (res | 1) << 1; //Set 1ones to bits needed
      i++;
    }
    while (i < 31) { //Leftover bits will be zeros
      res = res << 1;
      i++;
    }

    return res;
  }

  static String applyMask(String ip, String mask) {
    int dm = digitMask(mask);
    int iprn = ip2int(ip) & dm;
    String ipret = int2ip(iprn);
    //print(ip+"/"+dm.toRadixString(2)+"->"+ipret);
    return ipret;
  }

  static String mask(int bitMask) {
    int bits = 0xffffffff ^ (1 << 32 - bitMask) - 1;
    String outS = ((bits & 0x0000000000ff000000) >> 24).toString()+".";
    outS += ((bits & 0x0000000000ff0000) >> 16).toString()+".";
    outS += ((bits & 0x0000000000ff00) >> 8).toString()+".";
    outS += (bits & 0xff).toString();
    return outS;
  }

  static String int2ip(int num) {
    String outS = "";
    for (int i = 0; i < 4; i++) {
      int cNum = num & 0xff;
      outS = cNum.toString()+outS;
      if (i < 3) {
        num = num >> 8; //Shift 8 bits right
        outS = "."+outS; //Add a dot
      }
    }

    return outS;
  }

  static int ip2int(String ip) {
    List<String> ipL = ip.trim().split(".");
    int num = 0;
    for (int i = 0; i < ipL.length; i++) {
      try {
        num = (num << 8) | (int.parse(ipL[i]) & 0xff);
      } catch (err) { print('IpInt:'+err.toString()); }
    }

    return num;
  }

  String ip;
  String netMask;
  List<String> routers;
  String comment;
  Map<String, Host> _hosts = new Map();
  Map<String, Host> get hosts => _hosts;
  Map initMap;

  void addHost(Host nHost) => _hosts[nHost.name] = nHost;
  void delHost(String name) => _hosts.remove(name);

  SubNet();
  SubNet.decode(String name, this.initMap) {
    //name -> <ip> netmask <mask>
    List<String> splName = name.trim().split(" ");
    if (splName.length == 3) {
      ip = splName[0];
      netMask = splName[2];
    }

    dynamic opt = initMap["option"];
    if (opt != null) {
      //Convert to list if one element
      if (opt is String) opt = new List<dynamic>()..add(opt);

      if (opt is List)
        for (dynamic s in opt) {
          String str = s as String;
          if (str.indexOf("routers") == 0) {
            str = str.substring(8).trim();
            if (str.isNotEmpty) routers = str.split(",");
            if (routers != null)
              routers.forEach((String el) => el = el.trim()); //Trim spaces
          }
        }
    }

    dynamic hosts = initMap["host"];
    if (hosts is List<String> || hosts is List<dynamic>)
      hosts.forEach((dynamic el) {
        _hosts[el.toString()] = new Host.decode(el.toString(), initMap["###host###"+el.toString()] as Map);
      });
    else
      if (hosts != null)
        _hosts[hosts as String] = new Host.decode(hosts as String, initMap["###host###"+(hosts as String)] as Map);

    comment = initMap["###comment"] as String;
  }

  Map toMap() {
    List<String> names = new List();
    for (String name in _hosts.keys) {
      names.add(name); //Add name to list
      initMap["###host###"+name] = _hosts[name].toMap(); //Encode every host
    }

    initMap["host"] = names; //Encode names
    if ((comment != null) && (comment.isNotEmpty)) initMap["###comment"] = comment;

    return initMap;
  }

  String toString() => toMap().toString();

  List<int> getRange() {
    int num = ip2int(ip);
    int first = num + 1; //First address is network address + 1
    int last = (num | ~ip2int(netMask)) - 1;

    String ipF = int2ip(first);
    String ipL = int2ip(last);

    //Last address must be broadcast address -1

    return [Host.ipnum(ipF), Host.ipnum(ipL)];
  }

  bool available(String ip) {
    bool avail = true;
    List<String> k = _hosts.keys.toList();
    for (int i = 0; (i < k.length) && (avail); i++)
      if (_hosts[k[i]].ip == ip) avail = false;

    //If ip is still available check routers too!
    if ((avail) && (routers != null))
      for (String cIp in routers)
        if (cIp == ip) avail = false;

    return avail;
  }

  List<String> availableIps() {
    List<int> range = getRange();
    List<String> ipS = new List();
    int net = ip2int(ip); //Network num
    for (int i = range[0]; i < range[1]; i++) {
      String cIp = int2ip(net | i);
      if (available(cIp)) ipS.add(cIp);
    }

    return ipS;
  }

  String firstIp() {
    List<int> range = getRange();
    int net = ip2int(ip); //Network num

    for (int i = range[0]; i < range[1]; i++) {
      String cIp = int2ip(net | i);
      if (available(cIp)) return cIp;
    }

    return null; // We reach here if no address is found and will return null;
  }

}


class NetWork {
  String netName;
  String comment;
  Map<String, SubNet> _subNets = new Map();
  Map<String, SubNet> get subNets => _subNets;
  Map initMap;

  NetWork();
  NetWork.decode(this.netName, this.initMap) {
    dynamic subs = initMap["subnet"];
    //print(subs);
    if (subs is List<String> || subs is List<dynamic>)
      subs.forEach((dynamic el) {
        SubNet sub = new SubNet.decode(el.toString(), initMap["###subnet###"+el.toString()] as Map);
        _subNets[sub.ip]=  sub;
      });
    else {
      try {
        SubNet sub = new SubNet.decode(
            subs as String, initMap["###subnet###" + (subs as String)] as Map);
        _subNets[sub.ip] = sub;
      } catch (err) {
        print(err.toString()+"\n"+subs.runtimeType.toString()+"\n"+subs.toString());
      }
    }

    comment = initMap["###comment"] as String;
  }

  Map toMap() {
    List<String> names = new List();
    for (String name in _subNets.keys) {
      String cName = name+" netmask "+_subNets[name].netMask;
      names.add(cName); //Add name to list
      initMap["###subnet###"+cName] = _subNets[name].toMap(); //Encode every host
    }

    initMap["subnet"] = names; //Encode names
    if ((comment != null) && (comment.isNotEmpty)) initMap["###comment"] = comment;
    return initMap;
  }

  String toString() => toMap().toString();
}


class DHCPConf {
  String _name;
  String comment;
  DHCPConf(this._name);
  ConfReader _file;
  Map<String, NetWork> _networks = new Map();
  Map<String, NetWork> get networks => _networks;

  void decodeNets() {
    Map par = _file.params;
    dynamic nets = par["shared-network"];
    if (nets is List<String> || nets is List<dynamic>)
      nets.forEach((dynamic el) {
        _networks[el.toString()] = new NetWork.decode(el.toString(), par["###shared-network###"+el.toString()] as Map);
      });
    else
      if (nets != null)
        _networks[nets as String] = new NetWork.decode(nets as String, par["###shared-network###"+(nets as String)] as Map);

    comment = par["###comment"] as String;
  }

  Map toMap() {
    List<String> names = new List();
    for (String name in _networks.keys) {
      names.add(name); //Add name to list
      _file.params["###shared-network###"+name] = _networks[name].toMap(); //Encode every host
    }

    _file.params["shared-network"] = names; //Encode names

    if ((comment != null) && (comment.isNotEmpty)) _file.params["###comment"] = comment;

    return _file.params;
  }

  String toString() => toMap().toString();

  Future<bool> readFile() async {
    _file = new ConfReader(_name);
    if (await _file.readFile()) {
      decodeNets();
      //File is read now convert to objects
      return true;
    } else return false;
  }

  Future<bool> writeFile([String name]) async {
    if (_file == null) return false;
    toMap(); //return changed variables to map
    return _file.writeFile(name);
  }

}