part of conf_reader;

class SHReader {
  //\w+=.+ <--- var regex

  String _name;
  List<String> _lines;
  Map<String, int> _positions = new Map<String, int>();
  Map<String, dynamic> vars = new Map<String, dynamic>();
  SHReader(this._name);

  Future<bool> readFile() async {
    String content;
    try { content = await (new File(_name).readAsString()); }
    catch (err) { print('SHReader:'+err.toString()); return false; }

    content = content
        .replaceAll("\r\n", "\n")
        .replaceAll("\t", "")
        .replaceAll("; ", ";\n")
        .replaceAll(";\t", ";\n")
        .replaceAll("  ", " ");

    _lines = content.split("\n"); //SPlit by new line
    decodeLines();

    return true;
  }

  bool decodeLines() {
    dynamic parseEl(dynamic el) {
      el = el.trim();
      if (el.indexOf("\"") == 0) el = el.substring(1,el.length-1); //Remoove "
      else {
        try {
          //Must be number
          if ((el as String).indexOf(".") > -1)
            el = double.parse(el as String);
          else
            el = int.parse(el as String);
        } catch(err) {
          print("Error parsing var. "+err.toString());
          return null;
        }
      }

      return el;
    }

    if (_lines == null) return false;
    for (int i = 0; i < _lines.length; i++) {
      int idx = _lines[i].indexOf(new RegExp(r"\w+=.+"));
      if (idx > -1) {
        String cS = _lines[i].trim();
        List<String> cSP = cS.split("="); //Split by =
        cSP.forEach((el) => el = el.trim()); //Trim elements
        if (cSP.length == 2) {
          //Found element
          _positions[cSP[0]] = i;

          if (cSP[1].indexOf("( ") == 0)
           try {
            //it's an array
            String vals = cSP[1].substring(2,cSP[1].length-2);
            List<String> splVals = vals.split(" ");
            List oVars = new List<dynamic>();
            splVals.forEach((el) => oVars.add(parseEl(el)));

            vars[cSP[0]] = oVars;
          } catch(err) {
             //Parsing empty array?
             if (cSP[1] != "( )") print(err.toString()+" parsing: "+cSP[1]);
             vars[cSP[0]] = <String>[];
           } else {

            vars[cSP[0]] = parseEl(cSP[1]); //It's a single variable
          }
        }
      }
    }

    //print(vars);
    return true;
  }

  void encodeLines() {

    String encodeEl(dynamic el) {
      if (el is String) return "\""+el+"\""; //if it's string return with quotes
       else return el.toString(); //If it's number return without quotes
    }

    for (String cName in vars.keys) {
      int line = _positions[cName];
      int idx = _lines[line].indexOf(cName);

      String res = "";//print(idx);
      for (int i = 0; i < idx; i++) res += " "; //Restore position
      res += cName+"=";
      if (vars[cName] is List) {
        List<dynamic> cL = vars[cName] as List;
        res += "("; //Start array
        for (int j = 0; j < cL.length; j++) res += " "+encodeEl(cL[j]);
        res += " )";  //End array
      } else res += encodeEl(vars[cName]);

      _lines[line] = res;
    }
  }

  Future<bool> writeFile([String name]) async {
    if (name == null) name = _name;
    encodeLines();
    try {
      new File(name).writeAsString(_lines.join("\n"));
    } catch (err) { print('SHReader:'+err.toString()); return false; }

    return true;
  }

}