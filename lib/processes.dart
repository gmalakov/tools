library processes;
//Server side process control  functions

import 'dart:collection';
import 'dart:io';
import 'dart:async';
import 'dart:convert';

/*
 Api
  File > Send|Delete or whatever queue. To make sure all is sent to another location
  we need confirmation. So we send Map and receive OK or Error and retry on Error.

 */

typedef Future<String> SendFunc(Map data);

class AdvancedQueue {

  SendFunc _sendFunc;
  set sendFunc(SendFunc sF) => (_sendFunc = sF);
  bool _ready = true;

  String fileName;

  Queue<Map> messages = new Queue();
  AdvancedQueue(this._sendFunc, { String fileName = 'sendQueue.json'});

  Future<Null> _sendNext() async {
    //Save current queue status if something happens
    await saveQueue();
    //Exit if there are no messages
    if (messages.length == 0) return;

    Map el = messages.first;
    _ready = false; //Start sending message
    if (_sendFunc != null) {
      String res = await _sendFunc(el);
      if ((res != null) && (res == "OK"))
        {
          //If element is successfully sent then remove it from queue
          messages.removeFirst();

          if (messages.length == 0) {
            _ready = true; await delQueue();
          }
           else _sendNext();
        } else throw new Exception("Error sending queue! <$res>");
    }
     else throw new Exception("Send function is NULL!");
  }

  Future<Null> delQueue() async {
    //Remove queue file on empty queue
    await new File(fileName).delete();
  }

  Future<Null> saveQueue() async {
    //Save queue to text file
    List<Map> oList = messages.toList();
    await new File(fileName).writeAsString(json.encode(oList));
  }

  //Need this to be sync to load entire queue on starting queue
  void loadQueue() {
    String contents = "";

    try {
      contents = new File(fileName).readAsStringSync();
    } catch(err) { // File doesn't exist -> start empty queue
        }

    if (contents.isNotEmpty)
      try {
        List<Map> iList = json.decode(contents) as List<Map>;
        messages.addAll(iList);
      } catch (err) { print('Messages:'+err.toString()); }
  }

  Future<Null> sendMsg(Map el) async {
    messages.addFirst(el);
    if ((messages.length == 1) && (_ready))_sendNext();
  }

}