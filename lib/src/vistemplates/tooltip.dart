part of vistemplates;

class ToolTip {
  static const int autoHideSec = 30;
  static const String tpre = "tooltip_";
  static int ttid = 0;

  int _curId;
  bool _autohide = false;
  DivElement _tip, _arrow;
  num _top, _left, _bottom, _right;
  String _text;
  Timer _tim;
  Function _onClick;
  List<StreamSubscription> _ev = new List();

  void initAutoHide() {
    if (_tim == null) {
      _tim = new Timer(const Duration(seconds:autoHideSec), () {
        remove();
        _tim = null;
      });
    }
  }

  set autohide(bool hid) {
    if (hid) initAutoHide();
    else {
      if (_tim != null) _tim.cancel();
      _tim = null;
    }
  }

  void remove() {
    _tim?.cancel(); _tim = null;
    _ev?.forEach((el) => el.cancel());
    autohide = false; //Remove autohide timer;
    _arrow.remove(); //Remove arrow;
    _tip.remove(); //Remove tooltip
    _tip = null; _arrow = null; //Remove entries
  }

  bool get visible => (_tip != null);

  void appear() {
    if (_tip != null) return; //Check if _tooltip is already appeared

    _tip = new DivElement()
      ..id = "$tpre$ttid"
      ..className = "tooltip"
      ..appendHtml(_text);
    if (_top != null) _tip.style.top = _top.toString()+"%";
    if (_left != null) _tip.style.left = _left.toString()+"%";
    if (_bottom != null) _tip.style.bottom = _bottom.toString()+"%";
    if (_right != null) _tip.style.right = _right.toString()+"%";

    _arrow = new DivElement()
      ..className = "arrow";

    _tip.append(_arrow);
    document.body.append(_tip);
    //Appear tooltip
    _tip.classes.add("active");


    //Init automatic hiding if set
    autohide = _autohide;

    //Add click event
    _ev.add(document.body.onClick.listen((MouseEvent ev) {
      //Prevent default actions

      String id = (ev.target as Element).id;
      if (id == "$tpre$_curId") {
        ev.stopImmediatePropagation();
        ev.preventDefault();
        //We are clicked on the tooltip
        if (_onClick != null) _onClick(ev);
      }

      //In all cases on click tooltip is removed
      remove(); //Remove tooltip on click
    }));

  }

  void redraw() {
    if (!visible) appear();
    else {
      _tip.innerHtml = "";
      _tip
        ..appendHtml(_text)
        ..append(_arrow);
    }
  }

  set top(num i) {
    _top = i;
    if (_tip != null) _tip.style.top = i.toString()+"%";
  }

  set left(num i) {
    _left = i;
    if (_tip != null) _tip.style.left = i.toString()+"%";
  }

  set bottom(num i) {
    _bottom = i;
    if (_tip != null) _tip.style.bottom = i.toString()+"%";
  }

  set right(num i) {
    _right = i;
    if (_tip != null) _tip.style.right = i.toString()+"%";
  }

  set text(String txt) {
    _text = txt;
    if (visible) redraw();
  }


  num get top => (_top);
  num get left => (_left);
  num get bottom => (_bottom);
  num get right => (_right);
  String get text => (_text);


  ToolTip(this._text, {num top, num left, num bottom, num right, bool autohide:false, Function onClick}) {
    ttid++;
    _curId = ttid;
    _top = top; _left = left; _bottom = bottom; _right = right;
    _onClick = onClick;
    appear();
    _autohide = autohide;
  }

}