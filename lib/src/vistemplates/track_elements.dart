part of vistemplates;

class ActivityTrack {
  DateTime _lastMod = null;
  //Default time to modify 60 seconds
  int _modSec = 40;

  void set modSec(int sec)  {
    _modSec = sec;
  }

  void zeroMod() {
    _lastMod = null;
  }

  bool get saved {
    //Return true if not modification time is present
    if (_lastMod == null) return true;
    else return false;
  }

  bool get okMod {
    //In not modified yet return true - ok to modify
    if (_lastMod == null) return true;

    DateTime nD = new DateTime.now();
    if (nD.difference(_lastMod).inSeconds > _modSec) return true;
    else return false;
  }

  void modify() {
    _lastMod = new DateTime.now();
  }
}
//---------------------------------------------------
abstract class TrElem {
  ActivityTrack _act = new ActivityTrack();
  Element _inpEl = null;
  SpanElement _spEl = null;
  DivElement inDiv = new DivElement();
  bool _hidden = false;
  List<StreamSubscription> _ev = new List();
  Function onEnter = null;
  Function onChg = null;

  String fCol = "";
  String bCol = "";

  void setSpan(num size) {

    inDiv.style.width = "${size*14}"+"px";
    inDiv.style.lineHeight = "33px";
    _spEl.style.fontWeight = "bold";
    _spEl.style.userSelect = "text";
    _spEl.style.color = fCol;
    _spEl.style.lineHeight = "33px";
    _spEl.style.backgroundColor = bCol;
  }

  String toString() {
    return '{ "saved": ${_act.saved}, "okMod": ${_act.okMod}, "hidden": $hidden  }';
  }

  void calcSpan();

  bool get hidden => _hidden;
  void set hidden(bool hid) {
    _hidden = hid;

    if (_hidden) {
      _inpEl.style.display = "none";
      if (_spEl == null) _spEl = new SpanElement();
      _spEl.style.display = "inline";
      calcSpan();

      inDiv.append(_spEl);
    } else {
      _inpEl.style.display = "inline";
      if (_spEl != null) _spEl.remove();
      _spEl = null;
      inDiv.style.width = "";
    }
  }

  void set disabled(bool ck) {
    hidden = ck;
  }

  void setVis() {
    _inpEl.style.display = "inline";
    inDiv.style.display = "inline-block";
    inDiv.append(_inpEl);
  }

  void remove () {
    _ev.forEach((StreamSubscription el) => el.cancel());
    if (_inpEl != null) _inpEl.remove();
    if (_spEl != null) _spEl.remove();
    if (inDiv != null) inDiv.remove();
  }

  bool get okMod => _act.okMod;
  bool get saved => _act.saved;
  void zeroMod() => _act.zeroMod();
  void modify() => _act.modify();
}
//----------------------------------------------------
//Tracked Input Element
class TrSelect extends TrElem {
  SelectElement get inpEl => (_inpEl as SelectElement);
  void set inpEl (SelectElement el) {
    _inpEl = el;
  }

  int get id {
    int id = -1;

    try { id = int.parse(inpEl.options[idx].id); } catch(err) {}
    return id;
  }

  String get value {
    String rValue = "";
    try { rValue = inpEl.options[idx].text; } catch (err) { rValue = ""; }
    return rValue;
  }

  void set value(String val) {
    if (onChg != null) onChg(val);
    int idx = 0;
    inpEl.options.forEach((el) {
      if (el.value == val) idx = el.index;
    });
    inpEl.selectedIndex = idx;
    calcSpan();
  }

  int get idx {
    int idx = inpEl.selectedIndex;
    if (idx < 0) idx = 0;
    return idx;
  }

  int get maxSize {
    int cSize = 0;
    inpEl.options.forEach((el) {
      int s = el.value.length;
      if (s > cSize) cSize = s;
    });
    return cSize;
  }


  void calcSpan() {
    if (!_hidden) return;

    num size = maxSize*0.45;
    num size2 = maxSize*0.9;

    String txt = value;
    if (txt.length > size2) {
      _spEl.text = txt.substring(0, size2.round());
      _spEl.title = txt;
    } else {
      _spEl.text = value;
      _spEl.title = "";
    }
    setSpan(size);
  }

  TrSelect ({String fCol, String bCol}) {
    //Init parameters
    if (fCol == null) fCol = "";
    if (bCol == null) bCol = "";
    this.fCol = fCol;
    this.bCol = bCol;

    inpEl = new SelectElement();

    /* inpEl.onSelect
  inpEl.onKeyDown
  inpEl.onChange */

    _ev.add(inpEl.onKeyDown.listen((Event ev) {
      //Do action on enter key if preset
      KeyEvent keyEvent = new KeyEvent.wrap(ev as KeyboardEvent);
      if (keyEvent.keyCode == KeyCode.ENTER) {
        if (onEnter != null) {
          ev.preventDefault();
          onEnter();
        }
      }
    }));

    _ev.add(inpEl.onSelect.listen((Event ev) {
      _act.modify();
    }));

    _ev.add(inpEl.onChange.listen((Event ev) {
      if (onChg != null) onChg(value);
      _act.modify();
    }));

    setVis();
  }


}

class TrInput extends TrElem {
  num _size = 1;

  InputElement get inpEl => (_inpEl as InputElement);
  void set inpEl(InputElement el) {
    _inpEl = el;
  }

  String toString() {
    return '{"value": $value, "saved": ${_act.saved}, "okMod": ${_act.okMod}, "hidden": $hidden  }';
  }

  void calcSpan() {
    if (inpEl.size > _size) _size = inpEl.size;
    else inpEl.size = _size.round();

    num size = _size*0.6;
    num size2 = _size;

    String txt = inpEl.value;
    if (txt.length > size2) {
      if (_validator ==  7) {
        try {
          num bsize = txt.indexOf(" ");
          if (bsize > 1) size2 = bsize;
          else size2 = txt.length;
        } catch (err) {}
        size = size2/1.4;
      }

      if (size2 < 1) size2 = 1;
      _spEl.text = txt.substring(0, size2.round());
      _spEl.title = txt;
    } else {
      _spEl.text = inpEl.value;
      _spEl.title = "";
    }
    setSpan(size);
  }

  int _validator = 0; // 1- phoneVal; 2-CapitalLetters; 3-smallLetters; 4-numbersOnly; 5 - serial Number
  //6 - EMail; 7 - Date; 8 - numbers plus dot, 9 serial number with wildcard, 10 number with wildcard ...
  String addMap = "";


  void set validator(int val) {
    _validator = val;
    applyVal();
  }

  bool zeroMail(String el) {
    if (el.length > 0) return Validators.isEmail(el);
    else return true;
  }

  bool zeroDate(String el) {
    if (el.length > 0) return Validators.isDate(el);
    else return true;
  }

  void applyVal() {
    switch (_validator) {
      case 1:
        inpEl.value = Validators.phoneVal(inpEl.value);
        break;
      case 2:
        inpEl.value = inpEl.value.toUpperCase();
        break;
      case 3:
        inpEl.value = inpEl.value.toLowerCase();
        break;
      case 4:
        inpEl.value = Validators.numVal(inpEl.value,addMap:addMap);
        break;
      case 5:
        inpEl.value = Validators.snVal(inpEl.value);
        break;
      case 6:
        if (!zeroMail(inpEl.value)) showMessage("Невалиден Email!");
        break;
      case 7:
        if (!zeroDate(inpEl.value)) showMessage("Невалидна Дата!");
        break;
      case 8:
        inpEl.value = Validators.numValDec(inpEl.value);
        break;
      case 9:
        inpEl.value = Validators.snVal(inpEl.value, wldCard:true);
        break;
      case 10:
        inpEl.value = Validators.numVal(inpEl.value,addMap:"%"+addMap);
        break;
    }
    if (_spEl != null) calcSpan();
  }

  String get value {
    String outVal;
    switch (_validator) {
      case 1:
        outVal = Validators.stripSplitter(inpEl.value);
        break;
      case 2:
        outVal = inpEl.value.toUpperCase();
        break;
      case 3:
        outVal = inpEl.value.toLowerCase();
        break;
      case 4:
        outVal = Validators.numVal(inpEl.value, addMap:addMap);
        break;
      case 5:
        outVal = Validators.snVal(inpEl.value);
        break;
      case 6:
        if (!zeroMail(inpEl.value)) showMessage("Невалиден Email!");
        outVal = inpEl.value;
        break;
      case 7:
        if (!zeroDate(inpEl.value)) showMessage("Невалидна Дата!");
        outVal = inpEl.value;
        break;
      case 8:
        outVal = Validators.numValDec(inpEl.value);
        break;
      case 9:
        outVal = Validators.snVal(inpEl.value,wldCard:true);
        break;
      case 10:
        outVal = Validators.numVal(inpEl.value,addMap:"%"+addMap);
        break;
      default:
        outVal = inpEl.value;
    }
    return outVal;
  }



  TrInput ({bool pass, String fCol, String bCol}) {
    //Init parameters
    if (fCol == null) fCol = "";
    if (bCol == null) bCol = "";
    this.fCol = fCol;
    this.bCol = bCol;

    if (pass == null) pass = false;
    if (pass) inpEl = new PasswordInputElement() as InputElement;
    else inpEl = new InputElement();

    _ev.add(inpEl.onKeyDown.listen((KeyboardEvent ev) {
      //Do action on enter key if preset
      KeyEvent keyEvent = new KeyEvent.wrap(ev);
      if (keyEvent.keyCode == KeyCode.ENTER) {
        if (onEnter != null) {
          ev.preventDefault();
          onEnter();
        }
      }
    }));

    _ev.add(inpEl.onInput.listen((Event ev) {
      _act.modify();
    }));

    _ev.add(inpEl.onChange.listen((Event ev) {
      if (onChg != null) onChg(value);
      applyVal();
      _act.modify();
    }));

    setVis();
  }


  void set value(String val) {
    if (onChg != null) onChg(val);
    _act.modify();
    inpEl.value = val;
    applyVal();
  }

}

//---------------------------------------------------
class TrInputVarLen extends TrInput {
  TrInputVarLen _twinElem;

  void set twinElem(TrInputVarLen tw) {
    _twinElem = tw;
  }
  TrInputVarLen get twinElem => _twinElem;

  TrInputVarLen(TrInputVarLen twinElem, {bool pass, String fCol, String bCol}) : super (pass:pass, fCol:fCol, bCol:bCol) {
    _twinElem = twinElem;
  }

  void calcSpan() {
    if (inpEl.size <= 0) inpEl.size = 1;
    num size = inpEl.size*0.6;
    num size2 = inpEl.size;
    String txt = inpEl.value;

    if (_twinElem != null) {
      //Take a size from other value if possible
      num othSize = _twinElem.inpEl.size;
      int othSize2 = _twinElem.value.length;
      num diff = (othSize - othSize2);
      //print ("$size+$diff");

      if (diff > 1) {
        //Can take size from partner
        //?need size
        if (txt.length > size2) {
          //Take size
          size += (diff-1)*0.6;
          size2 += (diff-1);
        }
      }
      else
      if ((txt.length+1) < size2) // if our length is smaller than space - give up space to other
        size += (txt.length-size2+1)*0.6;
    }

    if (txt.length > size2) {
      if (_validator ==  7) {
        try { size2 = txt.indexOf(" "); } catch (err) {}
        size = size2/1.4;
      }

      if (size2 < 1) size2 = 1;
      _spEl.text = txt.substring(0, size2.round());
      _spEl.title = txt;
    } else {
      _spEl.text = inpEl.value;
      _spEl.title = "";
    }

    setSpan(size);
  }


}
