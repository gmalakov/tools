part of vistemplates;

class LogView {
  String _curLog = "";
  static String wName = "logWin";
  DWindow _win = null;
  List<StreamSubscription> _ev = new List();
  Map<String, dynamic> _el = new Map();
  bool popwin = false;
  ToolTip _toolTip;

  String get log => _curLog;
  void set log (String log) {
    _curLog += "\n";
    _curLog += "["+Dates.bgDateFull(new DateTime.now().toString())+"] ";
    _curLog += log;
    drawLog();
    if (!popwin) showToolTip(log);
  }

  void showToolTip(String lastLog) {
    if (_toolTip == null) _toolTip = new ToolTip(lastLog, bottom:6, right:2, autohide:false, onClick:(_) => _win.focusWin());
    else {
      //Tooltip is created
      _toolTip.text = lastLog;
      if(!_toolTip.visible) _toolTip.appear(); //Tooltip is invisible then make it appear
    }
  }

  static LogView _lv = new LogView();
  static LogView get instance => _lv;

  DivElement drawClear() {
    DivElement dElem = new DivElement();
    dElem
      ..className = "YNDiv"
      ..id = "clearLog"
      ..style.width = "100%"
      ..style.textAlign = "center";

    AnchorElement aElem = new AnchorElement();
    aElem
      ..text = "Изчисти"
      ..id = "save_usrComAnch"
      ..style.cursor = "pointer";

    _ev.add(dElem.onClick.listen((ev) {
      clearLog();
    }));
    _el["clDiv"] = dElem;
    _el["clAn"] = aElem;

    dElem.append(aElem);
    return dElem;
  }


  void drawLog() {
    //Display window if not exists and draw log into view
    if (TaskBar.tBar != null) _win = TaskBar.tBar.windows[wName];
    bool noInit = false;

    WinStatus wsc = WinStatus.regular;
    if (!popwin) wsc = WinStatus.minimized;

    if (_win == null) {
      if (TaskBar.tBar != null)
        _win = TaskBar.tBar.addWin(10,10,80,65,"Възникнали грешки",wsc,wName);
      else _win = new DWindow(10,10,80,65,"Възникнали грешки",wsc);
      _win.onClose = remove;
      noInit = true;
    }

    //Should draw Log view if not initialized
    if (noInit) {
      _el["logView"] = (new CommentBox("Грешки:", "logView")
        .._tEl.disabled = true);
      _win.addObj((_el["logView"] as CommentBox).inDiv);
      _win.addObj(drawClear());
      _win.escapeClose = true;
      _win.onAppear = () =>_el["logView"].text = _curLog;
    } else  {
      _el["logView"].text = _curLog;
      if (popwin) _win.focusWin(); //Focus current window if popwin flag
    }
  }

  void remove() {
    _ev.forEach((StreamSubscription ev) => ev.cancel());
    _el.forEach((String n, dynamic el) => el.remove());
    _win = null;
  }

  void removeWin() {
    if (_win != null) _win.removeWin();
    remove();
  }

  void clearLog() {
    _curLog = "";
    removeWin();
  }
}
