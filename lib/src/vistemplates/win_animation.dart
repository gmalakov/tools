part of vistemplates;

class WinAnimation {
  static bool doAnimation = true;
  num _x, _y, _w, _h;
  int step = 25;
  DivElement _anDiv = null;

  WinAnimation(this._x, this._y, this._w, this._h);

  bool get inAnim => (_anDiv != null);

  void drawDivFade(num progress) {
    _anDiv.style.opacity = (0.3 + 0.7*progress/100).toString();
  }

  void drawDiv(bool maximized,bool transition, num progress) {
    if (_anDiv == null) return;

    num w = _w; num h = _h;
    num x = _x; num y = _y;
    if (maximized) {
      x = 0; y = 0;
      w = 100; h = 94;
    }

    num cW; num cH;
    num cL; num cT;

    if (transition) {
      cW = _w + ((100-_w)*progress)~/100;
      cH = _h + ((94-_h)*progress)~/100;
      cL = _x + _w~/2 - cW~/2;
      cT = _y + _h~/2 - cH~/2;
    } else {
      cW = (w*progress)~/100;
      cH = (h*progress)~/100;
      cL = x + w~/2 - cW~/2;
      cT = y + h~/2 - cH~/2;
    }

    _anDiv.style
      ..left  = cL.toString()+"%"
      ..top = cT.toString()+"%"
      ..width = cW.toString()+"%"
      ..height = cH.toString()+"%"
      ..opacity = (0.3 + 0.7*progress/100).toString();


  }

  void initDiv() {
    _anDiv = new DivElement();
    _anDiv.className = "trWin";
    document.body.append(_anDiv);
  }

  void genAnim(bool maximized, bool transition, bool posStep, Function toDo) {
    if (!doAnimation) {
      toDo();
      return;
    }

    if (_anDiv != null) {
      //Another animation in progress for this window
      if (toDo != null) toDo();
      return;
    }
    initDiv();

    //Draw initial div if negative step
    if (posStep) drawDiv(maximized,transition,0);
    else drawDiv(maximized,transition,100);

    num progress;
    if (posStep) progress = step;
    else progress = 100-step;

    Timer timer;

    timer = new Timer.periodic(const Duration(milliseconds:60), (tim) {
      drawDiv(maximized,transition, progress);

      if (posStep) progress += step;
      else progress -= step;

      //When finished
      if ((progress >= 100) || (progress <= 0)) {
        timer.cancel();
        _anDiv.remove();
        _anDiv = null;
        if (toDo != null) toDo();
      }
    });
  }

  void maximize(Function toDo) {
    genAnim(true,true,true,toDo);
  }

  void regular(Function toDo) {
    genAnim(false,true,false,toDo);
  }

  void disapper(bool maximized,Function toDo) {
    genAnim(maximized, false, false, toDo);
  }

  void appear(bool maximized,Function toDo) {
    genAnim(maximized, false, true, toDo);
  }
}
