part of vistemplates;

class FormTypes {
  Map<String, Function> _fTypes = null;
  Map<String,String> fids = null;
  int curId = 0;
  ContextMenu ctMenu = null;

  FormTypes() {
    _fTypes = new Map();
    fids = new Map();
    ctMenu = new ContextMenu();
    ctMenu.items = _fTypes;
  }

  void addForm(String id, String name,Function doSomething) {
    fids[name] = id;
    _fTypes[name] = doSomething;
  }


  Map get fTypes => _fTypes;

}
