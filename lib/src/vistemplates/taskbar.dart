part of vistemplates;

class TaskBar extends TaskApp {
  StartButton _stButton = new StartButton();
  DivElement _container = new DivElement();
  List _ev = new List<StreamSubscription>();

  static TaskBar _tBar = null;
  static TaskBar get tBar => _tBar;

  String focused = "";
  Map<String,List<StreamSubscription>> _events = new Map();

  void addStartOption(String oName,Function doThing) {
    _stButton.ctMenu.items[oName] = doThing;
  }

  void remStartOption(String oName) {
    _stButton.ctMenu.items.remove(oName);
  }

  void focusWin(String id) {
    focused = id;
    windows[id].focusWin();
    _bars[id].style.border = "1px inset #fff";
  }

  void minimizeWin(String id) {
    windows[id].minimizeWin();
    _bars[id].style.border = "1px outset #fff";
  }

  void maximizeWin(String id) {
    windows[id].maximizeWin();
    if (_bars[id] != null) _bars[id].style.border = "1px inset #fff";
  }

  void unFocusWins(String id) {
    windows.forEach((winName,window) {
      if (winName != id) {
        window.unFocusWin();
        if (_bars[winName] != null) _bars[winName].style.border = "1px outset #fff";
      }
    });

    focused = id;
    if (_bars[id] != null) _bars[id].style.border = "1px inset #fff";
  }

//Taskbar handlers
  void focusCWin(MouseEvent ev) {
    ev.preventDefault(); ev.stopImmediatePropagation();
    DivElement curTask = ev.currentTarget as DivElement;
    String id = curTask.id;
    try {
      id = id.substring(5, id.length);
    } catch (err) { id = ""; return; }

    if (focused != id)  focusWin(id);
    else {
      minimizeWin(id);
      focused = "";
    }

  }

  DivElement genBar(String wTitle,String wname) {
    if (_ibars[wname] == null) {
      _ibars[wname] = new DivElement();
      _ibars[wname].className = "taskBarItem";
      _ibars[wname].id = "task_"+wname;

      _titles[wname] = new SpanElement();
      _titles[wname]
        ..style.cursor = "pointer"
        ..text = wTitle;

      _ibars[wname].append(_titles[wname]);
    }


    return _ibars[wname];
  }

  void wTitle(String wname, String wTitle) {
    windows[wname].winTitle = wTitle;
    _titles[wname].text = wTitle;
  }

  void addBar(String wname) {
    String wtitle = windows[wname].winTitle;
    _bars[wname] = new DivElement();
    _bars[wname].id = "оtask_"+wname;
    _bars[wname].className = "taskbarItemHolder";

    _bars[wname].append(genBar(wtitle,wname));

    _closes[wname] = new SpanElement();
    _closes[wname]
      ..className = "barClose"
      ..id = "bar_close_$wname";

    ImageElement imgEl = new ImageElement()
      ..src = "svg/power18.svg"
      ..style.width = "15px"
      ..style.height = "15px";
    _closes[wname].append(imgEl);

    _bars[wname].append(_closes[wname]);

    _events[wname] = new List<StreamSubscription>();
    _events[wname].add(_ibars[wname].onClick.listen(focusCWin));
    _events[wname].add(_closes[wname].onClick.listen((ev) {
      removeWin(wname);
    }));

    //Add to taskbar container
    _container.append(_bars[wname]);
    redrawBar();
  }

  void removeBar(String wname) {
    _closes[wname].remove();
    _closes.remove(wname);
    _titles[wname].remove();
    _titles.remove(wname);
    _ibars[wname].remove();
    _ibars.remove(wname);
    _bars[wname].remove();
    _bars.remove(wname);

    _events[wname].forEach((StreamSubscription el) => el.cancel());
    _events[wname].clear();
    _events.remove(wname);

    redrawBar();
  }

  void redrawBar() {
    int size = _bars.length;
    // width -> (100% -  start button size) / size
    // offset -> (start button size + (size -1) * width

    if (size > 0) {

      int width = (92~/size)-1;
      int count = 0;

      _bars.forEach((wname,bar) {
        int left = 7+count*(width+1);
        _bars[wname].style
          ..top = "0.4vh"
          ..left = left.toString()+"%"
          ..width = width.toString()+"%"
          ..height = "70%"
          ..position = "absolute"
          ..border = "1px outset #fff";
        count++;
      });
    }
  }


  DWindow addWin(num x,num y,num w,num h,String wtitle, [WinStatus winStatus, String wName]) {
    if (wName != null)
      if (windows[wName] != null) return null;

    if (winStatus == null) winStatus = WinStatus.regular;
    DWindow dWin = new DWindow(x,y,w,h,wtitle,winStatus,this);
    if (wName == null) wName = dWin.winId;
    else dWin.winId = wName;
    windows[wName] = dWin;
    addBar(wName);
    if ((winStatus != WinStatus.minimized) && (winStatus != WinStatus.shaded)) focusWin(wName);
    return dWin;
  }


  void removeWin(String id) {
    if (windows[id] != null) {
      windows[id].remove();
      windows.remove(id);

      //Drop zIndex of all other windows to match one less window
      windows.forEach((n,win) => win.unFocusWin());
      removeBar(id);
    }
  }

  String winName(Element el) {
    bool match = false;
    if (el != null) focusedEl = el;

    while ((el != null) && (!match)) {
      if ((el.id != null) && (el.id.length > DWindow.winPre.length) &&
          (el.id.substring(0,DWindow.winPre.length) == DWindow.winPre)) match = true;
      else el = el.parent;
    }

    if ((el != null) && (el.id.length > DWindow.winPre.length) &&
        (el.id.substring(0, DWindow.winPre.length) == DWindow.winPre))  {
      String id = el.id;
      String wName;

      windows.forEach((n,el) {
        if (el.id == id) {
          //Get Active window name
          wName = n;
        }
      });

      return wName;
    } else return null;
  }

  void clickHandler(MouseEvent ev) {
    Element el = ev.target as Element;
    String wName = winName(el);

    if ((wName != null) && (wName != focused)) {
      //ev.preventDefault(); ev.stopImmediatePropagation();
      // Should execute and command that's in window if any
      focusWin(wName);
    }
/*    else {
     windows[wName].focusInDiv();
   } */
  }

  String get focusedWin {
    //Get windows with maximum zIndex -> focused Window
    String fWin;
    int maxIdx = -1;
    windows.forEach((n, win) {
      if (win.zIndex > maxIdx) {
        maxIdx = win.zIndex;
        fWin = n;
      }
    });

    return fWin;
  }

  void triggerKeys(KeyboardEvent ev) {
    //Wrap Key
    KeyEvent keyEvent = new KeyEvent.wrap(ev);
    //get Extended Keycode
    int extCode = KeyCodes.keyCode(keyEvent);
    Function wAction;

    //Window specific button actions
    //get focused window
    if (focused == "") focused = focusedWin;
    DWindow fWin = windows[focused];
    //Get window action for this keycode if any
    if ((fWin != null) &&
        (!fWin.minimized) &&
        (!fWin.shaded))
      wAction = fWin.getButton(extCode);
    //Run window action for this keycode if any
    if (wAction != null) {
      wAction();
      ev.preventDefault(); ev.stopImmediatePropagation();
      return;
    }

    //Global button actions
    //get Action
    wAction = getButton(extCode);
    //Run Global action for this keycode if any
    if (wAction != null) {
      wAction();
      ev.preventDefault(); ev.stopImmediatePropagation();
      return;
    }

    //Deal window escape code
    if (keyEvent.keyCode == KeyCode.ESC)  {
      //If escape window minimization is enabled - minimize focused window
      if ((windows[focused].escapeMin) || (windows[focused].escapeClose)) {
        if (windows[focused].escapeMin) minimizeWin(focused);
        if (windows[focused].escapeClose) removeWin(focused);
        focused = "";
        ev.preventDefault(); ev.stopImmediatePropagation();
      }
    }
  }

  TaskBar() {
    _container.id = "taskBar";
    document.body.append(_container);
    _container.append(_stButton.startContainer);

    _ev.add(document.body.onKeyDown.listen(triggerKeys));
    _ev.add(document.body.onClick.listen(clickHandler));

    //Register global instance
    _tBar = this;
  }
}
