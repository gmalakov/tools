part of vistemplates;

abstract class GenTable<F extends FormTypes> {
  Map<int,TableRowElement> rows = new Map();
  F fTypes;
  String hiLightCol = "76FFFF";

  List<StreamSubscription> evCtx = new List();
  List<StreamSubscription> evSet = new List();
  List<StreamSubscription> evLight = new List();
  List<StreamSubscription> evUnLight = new List();
  List<StreamSubscription> othEvs = new List();

  Map<int,TableCellElement> ccallers = new Map();
  Map<int,TableCellElement> stcallers = new Map();

  int oldId = 0;
  String rowPre = "genTableRow_";
  String rowCtx = "forms_call_";

  void lightRow(int id) {
    try {
      List<Element> oRow = querySelector("#"+rowPre+id.toString()).children;
      oRow.forEach((Element el) {
        if (el.className != "datesCellRed") el.style.backgroundColor = "#"+hiLightCol;
      });
    } catch (err) {}
  }

  void unLightRow(int id) {
    try {
      List<Element> oRow = querySelector("#"+rowPre+id.toString()).children;
      oRow.forEach((Element el) {
        if (el.className != "datesCellRed") el.style.backgroundColor = "";
      });
    } catch (err) {}
  }

  int get selField;

  void repLight(MouseEvent ev) {
    Element el = (ev.currentTarget as Element);
    //String pre = "reptable_";
    int id = -1;
    try {
      id = int.parse(el.id.substring(rowPre.length,el.id.length));
    } catch(err) {}

    if (id > -1) {
      ev.preventDefault(); ev.stopImmediatePropagation();
      lightRow(id);
    }
    oldId = id;

  }

  void repUnLight(MouseEvent ev) {
    Element el = (ev.currentTarget as Element);
    //String pre = "reptable_";
    int id = -1;
    try {
      id = int.parse(el.id.substring(rowPre.length,el.id.length));
    } catch(err) {}

    if (oldId == id) {
      //if (color == "") color = "#"+Globals.instance.vars["normalCol"];

      int sel = selField;
      if (sel != oldId) {
        ev.preventDefault(); ev.stopImmediatePropagation();
        unLightRow(id);
      }
    }

  }

  void setRep(int cId) {
    int oId = fTypes.curId;
    fTypes.curId = cId;

    unLightRow(oId);
    lightRow(cId);
  }

  void openCloseMenu(MouseEvent ev, { int cId }) {
    ev.preventDefault(); ev.stopImmediatePropagation();

    if (fTypes.ctMenu.shown) {
      fTypes.ctMenu.hideMenu();
      return;
    }

    Element el = (ev.currentTarget as Element);
    String curId  = el.id;

    //Get row id
    // forms_call_id
    if (cId == null) {
      try {
        if (el.id != "") curId = el.id.substring(rowCtx.length, el.id.length);
        cId = int.parse(curId);
      } catch(err) {}
    }

    fTypes.curId = cId;
    if (cId != null) setRep(cId);

    int posXP = ((ev.screen.x*100)~/window.innerWidth);
    int posYP = ((ev.screen.y*100)~/window.innerHeight)-4; //-4% for window title and so on

    int cWidth = fTypes.ctMenu.mWidth; //context menu width
    int cHeight = fTypes.ctMenu.mHeight; // context menu Height

    int posWinX = null; int posWinY = null;
    int posWinXR = null; int posWinYR = null;
    if ((posXP + cWidth) < 100) posWinX = posXP;
    else posWinXR = posXP - (cWidth+1);
    if ((posYP + cHeight) < 100) posWinY = posYP;
    else posWinYR = posYP - (cHeight+1);

    if (!fTypes.ctMenu.shown) {
      fTypes.ctMenu.showMenu(el,left:posWinX,top:posWinY,bottom:posWinYR, right:posWinXR);
    }
  }

  void installMenuHandler () {
    rows.forEach((id,el) => evLight.add(el.onMouseOver.listen(repLight)));
    rows.forEach((id,el) => evUnLight.add(el.onMouseOut.listen(repUnLight)));
  }

  void fillList();
  void fillTable();

  void cancelEvents() {
    evCtx.forEach((StreamSubscription ev) => ev.cancel());
    evCtx.clear();
    evSet.forEach((StreamSubscription ev) => ev.cancel());
    evSet.clear();
    evLight.forEach((StreamSubscription ev) => ev.cancel());
    evLight.clear();
    evUnLight.forEach((StreamSubscription ev) => ev.cancel());
    evUnLight.clear();

    othEvs.forEach((StreamSubscription ev) => ev.cancel());
    othEvs.clear();
  }

}

