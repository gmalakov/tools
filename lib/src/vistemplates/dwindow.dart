part of vistemplates;

class DWindow extends AppElement {
  static const winPre = "window_";
  static bool anim = true;

  num _x, _y, _w, _h;
  List<StreamSubscription> _events = new List();
  List<StreamSubscription> _eventsDyn = new List();
  String winTitle = "";
  TaskApp _tapp;
  Element focusEl;

  //Overflows
  bool _overFlow = true;
  bool _overFlowX = true;
  bool get overFlow => _overFlow;
  bool get overFlowX => _overFlowX;

  void set overFlow(bool ov) {
    _overFlow = ov;
    if (_inContainer == null) return;
    if (ov) _inContainer.style.overflow = "auto";
    else _inContainer.style.overflow = "hidden";
  }

  void set overFlowX(bool ov) {
    _overFlowX = ov;
    if (_inContainer == null) return;
    if (ov) _inContainer.style.overflowX = "auto";
    else _inContainer.style.overflowX = "hidden";
  }

  int get scrollTop => _inContainer.scrollTop;
  void set scrollTop(int val) => _inContainer.scrollTop = val;

  //Escape Actions
  EscAction _escAct = EscAction.None;
  bool get escapeMin => (_escAct == EscAction.EscMinimize);
  bool get escapeClose => (_escAct == EscAction.EscClose);

  void set escapeMin(bool val) {
    (val) ? _escAct = EscAction.EscMinimize : _escAct = EscAction.None;
  }
  void set escapeClose(bool val) {
    (val) ? _escAct = EscAction.EscClose : _escAct = EscAction.None;
  }
  //---------------

  String get id => (_container.id);

  static const baseIndex = 50;

  Function onClose = null;
  Function onAppear = null;

  WinStatus _winStatus = WinStatus.regular;
  //States
  bool _moveWin = false;
  bool _resizeWin = false;
  bool _lresizeWin = false;
  DeBounce _dbounce;

  bool _resizable = true;
  bool _moveable = true;
  bool _closeable = true;
  bool _maximizeable = true;

  num _stX = 0;
  num _stY = 0;
  DivElement _container = null;
  DivElement _inContainer = null;

  DivElement get inDiv => _inContainer;

  DivElement _titleContainer = null;
  DivElement _closeContainer = null;
  DivElement _shadeContainer = null;
  DivElement _resizeContainer = null;
  DivElement _lresizeContainer = null;

  //WinAnimation _anim = null;

  static int lastId = 0;
  String winId = "";

  void set app(TaskApp app) {
    _tapp = app;
  }

  bool get inAnim {
    return false;
    //if (_anim != null) return _anim.inAnim;
    //else return false;
  }

  DWindow(num x,num y,num w,num h,String winTitle,[WinStatus winStatus, TaskApp tapp]) {
    _x = x; _y = y; _w = w; _h = h;
    //_anim = new WinAnimation(_x, _y, _w, _h);
    this.winTitle = winTitle;
    if (winStatus == null) _winStatus = WinStatus.regular;
    else _winStatus = winStatus;

    if (tapp != null) _tapp = tapp;

    _container = new DivElement();
    _container.style
      ..display = "none";
//      ..transform = "scale(0,0)";
    document.body.append(_container);

    //Initialize window unique id
    lastId++;
    winId = winPre+lastId.toString();

    _container
      ..style.position = "absolute"
      ..style.left   = _x.toString()+"%"
      ..style.top    = _y.toString()+"%"
      ..style.width  = _w.toString()+"%"
      ..style.height = _h.toString()+"%"
      ..style.zIndex = "50"
      ..className = "window"
      ..id = winId;

    _titleContainer = new DivElement();
    _titleContainer
      ..className = "windowTitle"
      ..id = "title_$winId"
      ..style.cursor = "move"
      ..appendHtml(winTitle);

    //Window right side buttons
    DivElement _rightContainer = new DivElement();
    _rightContainer
      ..id="rightContainer_$winId"
      ..className = "windowRight";

    _shadeContainer = new DivElement();
    _shadeContainer
      ..className = "windowClose"
      ..id = "shade_$winId"
      ..style.float = "left";
    ImageElement imgEl = new ImageElement()
      ..src = "svg/arrowup.svg"
      ..style.width = "14px"
      ..style.height = "14px";
    _shadeContainer.append(imgEl);

    _closeContainer = new DivElement();
    _closeContainer
      ..className = "windowClose"
      ..id = "close_$winId"
      ..style.float = "right";
    imgEl = new ImageElement()
      ..src = "svg/power18.svg"
      ..style.width = "14px"
      ..style.height = "14px";
    _closeContainer.append(imgEl);

    _rightContainer.append(_shadeContainer);
    _rightContainer.append(_closeContainer);
    //----------------------------------

    _container.append(_titleContainer);
    _container.append(_rightContainer);

    _inContainer = new DivElement();
    _container.append(_inContainer);
    _inContainer.style
      ..width = "100%"
      ..height = "94%"
      ..overflow = "auto"
      ..overflowX = "auto";

    _resizeContainer = new DivElement();
    _container.append(_resizeContainer);
    _resizeContainer
      ..id = "resize_$winId"
      ..style.position = "absolute"
      ..style.bottom = "0px"
      ..style.right = "0px"
      ..style.cursor = "se-resize"
      ..appendHtml("_|")
      ..style.zIndex = (51+lastId).toString();

    _lresizeContainer = new DivElement();
    _container.append(_lresizeContainer);
    _lresizeContainer
      ..id = "lresize_$winId"
      ..style.position = "absolute"
      ..style.bottom = "0px"
      ..style.left = "0px"
      ..style.cursor = "sw-resize"
      ..appendHtml("|_")
      ..style.zIndex = (51+lastId).toString();

    //On creation shouldn't be necessary to hide window initiallly hidden
    //if (_winStatus == WinStatus.minimized) hideWin();
    if (_winStatus == WinStatus.maximized) drawMaxWin(); //_anim.appear(true, drawMaxWin);
    if (_winStatus == WinStatus.regular) displayWin(); //_anim.appear(false, displayWin);
    if (_winStatus == WinStatus.shaded) shadeWin();

    //Window Static Events
    _events.add(_closeContainer.onClick.listen(closeUWin));
    _events.add(_shadeContainer.onClick.listen(shadeUnshadeWin));
    _events.add(_titleContainer.onDoubleClick.listen(maximizeRegularWin));
    _events.add(_titleContainer.onMouseDown.listen(stmoveWin));
    _events.add(_resizeContainer.onMouseDown.listen(stresizeWin));
    _events.add(_lresizeContainer.onMouseDown.listen(stlresizeWin));
    _events.add(_titleContainer.onClick.listen(focusUWin));
    _events.add(_titleContainer.onClick.listen(focusUWin));
  }

  void addButton(int butId, Function callBack, {String descr}) {
    super.addButton(butId, callBack, descr: descr);
    if (descr != null) winHover = butDescr;
  }

  void removeButton(int budId) {
    super.removeButton(budId);
    winHover = butDescr;
  }

  void scrollUp() {
    _inContainer.scrollTop = 0;
  }

  void shadeWin() {
    _winStatus = WinStatus.shaded;
    _container.style.height = "16px";
    _container.style.overflow = "hidden";
    _inContainer.style.display = "none";
    _resizeContainer.style.display = "none";
    _lresizeContainer.style.display = "none";
    _container.style.display = "block";
  }

  void appearWin() {
    void doAppear() {
      //_container.style.transform = "scale(1,1)";
      if (TaskBar.tBar != null) focusWin();
      if (onAppear != null) onAppear();
    }

    if (anim) {
      new Timer(const Duration(milliseconds:20), doAppear);
      _container.classes.add("bounce-target");
    }
    else doAppear();
  }

  void displayWin() {
    //restore window size and position
    _container.style
      ..left = _x.toString()+"%"
      ..top = _y.toString()+"%"
      ..width = _w.toString()+"%"
      ..height = _h.toString()+"%"
      ..display = "block"
      ..overflow = "hidden";

    appearWin();

    _inContainer.style.display = "block";
    //Do not display resize containers if not resizable
    if (_resizable) {
      _resizeContainer.style.display = "block";
      _lresizeContainer.style.display = "block";
    }

  }

  void regularWin() {
    if ((_winStatus == WinStatus.minimized) || (_winStatus == WinStatus.shaded)) {
      //hideWin();
      _winStatus = WinStatus.regular;
      //_anim.appear(false, displayWin);
      displayWin();
    }
    else
    if (_winStatus == WinStatus.maximized) {
      //hideWin();
      _winStatus = WinStatus.regular;
      //_anim.regular(displayWin);
      displayWin();
    }
  }

  void hideWin ()  {
    _container.style.transform = "scale(0,0)";

    Timer tim;
    tim = new Timer(const Duration(milliseconds: 150), () {
      _container.style.display = "none";
      tim.cancel();
    });

    _inContainer.style.display = "none";
  }

  void minimizeWin() {
    hideWin();
/*  if (_winStatus == WinStatus.regular) _anim.disapper(false, null);
  else
    if (_winStatus == WinStatus.maximized) _anim.disapper(true, null); */
    _winStatus = WinStatus.minimized;
  }

  String calcZIndex() {
    int fIndex = baseIndex + _tapp.windows.length - 1;
    return fIndex.toString();
  }

  String dropZIndex() {
    int cIndex;
    try { cIndex = int.parse(_container.style.zIndex); } catch (err) { cIndex = baseIndex; }
    if (cIndex > baseIndex) cIndex--;
    return cIndex.toString();
  }

  bool get keepContainer {
    bool kContainer = false;
    //Focus container
    if ((_tapp != null) && (_tapp.focusedEl != null)) {
      for (int i = 0; i < TaskApp.importantEls.length; i++)
        if (_tapp.focusedEl.runtimeType == TaskApp.importantEls[i]) kContainer = true;
    }

    return kContainer;
  }

  void focusInDiv() {
    if (!keepContainer) {
      if (focusEl == null) inDiv.focus();
      else focusEl.focus();
    }
  }

  void focusWin() {
    if (_winStatus != WinStatus.maximized) regularWin();
    if (_tapp != null) {
      _tapp.unFocusWins(winId);
      _container.style.zIndex = calcZIndex();
    } else _container.style.zIndex = "100";

    //If container is not important to keep (input element) select window body
    focusInDiv();
  }

  void focusUWin(MouseEvent ev) {
    ev.preventDefault(); ev.stopImmediatePropagation();
    focusWin();
  }

  void unFocusWin() {
    _container.style.zIndex = dropZIndex();
  }

  void drawMaxWin() {
    //restore window size and position
    _container.style
      ..left = "0%"
      ..top = "0%"
      ..width = "100%"
      ..height = "96%"
      ..overflow = "auto"
      ..display = "block";

    appearWin();

    _inContainer.style.display = "block";
    _resizeContainer.style.display = "none";
    _lresizeContainer.style.display = "none";

  }

  void maximizeWin() {
    if (_winStatus != WinStatus.maximized) {
      //hideWin();
      //_anim.maximize(drawMaxWin);
      drawMaxWin();
    }
    _winStatus = WinStatus.maximized;

  }

  void shadeUnshadeWin(MouseEvent ev) {
    ev.preventDefault(); ev.stopImmediatePropagation();
    //If no regular or shaded function will pass without changes

    if (_winStatus == WinStatus.maximized) regularWin();
    else
    if (_winStatus == WinStatus.regular) {
      _winStatus = WinStatus.shaded;
      //hideWin();
      //_anim.disapper(false,shadeWin);
      shadeWin();
    }
    else regularWin();
  }

  void maximizeRegularWin(Event ev) {
    ev.preventDefault(); ev.stopImmediatePropagation();
//If no regular or shaded function will pass without changes

    if (_maximizeable) {
      //Reverse _shade status
      if (_winStatus == WinStatus.regular) maximizeWin();
      else
      if (_winStatus == WinStatus.maximized) regularWin();

    }
  }

  //--------------------- Move/Resize ----------------------------------------------
  void stActionWin(MouseEvent ev) {
    //Disable container transition effect
    _container.style.transition = "none";

    ev.preventDefault(); ev.stopImmediatePropagation();
    _stX = ev.screen.x;
    _stY = ev.screen.y;
    _eventsDyn.add(document.onMouseMove.listen(moveWin));
    _eventsDyn.add(document.onMouseUp.listen(relmoveWin));
  }

  void stmoveWin(MouseEvent ev) {
    ev.preventDefault(); ev.stopImmediatePropagation();
    if (_moveable) {
      _moveWin = true;
      stActionWin(ev);
    }
  }

  void stresizeWin(MouseEvent ev) {
    ev.preventDefault(); ev.stopImmediatePropagation();
    _resizeWin = true;
    stActionWin(ev);
  }

  void stlresizeWin(MouseEvent ev) {
    ev.preventDefault(); ev.stopImmediatePropagation();
    _lresizeWin = true;
    stActionWin(ev);
  }

  void relmoveWin(MouseEvent ev) {
    ev.preventDefault(); ev.stopImmediatePropagation();
    _eventsDyn.forEach((StreamSubscription ev) => ev.cancel());
    _moveWin = false; _resizeWin = false; _lresizeWin = false;

    //Debouncer responsible opacity restore
    new Timer(const Duration(milliseconds: 100), () {
      _container.style.opacity = "1";
    });

    _inContainer.style.display = "block";
    _stX = 0; _stY = 0;

    if (_winStatus != WinStatus.maximized) {
      //Strip percent and int-ify
      String left = _container.style.left.substring(0, _container.style.left.length-1);
      String top = _container.style.top.substring(0, _container.style.top.length-1);
      try { _x = int.parse(left); _y = int.parse(top); } catch (err) {}
    }

    if (_winStatus == WinStatus.regular) {
      String width = _container.style.width.substring(0, _container.style.width.length-1);
      String height = _container.style.height.substring(0, _container.style.height.length-1);
      try { _w = int.parse(width); _h = int.parse(height); } catch (err) {}
    }

    //Return container transition to first state
    _container.style.transition = "";
  }

  void moveWin(MouseEvent ev) {
    if (_dbounce == null) _dbounce = new DeBounce(adaptMoveWin, dur: const Duration(microseconds: 500));
    _dbounce.exec(ev);
  }

  void adaptMoveWin(MouseEvent ev) {
    ev.preventDefault(); ev.stopImmediatePropagation();

    _container.style.opacity = ".65";
    //remove contents of window for better performance
    //_inContainer.style.display = "none";

    if ((_stX > 0) && (_stY > 0)) {
      //Difference between start drag and stop drag
      num difX = ev.screen.x - _stX;
      num difY = ev.screen.y - _stY;

      if (_moveWin) {
        //Calculate Percent
        num winWP = ((difX*100)/window.innerWidth)+_x;
        num winHP = ((difY*100)/window.innerHeight)+_y;

        _container.style.left = winWP.toString()+"%";
        _container.style.top = winHP.toString()+"%";
      }

      if (_resizeWin) {
        //Calculate Percent
        num winWP = ((difX*100)/window.innerWidth)+_w;
        num winHP = ((difY*100)/window.innerHeight)+_h;

        _container.style.width = winWP.toString()+"%";
        _container.style.height = winHP.toString()+"%";
      }

      if (_lresizeWin) {
        //Calculate Percent
        num winWP = _w-((difX*100)/window.innerWidth);
        num winWPx = ((difX*100)/window.innerWidth)+_x;
        num winHP = ((difY*100)/window.innerHeight)+_h;

        _container.style.width = winWP.toString()+"%";
        _container.style.left = winWPx.toString()+"%";
        _container.style.height = winHP.toString()+"%";

      }
    }
  }
//----------------------------------------------------------------
  void removeWin() {
    if (_tapp != null) _tapp.removeWin(winId);
    else closeWin();
  }

  void closeWin() {
    _events.forEach((StreamSubscription ev) => ev.cancel());
    if (onClose != null) onClose();
    hideWin();

    Timer tim;
    tim = new Timer(const Duration(milliseconds:150), () {
      _container.remove();
      clearButtons();
      tim.cancel();
    });
    //_anim.disapper((_winStatus == WinStatus.maximized), null);
  }

  void closeUWin(MouseEvent ev) {
    ev.preventDefault(); ev.stopImmediatePropagation();
    removeWin();
  }

  void remove () => closeWin();

  void addHTML (String html) {
    _inContainer.appendHtml(html);
  }

  void clearHTML() {
    _inContainer.innerHtml = "";
  }

  void addObj (Node obj) {
    _inContainer.append(obj);
  }

  void set setTitle(String winTitle) {
    this.winTitle = winTitle;
    _titleContainer.innerHtml = winTitle;
    _tapp.wTitle(winId, winTitle);
  }

  void set winHover(String wH) {
    _titleContainer.title = wH;
  }

/*bool _resizable = true;
bool _moveable = true;
bool _closeable = true;
bool _maximizeable = true */
  void set resizable(bool res) {
    if (!res) {
      _resizeContainer.style.display = "none";
      _lresizeContainer.style.display = "none";
    } else
    if (_winStatus == WinStatus.regular) {
      _resizeContainer.style.display = "block";
      _lresizeContainer.style.display = "block";
    }
    _resizable = res;
  }

  void set moveable(bool mov) {
    _moveable = mov;
  }

  void set closeable(bool close) {
    _closeable = close;
    if (_closeable) _closeContainer.style.display = "block";
    else _closeContainer.style.display = "none";
  }

  void set zIndex(int index) {
    _container.style.zIndex = index.toString();
  }

  int get zIndex {
    int idx;
    try { idx = int.parse(_container.style.zIndex); } catch (err) { idx = 0; }
    return idx;
  }

  void set maximizeable(bool max) {
    _maximizeable = max;
  }

  bool get resizable => _resizable;
  bool get moveable => _moveable;
  bool get closeable => _closeable;
  bool get maximizeable => _maximizeable;

  bool get regular   => (_winStatus == WinStatus.regular);
  bool get maximized => (_winStatus == WinStatus.maximized);
  bool get minimized => (_winStatus == WinStatus.minimized);
  bool get shaded    => (_winStatus == WinStatus.shaded);

}
