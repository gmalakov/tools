part of vistemplates;

class Validators {
  static const splitter = "-";
  static const digSplit = 3;
  static const snMinLen = 5;

  static bool isEmail(String em) {
    if (em.length == 0) return false;
    String p = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = new RegExp(p);
    return regExp.hasMatch(em);
  }

  static bool isDate(String inStr) {
    if (inStr.length == 0) return false; //Exit on null string
    inStr = inStr
        .replaceAll(',', '.')
        .replaceAll('-', '.'); //Replace delimiters if needed

    String eDate = Dates.engDateFull(inStr); // to english format date
    DateTime oDate = Dates.parseDate(eDate); //parse date

    if (oDate != null) return true;
    else return false;
  }


  //Digits 48-57
  //BigLetters 65-90
  //Allow % or *

  static String numVal(String inStr, {String addMap}) {
    //set state of undefined string
    if (addMap == null) addMap = "";
    List valMap = addMap.codeUnits;

    if (inStr.length == 0) return "";

    inStr = inStr.replaceAll("*", "%");
    List<int> inBytes = inStr.codeUnits;
    List<int> outBytes = new List();

    //If is a number add it to output list
    for (int i=0;i < inBytes.length;i++) {
      if ((inBytes[i] >= 48) && (inBytes[i] <= 57)) outBytes.add(inBytes[i]);

      //Enable searching for predefined symbols
      for (int j = 0; j < valMap.length; j++) {
        int ctr = 0;
        if ((inBytes[i] == valMap[j]) && (ctr == 0)) {
          outBytes.add(inBytes[i]);
          ctr++;
        }
      }
    }

    String outStr =  new String.fromCharCodes(outBytes);
    return outStr;
  }

  //Numeric value Decimal
  static String numValDec(String inStr) {
    if (inStr.length == 0) return "";
    inStr = inStr.replaceAll(",", ".");
    String outStr = numVal(inStr,addMap:".");
    return outStr;
  }

  static bool shitCheck(List<int> inBytes) {
    //Check for 0000 or 1111 or whatever shit !
    int lastNum;
    int same = 1; int next = 1; int before = 1;
    for (int i = 0; i < inBytes.length;i++) {
      if (lastNum != null) {
        if (lastNum == inBytes[i]) same++;
        if (lastNum+1 == inBytes[i]) next++;
        if (lastNum-1 == inBytes[i]) before++;
      }
/*      if ((lastNum != null) &&
          ((lastNum == inBytes[i]) || (lastNum+1 == inBytes[i]) || (lastNum-1 == inBytes[i])
          )) same++; // same digit or letter or next digit */
      lastNum = inBytes[i];
    }

    if ((same == inBytes.length) || (next == inBytes.length) ||
        (before == inBytes.length)) return true; //All the same numbers result empty string
    else return false;
  }

  static String snVal(String inStr, {bool wldCard}) {
    //Set state to false if undefined wildcard
    if (wldCard == null) wldCard = false;
    if (inStr.length == 0) return "";
    inStr = inStr.replaceAll("*", "%");
    //Capitalize first
    inStr = inStr.toUpperCase();
    List<int> inBytes = inStr.codeUnits;
    List<int> outBytes = new List();

    if (shitCheck(inBytes) || (inBytes.length < snMinLen)) return "";

    //If is a number add it to output list
    for (int i=0;i < inBytes.length;i++) {
      if (((inBytes[i] >= 48) && (inBytes[i] <= 57)) ||
          ((inBytes[i] >= 65) && (inBytes[i] <= 90))) outBytes.add(inBytes[i]);

      //if wildcard is enabled and char is % add it
      if  ((inBytes[i] == 37) && (wldCard)) outBytes.add(inBytes[i]);

      //print(inBytes[i]);
    }

    String outStr =  new String.fromCharCodes(outBytes);
    return outStr;
  }

  static String stripSplitter(String inStr) {
    if (inStr.length == 0) return "";
    inStr = inStr.replaceAll(splitter, "");
    if (inStr.length == 0) return "";

    //Remove first letter if zero
    if (inStr.substring(0,1) == "0") inStr = inStr.substring(1, inStr.length);
    if (inStr.length == 0) return "";

    //If is a number ,;/ add it to output list
    String outStr = numVal(inStr,addMap:",;/");
    return outStr;
  }

  static String phoneVal(String inPhone) {
    if (inPhone.length == 0) return "";
    inPhone = stripSplitter(inPhone);
    if (inPhone.length == 0) return "";

    int splitNum = inPhone.length ~/ digSplit;
    if ((splitNum * digSplit) < inPhone.length) splitNum++;
    for (int splitPos = 1; splitPos < splitNum; splitPos ++) {
      int cPos = splitPos*digSplit;
      //Current position + number of inserted splitters
      inPhone = inPhone.substring(0, cPos+splitPos-1)+splitter+
          inPhone.substring(cPos+splitPos-1,inPhone.length);
    }
    return inPhone;
  }

}
