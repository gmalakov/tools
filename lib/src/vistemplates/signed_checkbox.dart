part of vistemplates;

class SignedCheckBox {
  AnchorElement _aEl = new AnchorElement();
  CheckboxInputElement _cEl = new CheckboxInputElement();
  CheckboxInputElement get cBox => _cEl;
  AnchorElement get title => _aEl;
  List<StreamSubscription> _evs = new List();
  String bColor = "red";
  ActivityTrack _act = new ActivityTrack();
  bool _disabled = false;
  Function onClick = null;

  void set disabled(bool ck) {
    cBox.disabled = ck;
    _disabled = ck;
  }

  //set title color and checkbox
  void set checked(bool ck) {
    _act.modify();
    if (ck) {
      _aEl.style.backgroundColor = bColor;
      _cEl.checked = true;
    } else {
      _aEl.style.backgroundColor = "";
      _cEl.checked = false;
    }
  }

  void invert() {
    checked = !_cEl.checked;
  }

  //get status from checkbox
  bool get checked => _cEl.checked;

  static int curCount = 0;

  void checkUnCheckBox(MouseEvent ev) {
    //ev.preventDefault(); ev.stopImmediatePropagation();
    if (_disabled) return;
    _act.modify();
    CheckboxInputElement cEl = (ev.target as CheckboxInputElement);
    bool cchecked = cEl.checked;

    if (cEl == _cEl) {
      if (cchecked) _aEl.style.backgroundColor = bColor;
      else _aEl.style.backgroundColor = "";
    }
    if (onClick != null) onClick();
  }

  void checkUnCheckEl(MouseEvent ev) {
    ev.preventDefault(); ev.stopImmediatePropagation();
    if (_disabled) return;
    _act.modify();
    AnchorElement lEl = (ev.target as AnchorElement);

    bool cchecked = false;
    if (lEl == _aEl) cchecked = _cEl.checked;

    (cchecked) ? checked = false :  checked = true;
    if (onClick != null) onClick();
  }

  bool get okMod => _act.okMod;
  bool get saved => _act.saved;
  void zeroMod() => _act.zeroMod();
  void modify() => _act.modify();

  SignedCheckBox(String text,[bool isChecked]) {
    curCount++;
    _aEl
      ..style.cursor = "pointer"
      ..id = "cBoxAEl_"+curCount.toString()
      ..className = "scBoxTitle"
      ..text = text;

    _cEl.id = "cBoxCEl_"+curCount.toString();

    if (isChecked != null) checked = isChecked;
    _evs.add(_aEl.onClick.listen(checkUnCheckEl));
    _evs.add(_cEl.onClick.listen(checkUnCheckBox));
  }

  void remove () {
    _evs.forEach((StreamSubscription el) => el.cancel());
    _cEl.remove();
    _aEl.remove();
  }


}

