part of vistemplates;

class DatePicker {
  static int _ctr = 0;

  List<String> wDay = ['Пон','Вто','Сря','Чет','Пет','Съб','Нед'];
  List<String> months = ['Януари','Февруари','Март','Април','Май','Юни',
  'Юли','Август','Септември','Октомври','Ноември','Декември'];
  List<int> dpMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
  Map<int,String> suffix = { 1: 'ви', 2: 'ри', 3: 'ти', 21: 'ви', 22: 'ри', 23: 'ти', 31: 'ви' };
  int curYear = 0;
  int curMon = 0;
  int curDay = 0;
  num _curX, _curY, _curXP, _curYP;
  int _id;

  DivElement _curMonDiv;
  InputElement _el;
  TrInput _trEl;
//  DivElement _calContainer;
  StreamSubscription _ev;
  StreamSubscription _stev;
  Function onPick; //returns selected value in on pick if not null

  void prevMon() {
    curMon--;
    if (curMon < 1) {
      curMon = 12; curYear--;
    }
    redrawCalendar(false);
  }

  void nextMon() {
    curMon++;
    if (curMon > 12) {
      curMon = 1; curYear++;
    }
    redrawCalendar(false);
  }

  void pickDate(MouseEvent ev) {
    ev.preventDefault(); ev.stopImmediatePropagation();
    if (_el != null) {
      int dayPicked = null;
      try {
        dayPicked = int.parse((ev.target as Element).innerHtml);
      } catch(err) { dayPicked = null; }
      if (dayPicked != null) {
        String dayPickedStr = dayPicked.toString();
        if (dayPickedStr.length == 1) dayPickedStr = "0"+dayPickedStr;
        String curMonStr = curMon.toString();
        if (curMonStr.length == 1) curMonStr = "0"+curMonStr;

        //if we have track element set data to it's value to track input changes
        if (_trEl != null) _trEl.modify();
        _el.value = "$dayPickedStr.$curMonStr.$curYear";

        closeCalendar();
        if (onPick != null) onPick(_el.value);
        return;
      }
    }

    if ((ev.target as Element).id == "calPrevMon_${_id}") {
      closeCalendar(); prevMon(); return;
    }
    if ((ev.target as Element).id == "calNextMon_${_id}") {
      closeCalendar(); nextMon(); return;
    }

    if ((ev.target != _el) && (ev.target != _curMonDiv)) closeCalendar();
  }

  void redrawCalendar([bool init = true]) {
    String val = _el.value;
    if ((init) && (val != "")) {
      List<String> oVal = val.split(" ");
      val = oVal[0];
      oVal = val.split(".");
      if (oVal.length == 3) {
        try { curDay = int.parse(oVal[0]); } catch (err) {}
        try { curMon = int.parse(oVal[1]); } catch (err) {}
        try { curYear = int.parse(oVal[2]); } catch (err) {}
      }
    }

    if ((init) && (_el.value != "")) {
      try {
        DateTime dtEl = DateTime.parse(_el.value);
        curYear = dtEl.year;
        curMon = dtEl.month;
        curDay = dtEl.day;
      } catch(err) {}

    }

    if (_curMonDiv == null) _curMonDiv = new DivElement();
    else _curMonDiv.innerHtml = "";

    _curMonDiv
      ..id = 'calendar_$_id'
      ..className = 'calendar';

    String monDiv = '<div class=\"months\">';
    monDiv += '<span id="calPrevMon_${_id}" class="prevMonth" style="float:left">&lt;</span>';
    monDiv += '<span class="currentMon">'+months[curMon-1]+'  $curYear</span>';
    monDiv += '<span id="calNextMon_${_id}" class="nextMonth" style="float:right">&gt;</span>';

    _curMonDiv.appendHtml(monDiv);

    //Build weekdays
    String tBody = "<table><thead>";
    tBody += "<tr class=\"weekDays\">";
    wDay.forEach((day) => tBody += "<th>"+day+"</th>");
    tBody += "</tr></thead>";

    tBody += "<tbody>";
    //Get day of week for first date into the month
    String curMonStr = curMon.toString();
    if (curMonStr.length == 1) curMonStr = "0"+curMonStr;
    int firstOfMonth = DateTime.parse("$curYear-$curMonStr-01").weekday - 1;
    int dayCount = 0; int i = 0;
    for (i = 0;i < firstOfMonth;i++) {
      tBody += "<td>&nbsp;</td>";
      dayCount++;
    }
    for (i = 0;i < dpMonth[curMon-1];i++) {
      if (dayCount == 7) {
        tBody += "</tr><tr>";
        dayCount = 0;
      }
      tBody += "<td class=\"";
      if (isToday(i+1,curMon,curYear)) tBody += "today";
      else tBody += "day";
      tBody += "\"><span>"+(i+1).toString()+"</span></td>";
      dayCount++;
    }
    tBody += "</tr>";

    tBody += "</tbody></table>";
    _curMonDiv.appendHtml(tBody);

    _curMonDiv.style
      ..display = "block"
      ..zIndex = "150"
      ..position = "absolute";

    //Percent values
    if (_curXP == null || _curYP == null) {
      _curXP = (_curX * 100) ~/ window.innerWidth;
      _curXP = _curXP <= 65 ? _curXP : 65; //Max 65%
      _curYP = (_curY * 100) ~/ window.innerHeight;
      _curYP = _curYP <= 65 ? _curYP : 65; //Max 65%
    }

    _curMonDiv.style
        ..left = (_curXP+1).toString()+'%'
        ..top = (_curYP+1).toString()+'%';

//    if (_curXP > 70) _curMonDiv.style.right = (99-_curXP).toString()+"%";
//    else _curMonDiv.style.left = (_curXP+1).toString()+"%";
//
//    if (_curYP > 70) _curMonDiv.style.bottom = (99-_curYP).toString()+"%";
//    else _curMonDiv.style.top = (_curYP+1).toString()+"%";

    document.body.append(_curMonDiv);
    _ev = document.body.onClick.listen(pickDate);
  }

  void closeCalendar() {
    _ev.cancel(); //print ("close Calendar");
    if (_curMonDiv != null) {
      _curMonDiv.innerHtml = "";
      _curMonDiv.style.display = "none";
      _curMonDiv.remove();
      _curMonDiv = null;
    }
  }

  bool isToday(int day, int mon, int year) {
    DateTime dtNow = new DateTime.now();
    if ((dtNow.day == day) && (dtNow.month == mon) && (dtNow.year == year)) return true;
    else  return false;
  }

  DatePicker({String attachId,dynamic inpEl}) {
    DateTime curDate = new DateTime.now();
    curYear = curDate.year;
    curMon = curDate.month;
    curDay = curDate.day;
    _el = null;
    //Next counter
    _id = ++_ctr;

    if (attachId != null) _el = (querySelector("#"+attachId) as InputElement);
    if (inpEl != null){
      if (inpEl is InputElement) _el = inpEl;
      if (inpEl is TrInput) {
        _el = inpEl.inpEl; //Variable _el must be InputElement or TrInput
        _trEl = inpEl;
      }
    }
    if (_el == null) return;

    //showMessage(cEl.runtimeType.toString());
    _stev = _el.onClick.listen((ev) {
      _curX = ev.client.x; _curY = ev.client.y;
      //showMessage("here");
      redrawCalendar();
    });

  }

  void remove() {
    _stev.cancel();
  }
}
