part of vistemplates;

class StartButton {
  DivElement startContainer = null;
  ContextMenu ctMenu = null;

  StartButton() {
    startContainer = new DivElement();
    startContainer
      ..id = "startButton"
      ..classes.add("startButtonTxt")
      ..onClick.listen(openCloseMenu)
      ..append(new SpanElement()
        ..style.cursor = "pointer"
        ..text = "Start");

    ctMenu = new ContextMenu(bottom:5,left:0,id:"startMenu");
  }




  void openCloseMenu(MouseEvent ev) {
    //Open Start menu
    //showMessage("openClose");
    ev.preventDefault(); ev.stopImmediatePropagation();
    if (!ctMenu.shown) ctMenu.showMenu(startContainer);
    else ctMenu.hideMenu();
  }

}
