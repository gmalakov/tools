part of vistemplates;

class YNBox {
  DWindow _win = null;
  String _title = "Въпрос";
  DivElement _YDiv = new DivElement();
  DivElement _NDiv = new DivElement();
  int _answer = 0;
  Function _attfunc = null;

  void answerMsg(MouseEvent ev) {
    ev.preventDefault(); ev.stopImmediatePropagation();
    DivElement cDiv = ev.currentTarget as DivElement;
    if (cDiv == _YDiv) _answer = 1;
    if (cDiv == _NDiv) _answer = 2;
    if (_attfunc != null) _attfunc(_answer);
    _win.remove();
    remove();
  }

  YNBox(String msg, Function attfunc) {
    _attfunc = attfunc;

    int wid = 20; int left = 40;
    if (msg.length > 20) {
      wid = msg.length;
      left = 50-wid~/2;
    }

    _win = new DWindow(left,45,wid,11,_title);
    _win.addHTML("<center/><p style=\"font-size:17px;font-weight:bold \">$msg</p>");

    _win
      ..zIndex = 150
      ..resizable = false
      ..closeable = false
      ..maximizeable = false;

    _win.addObj(_YDiv);
    _YDiv
      ..className = "YNDiv"
      ..innerHtml = "<a style=\"cursor:pointer\">Да</a>"
      ..style.left = "10%"
      ..onClick.listen(answerMsg);
    _win.addObj(_YDiv);

    _NDiv
      ..className = "YNDiv"
      ..innerHtml = "<a style=\"cursor:pointer\">Не</a>"
      ..style.right = "10%"
      ..onClick.listen(answerMsg);
    _win.addObj(_NDiv);

    _win
      ..zIndex = 150
      ..resizable = false
      ..closeable = false;
  }


  void set title(String tit) {
    _title = tit;
    _win.setTitle = tit;
  }

  void remove () =>_win.remove();

}

void YNMsg (String msg,Function attfunc) {
  new YNBox(msg,attfunc);
}


