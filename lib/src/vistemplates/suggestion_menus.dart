part of vistemplates;

typedef FutureOr<dynamic> SuggFunc(String cmd, Map<String, dynamic> data);

class SimpSuggMenu extends ContextMenu {
  Map _sugg = new Map<String, dynamic>();
  Function _callBack = null;
  Map srvData = null;
  String _getCmd = "";
  //Future<String> func(getCmd, map Data);
  static SuggFunc cServer = null;

  Future<dynamic> callServer(String cmd, Map data) {
    if (cServer == null) return null;
    return cServer(cmd, data);
  }

  SimpSuggMenu(InputElement parent, String getCmd, SearchCallBack callBack, {Map sData, String id, bool focusFirst : true})
      : super(id:id, focusFirst: focusFirst) {
    //If named parameter sData is passed -> pass Data to srvData map to be used when sending to server
    // or create a new empty map
    if (sData != null) srvData = sData;
    else srvData = new Map<String, dynamic>();

    if (parent == null) return;
    _parent = parent;
    if (_parent != null) _othEv.add(_parent.onKeyDown.listen(triggerKeys));
    _callBack = callBack;

    _getCmd = getCmd;
  }

  void fireSuggestion(String selId) {
    if (selId == null) _callBack(<String, String>{});
    if (_sugg != null) {
      (_parent as InputElement).value = selId;
      _callBack(_sugg[selId]);
    }
  }

}

//----------------------------------------------------
class ComboMenu extends SimpSuggMenu {
  AnchorElement _aEl = null;
  String _srch = "";

  ComboMenu(InputElement parent, String getCmd, String srch, String label, SearchCallBack callBack, { String id, Map sData, bool focusFirst : true})
      : super(parent, getCmd, callBack, sData:sData, id:id, focusFirst:focusFirst) {
    if (_parent == null) return;
    if ((srch == null) || (srch == "")) return;
    _srch = srch;

    _aEl = new AnchorElement();
    _aEl.className = "smSave";
    _aEl.style
      ..display = "inline"
      ..width = "14px"
      ..height = "18px";
    _aEl.innerHtml = label;
    _parent.parent.classes.add("contextCaller");
    _parent.parent.insertBefore(_aEl, _parent);

    _othEv.add(_aEl.onClick.listen(triggerCombo));
  }

  void set search(String srch) {
    if (_srch != srch) {
      //Clear previous items
      items.clear();
      _sugg.clear();
      _srch = srch;
    }

  }

  void remove() {
    super.remove();
    _aEl.remove();
  }

  void triggerCombo(MouseEvent ev) {
    if (shown) {
      fireSuggestion(null);
      hideMenu();
      return;
    }

    srvData["search"] = _srch;

    if (_sugg.length == 0) {
      callServer(_getCmd, srvData).then((retData) {
        items.clear();
        _sugg.clear();
        if ((retData != "") && (retData != "EMPTY")) {
          List<Map> rRes = [];
          if (retData is String) {
            List<String> retRes = retData.split("##");
            retRes.forEach((res) => rRes.add(json.decode(res.trim()) as Map));
          } else
            if (retData is List) rRes = retData.cast<Map>();

            rRes.forEach((curRes) {
              String curKey = curRes["key"] as String;
              items[curKey] = fireSuggestion;
              _sugg[curKey] = curRes;
            });

          showContext();
        }
      });
    }
    else {
      ev.stopImmediatePropagation();
      ev.preventDefault();
      showContext();
    }
  }

}

//----------------------------------------------------
class SuggestionMenu extends SimpSuggMenu {
  int minLen = 5;
  int _lastLen = 0;
  bool _loading = false;

  StreamSubscription<Event> _listener;

  SuggestionMenu(InputElement parent,String getCmd,SearchCallBack callBack, {String id, Map sData, int mLen, bool focusFirst : true})
      : super (parent, getCmd, callBack, sData:sData, id : id, focusFirst : focusFirst) {
    //Get minimum length from mLen if is positive and not null
    if ((mLen != null) && (mLen > 0)) minLen = mLen;

    _listener = parent.onInput.listen(trackInput);
  }


  void remove() {
    super.remove();
    _listener.cancel();
  }

  void trackInput(Event ev) {
    String srch = "";
    if (_parent != null) srch = (_parent as InputElement).value;

    if (srch.length < minLen) {
      items.clear();
      _sugg.clear();
      if (shown) hideMenu();
      return;
    }

    if (srch.length > minLen) {
      srch = srch.toLowerCase();
      items.clear();
      (_sugg as Map<String, dynamic>).forEach((String id,dynamic data) {
        if ((id != null) && (id.toLowerCase().indexOf(srch) > -1)) items[id] = fireSuggestion;
      });
      if (items.length > 0) showContext();
      else hideMenu();
    }

    //Difference from last length and current length is more than 2 symbols and data length is zero
    // and  current length is more than minLen

    bool toTrigger = false;
    if ((srch.length > minLen) && (_sugg.length == 0) && ((_lastLen - srch.length).abs() > 2)) toTrigger = true;
    //set _lastLen
    _lastLen = srch.length;

    srvData["search"] = srch;
    if (((srch.length == minLen) || (toTrigger)) && (!_loading)) {
      //Start loading data
      _loading = true;
      callServer(_getCmd, srvData).then((retData) {
        items.clear();
        _sugg.clear();
        if ((retData != "") && (retData != "EMPTY")) {
          List<Map> rRes = [];
          if (retData is String) {
            List<String> retRes = retData.split("##");
            retRes.forEach((res) => rRes.add(json.decode(res.trim()) as Map));
          } else
            if (retData is List) rRes = retData.cast<Map>();

          rRes.forEach((curRes) {
            String curKey = curRes["key"] as String;
            items[curKey] = fireSuggestion;
            _sugg[curKey] = curRes;
          });

          showContext();
        }
        //Loading is done
        _loading = false;
      });
    }

  }

}
