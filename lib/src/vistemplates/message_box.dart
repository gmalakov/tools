part of vistemplates;

class MessageBox {
  DWindow _win = null;
  String _title = "Съобщение";
  DivElement _okDiv = new DivElement();
  ButtonInputElement _inpEl;
  List<StreamSubscription> _ev = new List();

  void waitEnter(KeyboardEvent ev) {
    KeyEvent keyEvent = new KeyEvent.wrap(ev);
    if (keyEvent.keyCode == KeyCode.ENTER) {
      ev.preventDefault();
      closeMsg();
    }
  }

  void closeMsg([Event ev]) {
    _ev.forEach((StreamSubscription el) => el.cancel());
    if (_inpEl != null) _inpEl.remove();
    _okDiv.remove();
    _win.remove();
  }

  MessageBox (String msg) {
    int wid = 20; int left = 40;
    if (msg.length > 20) {
      wid = msg.length;
      left = 50-wid~/2;
    }
    _win = new DWindow(left,42,wid,16,_title);
    ParagraphElement pEl = new ParagraphElement()
      ..style.textAlign = "center"
      ..style.fontSize = "17px"
      ..style.fontWeight = "bold"
      ..text = msg;

    _win.addObj(pEl);
    _win.addObj(_okDiv);
    _okDiv
      ..className = "okDiv";

    _inpEl = new ButtonInputElement();
    _inpEl
      ..value = "Ok"
      ..style.border= "0px"
      ..style.color = "#000"
      ..style.backgroundColor = "#2ac5ff"
      ..style.cursor = "pointer";

    _okDiv.append(_inpEl);

    _ev.add(_okDiv.onClick.listen(closeMsg));
    _win.onAppear = (() {
      _inpEl.focus();
    });

    _win
      ..zIndex = 150
      ..resizable = false
      ..closeable = false
      ..maximizeable = false;

    _ev.add(_inpEl.onKeyPress.listen(waitEnter));

  }

  void set title(String tit) {
    _title = tit;
    _win.setTitle = tit;
  }
}

void showMessage(String msg) {
  new MessageBox(msg);
}
