part of vistemplates;

class SaveRestrict {
  Timer _sTimer = null;
  bool get ready => (_sTimer == null);
  Duration _dur = null;
  Function onReady = null;
  static const Duration defDur = const Duration(seconds:5);

  SaveRestrict(Duration dur) {
    _dur = dur;
    if (_dur == null) _dur = defDur;
  }

  void wait() {
    if (_sTimer != null) return;
    _sTimer = new Timer(_dur, () {
      _sTimer.cancel();
      _sTimer = null;
      if (onReady != null) onReady();
    });
  }

  void clear() {
    if (_sTimer != null) {
      _sTimer.cancel();
      _sTimer = null;
      if (onReady != null) onReady();
    }
  }
}

class VisSaveRestrict extends SaveRestrict {
  Function visOnReady = null;
  DivElement _dEl = null;
  DivElement _shadow = null;

  void removeBanner() {
    if (_dEl != null) _dEl.remove();
    _dEl = null;

    if (_shadow != null) _shadow.remove();
    _shadow = null;
  }

  void setBanner() {
    if (_dEl != null) return;

    _dEl = new DivElement();
    _dEl.className = "srvComm";
    _dEl
      ..style.left="75%"
      ..style.backgroundColor = "#aaa";

    _shadow = new DivElement();
    _shadow.className = "resShadow";

    _dEl.appendHtml("Изчакване за запис!");
    document.body.append(_shadow);
    document.body.append(_dEl);
  }

  VisSaveRestrict(Duration dur) : super(dur) {
    //Set onReady function
    onReady = (() {
      removeBanner();
      if (visOnReady != null) visOnReady();
    });
  }

  void wait()  {
    super.wait();
    setBanner();
  }

  void clear() {
    if (_sTimer != null) {
      super.clear();
      removeBanner();
    }
  }

}
