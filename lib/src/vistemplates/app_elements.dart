part of vistemplates;

enum WinStatus {shaded, minimized, maximized, regular}

abstract class AppElement {
  //Button Actions
  Map<int, Function> _butActions = new Map();
  Map<int, String> _butDescr = new Map();

  void addButton(int butId, Function callBack, {String descr}) {
    _butActions[butId] = callBack;
    if (descr != null) _butDescr[butId] = descr;
  }

  void removeButton(int butId) {
    try { _butActions.remove(butId); } catch (err) {}
    try { _butDescr.remove(butId); } catch (err) {}
  }

  void clearButtons() {
    _butActions.clear();
    _butDescr.clear();
  }

  Function getButton(int butId) => _butActions[butId];

  String get butDescr {
    String res = "";
    _butDescr.forEach((id,descr) => res += descr+"\n");
    return res;
  }
}

abstract class TaskApp extends AppElement {
  Map<String,DWindow> windows = new Map();
  Map<String,DivElement> _bars = new Map();
  Map<String,DivElement> _ibars = new Map();
  Map<String,SpanElement> _closes = new Map();
  Map<String,SpanElement> _titles = new Map();

  static const List<dynamic> importantEls = const [InputElement, TextAreaElement, SelectElement, ButtonElement]; // ignore: strong_mode_implicit_dynamic_list_literal
  Element focusedEl;

  void focusWin(String id);
  void minimizeWin(String id);
  void maximizeWin(String id);
  void unFocusWins(String id);
  DWindow addWin(num x, num y,num w,num h,String wtitle, [WinStatus winStatus, String wName]);
  void removeWin(String id);
  void redrawBar();
  void wTitle(String wname, String wTitle);
}
