part of vistemplates;

class TabBar {
  static int curBar = 0;
  static const sysBar = "СИСТЕМА";
  static const key = "msgR";


  int _curBar;
  int _curOpt = 0;
  int qMaxLen = 4;
  UListElement _ul;
  UListElement get body => _ul;
  Map<String,LIElement> _lis = new Map();

  List<StreamSubscription> _uev = new List();
  List<StreamSubscription> _ev = new List();
  StreamSubscription _rev;
  Function loadData;
  void set clickLink(Function click) {
    //Add handlers on system bar if necessarry
    if (_rev == null) _rev = body.onClick.listen(click); // ignore: argument_type_not_assignable
  }

  DivElement _userDiv = new DivElement()
    ..className = "usersdiv";
  DivElement _botDiv = new DivElement()
    ..className = "bot-div";

  TrInput _newMessage = new TrInput()
    ..inpEl.size = 70;
  void set onEnter(Function onEnter) {
    _newMessage.onEnter = onEnter;
  }
  String get newMessage => _newMessage.value;
  void set newMessage(String msg) {
    _newMessage.value = msg;
  }

  AnchorElement _anEl = new AnchorElement()
    ..className = "smBut"
    ..text = "Изпрати";

  DivElement _dummyContent = new DivElement()
    ..className = "tab-content"
    ..style.display = "block"
    ..style.zIndex = "1";

  Map<String, RadioButtonInputElement> _uRadios = new Map();
  Map<String, LabelElement> _uLabels = new Map();
  Queue<String> _tQueue = new Queue();

  Map<String,DivElement> _containers = new Map();

  String get tChecked {
    String checked = "";
    _radios.forEach((n,el) {
      if (el.checked) checked = n;
    });
    return checked;
  }

  String get uChecked {
    String checked = "";
    _uRadios.forEach((n,el) {
      if (el.checked) checked = n;
    });
    return checked;
  }

  void addUsers(Map<int,String> uNames, List<int> online) {
    int cOpt = 0;

    bool isOnline(int id) {
      if (id == 0) return true;
      int i = 0;
      while ((i < online.length) && (online[i] != id)) i++;
      if (i == online.length) return false;
      else return true;
    }

    void addToList(String n, bool isOnLine) {
      cOpt++;
      _uRadios[n] = new RadioButtonInputElement()
        ..id = "utab${curBar}_${cOpt}"
        ..name = "utab${curBar}";
      _userDiv.append(_uRadios[n]);
      _uev.add(_uRadios[n].onClick.listen((el) {
        String name = uChecked;
        checked = name;
      }));

      _uLabels[n] = new LabelElement()
        ..htmlFor = "utab${curBar}_${cOpt}";

      if (isOnLine) _uLabels[n].innerHtml = "<strong>$n</strong>";
      else _uLabels[n].text = n;
      _userDiv.append(_uLabels[n]);
    }

    try {
      _uev.forEach((StreamSubscription el) => el.cancel());
      _uRadios.forEach((n, el) => el.remove());
      _uRadios.clear();
      _uLabels.forEach((n, el) => el.remove());
      _uLabels.clear();
      _userDiv.innerHtml = "";
    } catch (err) {
      print(err.toString());
    }

    List<String> onlineusr = new List();
    List<String> offlineusr = new List();
    uNames.forEach((id,n) {
      if (isOnline(id)) onlineusr.add(n);
      else offlineusr.add(n);
    });

    onlineusr.forEach((el) => addToList(el, true));
    offlineusr.forEach((el) => addToList(el, false));
  }

  bool hasContainer(String option) => (_containers[option] != null);

  void scrollDown() {
    //Scroll to bottom
    _containers.forEach((id,cont) => cont.scrollTop = cont.scrollHeight);
  }

  DivElement getContainer(String option) {
    DivElement container = _containers[option];

    //Add this option if not exists
    if (container == null) {
      addOption = option;
      container = _containers[option];
    }

    return container;
  }

  Map<String, RadioButtonInputElement> _radios = new Map();
  Map<String, LabelElement> _labels = new Map();

  void removeOption(String option) {
    //Remove all containers for this option if exists
    if (_lis[option] != null) try {
      _containers[option].remove();
      _containers.remove(option);
      _labels[option].remove();
      _labels.remove(option);
      _radios[option].remove();
      _radios.remove(option);
      _lis[option].remove();
      _lis.remove(option);
    } catch(err) {
      print(err.toString());
    }
  }

  void set checked (String option) {
    if (_radios[option] == null) addOption = option;
    _radios[option].checked = true;
    if (_uRadios[option] != null) _uRadios[option].checked = true;
  }

  void set addOption(String option) {
    //Remove first bar if too much bars
    _tQueue.addFirst(option);
    if (_tQueue.length > qMaxLen) {
      removeOption(_tQueue.last);
      _tQueue.removeLast();
    }

    _curOpt++;
    _lis[option] = new LIElement();

    _radios[option] = new RadioButtonInputElement()
      ..name = "tab_${_curBar}_radio"
      ..id = "tab${_curBar}_${_curOpt}" ;
    _lis[option].append(_radios[option]);

    _labels[option] = new LabelElement()
      ..text = option
      ..htmlFor = "tab${_curBar}_${_curOpt}";
    _lis[option].append(_labels[option]);

    _containers[option] = new DivElement()
      ..id ="tab-content${_curBar}_${_curOpt}"
      ..className = "tab-content";
    _lis[option].append(_containers[option]);

    _ul.append(_lis[option]);

    //Li -contains- Input, label, div
    //FillIn container
    fillBar(option);
  }

  void fillBar(String option) {
    DivElement curEl = getContainer(option);
    if (loadData != null)
      loadData(option).then((String res) {
        curEl.innerHtml = res;
        try {
          curEl.scrollTop = curEl.scrollHeight;
        } catch (err) { print('TabBar:'+err.toString()); }
      });
  }

  TabBar() {
    _ul = new UListElement()
      ..className = "tabs";
    _ul.append(_userDiv);

    _botDiv.append(_newMessage.inpEl);
    _botDiv.append(_anEl);
    _ev.add(_anEl.onClick.listen((ev) {
      if (_newMessage.onEnter != null) _newMessage.onEnter();
    }));

    _ul.append(_botDiv);
    _ul.append(_dummyContent);

    curBar++; _curBar = curBar;
  }

  void remove() {
    _ul.remove();
    _containers.forEach((n,el) => el.remove());
    _radios.forEach((n,el) => el.remove());
    _labels.forEach((n,el) => el.remove());
    _lis.forEach((n,el) => el.remove());
    _ev.forEach((StreamSubscription el) => el.cancel());
    if (_rev != null) _rev.cancel();

    _uev.forEach((StreamSubscription el) => el.cancel());
    _uRadios.forEach((n,el) => el.remove());
    _uLabels.forEach((n,el) => el.remove());
    _userDiv.remove();
    _newMessage.remove();
    _anEl.remove();
    _botDiv.remove();
    _dummyContent.remove();

  }

}
