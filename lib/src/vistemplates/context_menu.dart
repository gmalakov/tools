part of vistemplates;

class ContextMenu {
  static const String selectedBackground = "#FFA347";

  DivElement _startMenu = null;
  List<StreamSubscription> _ev = new List();
  List<StreamSubscription> _othEv = new List();
  Map<String, Function> items = new Map();
  Element _parent = null;
  String _parentStyle = "";
  String _idT = "";
  static int _num = 0;
  Function onEnter = null;
  Function onPick = null;
  bool firstShow = true;
  bool _focusFirst = true;

  int _top = null;
  int _left = null;
  int _bottom = null;
  int _right = null;
  String _id = null;
  String selName = "";
  int _selId = 0;

  int get maxLen {
    int len = 0;
    items.forEach((String name,Function doThing) {
      if ((name != null) && (name.length > len)) len = name.length;
    });
    return len;
  }

  int get mWidth => ((maxLen)+3).round();

  int get mHeight {
    int height = (items.length*3+6);
    if (height > 40) height = 40;
    return height;
  }

  ContextMenu ({int top,int left,int bottom,int right,String id, bool focusFirst : true}) {
    _num++;
    _top = top; _left = left; _bottom = bottom; _right = right; _id = id;
    _focusFirst = focusFirst;
    if (!_focusFirst) _selId = -1;
    String gid = "ctMenu_"+_num.toString();
    if (_id == null) _id = gid;
  }

  void remove() {
    hideMenu();
    _othEv.forEach((StreamSubscription ev) => ev.cancel());
  }

  UListElement makeList () {
    _idT = "ctMenuOption_"+_id+"_";
    UListElement uL = new UListElement()
      ..id = "ctMenuOption_"+_id+"_"
      ..classes.add("contextMenu");

    _startMenu
      ..style.width = mWidth.toString()+"%"
      ..style.height = mHeight.toString()+"%";

    int i = 0;

    items.forEach((String label, Function doThing) {
      uL.append(new LIElement()..append(new AnchorElement()
        ..id = "${_idT}${i}"
        ..style.cursor = "pointer"
        ..text = label));
      i++;
    });

    return uL;
  }

  void unLightSel() {
    if(_selId > -1) {
      String cId = _idT + _selId.toString();
      try {
        AnchorElement el = querySelector("#" + cId) as AnchorElement;
        if (el != null) el.style.backgroundColor = "";
      } catch (err) {}
    }
  }

  void hiLightSel() {
    if (_selId > -1) {
      String cId = _idT + _selId.toString();
      try {
        AnchorElement el = querySelector("#" + cId) as AnchorElement;
        if (el != null) el.style.backgroundColor = selectedBackground;
        el.focus();
        if (_parent != null) _parent.focus();
      } catch (err) {}
    }
  }


  void triggerKeys(KeyboardEvent ev) {
    KeyEvent keyEvent = new KeyEvent.wrap(ev);
    if (keyEvent.keyCode == KeyCode.DOWN) {
      if (shown) {
        ev.preventDefault();
        unLightSel();
        _selId++;
        if (_selId >= items.length || _selId == -1) _selId = 0;
        hiLightSel();
      }
    }
    if (keyEvent.keyCode == KeyCode.UP) {
      if (shown) {
        ev.preventDefault();
        unLightSel();
        _selId--;
        if (_selId < 0) _selId = items.length-1;
        hiLightSel();
      }
    }

    if (keyEvent.keyCode == KeyCode.ENTER) {
      if (shown && _selId > -1) {
        ev.preventDefault(); ev.stopImmediatePropagation();
        triggerKey(_idT+_selId.toString());
      } else
      if (onEnter != null) {
        //ev.preventDefault();
        //ev.stopImmediatePropagation();
        onEnter();
      }

    }

    if (keyEvent.keyCode == KeyCode.ESC)  hideMenu();
  }


  void triggerKey(String id) {
    //id = id.substring(idT.length, id.length);
    //get id from withing a

    id = querySelector("#"+id)?.text;
    selName = id;
    items[id](id);
    if (onPick != null) onPick();
    //if (onEnter != null) onEnter();
    hideMenu();
  }

  void triggerMenu(MouseEvent ev) {
    ev.preventDefault(); ev.stopImmediatePropagation();
    Element el = (ev.target as Element);
    while ((el.parent != null) && (el.id == "")) el = el.parent;
    //print(el.id);

    if ((el.id.length > _idT.length) && (el.id.substring(0, _idT.length) == _idT))
      triggerKey(el.id);
    else
      hideMenu();
  }

  void showContext() {
    final docElem = document.documentElement;
    final box = _parent.getBoundingClientRect();
    int t = (box.top  + window.pageYOffset - docElem.clientTop+20).round();
    int l = (box.left + window.pageXOffset - docElem.clientLeft).round();

    int lp = (l*100)~/window.innerWidth;
    int tp = (t*100)~/window.innerHeight;

    showMenu(_parent,top:tp,left:lp);
    if (_focusFirst) hiLightSel();
  }


  void showMenu(Element parent,{int top,int left,int bottom,int right}) {
    if (top != null) _top = top;
    if (left != null) _left = left;
    if (bottom != null) _bottom = bottom;
    if (right != null) _right = right;
    _parent = parent;

    if (_focusFirst )_selId = 0;
     else _selId = -1;

    //Create div if not created!
    if (_startMenu == null) {
      _startMenu = new DivElement();
      document.body.append(_startMenu);
    }

    _startMenu.innerHtml = "";

    UListElement items = makeList();
    _startMenu
      ..className = "contextMenuDiv"
      ..id = _id
      ..append(items);

    //Insert non null properties
    if (_left != null) _startMenu.style.left = _left.toString()  +"%";
    if (_top != null) {
      _startMenu.style
        ..transformOriginY = "0%"
        ..top = _top.toString()+"%";
    }
    if (_bottom != null) _startMenu.style.bottom = _bottom.toString()+"%";
    if (_right != null) _startMenu.style.right = _right.toString()+"%";

    if (_parent != null) {
      _parentStyle = _parent.style.border;
      _parent.style.border = "1px inset #fff";
    }

    if (_ev.length == 0) {
      _ev.add(document.body.onClick.listen(triggerMenu));
    }

    //Make the annimation
    if (firstShow) new Timer(const Duration(milliseconds:10), () {
      _startMenu?.classes.add("contextMenuAnim");
      firstShow = false;
    });
    else _startMenu.classes.add("contextMenuAnim");
  }

  void hideMenu () {
    firstShow = true;
    if (_startMenu != null) _startMenu.remove();
    _startMenu = null;
    if (_parent != null) _parent.style.border = _parentStyle;
    _ev.forEach((StreamSubscription el) => el.cancel());
    _ev.clear();
  }


  bool get shown {
    bool shown = false;
    (_startMenu != null) ?  shown = true : shown = false;
    return shown;
  }

}