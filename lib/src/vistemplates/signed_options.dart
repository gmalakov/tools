part of vistemplates;

class SignedOptions {
  static int _curCount = 0;
  Map<String,LabelElement> _aEl = new Map();
  Map<String,RadioButtonInputElement> _rEl = new Map();
  Map<String,String> _col = new Map();
  List<StreamSubscription> _evs = new List();
  Function onDoubleClick = null;
  Function onClick = null;

  bool _disabled = false;

  void set disabled(bool ck) {
    _rEl.forEach((n,v) => v.disabled = ck);
    _disabled = ck;
  }


  SignedOptions () {
    _curCount++;
  }

  //set title color and checkbox
  void check(String id) {
    _rEl.forEach((n,v) {
      if (n == id) v.checked = true;
    });
  }

  bool checked(String id) {
    return _rEl[id].checked;
  }

  String get checkedId {
    String id = "";
    _rEl.forEach((n,v) {
      if (v.checked) id = n;
    });
    return id;
  }


  void remove () {
    _evs.forEach((StreamSubscription el) => el.cancel());
    _rEl.forEach((String n,RadioButtonInputElement v) => v.remove());
    _aEl.remove((String n, LabelElement v) => v.remove());
  }


  void addOption(String id, String label,[String bgColor = "", String fgColor = "000"]) {
    if (bgColor == null) bgColor = "";
    if (fgColor == null) fgColor = "";

    _rEl[id] = new RadioButtonInputElement();
    _rEl[id]
      ..id = "rButREl_id_"+_curCount.toString()+"_"+id
      ..name = "rButRel_"+_curCount.toString()
      ..value = id;

    _aEl[id] = new LabelElement();
    _aEl[id]
      ..style.cursor = "pointer"
      ..id = "cBoxAEl_"+_curCount.toString()
      ..htmlFor = "rButREl_id_"+_curCount.toString()+"_"+id
      ..style.textDecoration = "none"
      ..text = label;

    if (fgColor != "") _aEl[id].style.color = "#"+fgColor;
    if (bgColor != "") _aEl[id].style.backgroundColor="#"+bgColor;

    _evs.add(_aEl[id].onDoubleClick.listen((ev) {
      if (onDoubleClick != null) onDoubleClick();
    }));
    _evs.add(_rEl[id].onChange.listen((ev) {
      if (onClick != null) onClick();
    }));

    _col[id] = bgColor;
    //_evs.add(_aEl[id].onClick.listen(checkLink));
  }

  RadioButtonInputElement gBut(String id) {
    return _rEl[id];
  }

  LabelElement gLabel(String id) {
    return _aEl[id];
  }
}
