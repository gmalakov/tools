part of vistemplates;

class InputExtension {
  static const num offsetX = 2;
  static const num offsetY = 1;
  String wTitle;

  InputElement _parent;
  DWindow _win;
  TextAreaElement _inp;
  List<StreamSubscription> _ev = new List();
  List<StreamSubscription> _evInt = new List();
  num _width, _height;

  List<num> calcXY (MouseEvent ev) {
    num x = ev.client.x; num y= ev.client.y;
    x = (x*100)/window.innerWidth+offsetX; //Calculate percents
    y = (y*100)/window.innerHeight+offsetY; //Calculate percents
    return [x,y];
  }

  void remove() {
    if (_win != null) _win.remove();
    _win = null;
    _ev.forEach((el) => el.cancel());
    _ev.clear();
    _evInt.forEach((el) => el.cancel());
    _evInt.clear();
  }

  void fillBack() {
    //Fill back the parent element
    if (_inp == null) return;
    _win = null; //Set window pointer to null
    String val = _inp.value
        .replaceAll("\n", " ")
        .replaceAll("\r\n", " ");

    _evInt.forEach((el) => el.cancel()); //Remove internal events
    _evInt.clear();
    _inp.remove(); _inp = null; //Remove textarea and zero pointer
    _parent?.value = val;
  }

  void watchEsc(KeyboardEvent ev) {
    if (ev.keyCode == KeyCode.ESC) {
      //Out key is pressed
      ev.preventDefault(); ev.stopImmediatePropagation();
      _win.removeWin(); fillBack();
    }
  }

  void openWin(Event ev) {
    if (_win != null) return;
    List<num> point = calcXY(ev as MouseEvent);
    _win = TaskBar.tBar.addWin(point[0], point[1], _width, _height, wTitle)
      ..onClose = fillBack
      ..escapeClose = true;
    _inp = new TextAreaElement()
      ..id = _win.winId+"_textArea"
      ..value = _parent.value
      ..style.width = "96%"
      ..style.height = "81%";
    _win.addObj(_inp);
    _evInt.add(_inp.onKeyDown.listen(watchEsc));

  }

  InputExtension(this._parent,
      {num width: 25, num height: 15, String name: "Описание"} ) {
    _width = width; _height = height;
    wTitle = name;
    _ev.add(_parent.onDoubleClick.listen(openWin));
  }
}