part of vistemplates;

class CommentBox {
  TextAreaElement _tEl = new TextAreaElement();
  int minH = 30;
  ActivityTrack _act = new ActivityTrack();

  String fCol = "";
  String bCol = "";

  DivElement _inDiv = new DivElement();
  DivElement get inDiv => _inDiv;

  List<StreamSubscription> _ev = new List();
  AnchorElement _title = new AnchorElement();
  DivElement _text = new DivElement();
  bool _hidden = false;

  bool get hidden => _hidden;
  void set hidden(bool hid) {
    _hidden = hid;

    if (_hidden) {
      _tEl.style.display = "none";
      _text.style.display = "inline";
      _text.style.color = fCol;
      _text.style.backgroundColor = bCol;
      _text.style.cursor = "text";
      _text.style.userSelect = "text";
      //_text.innerHtml = _tEl.value.replaceAll("\n", "<br/>")+"<br/>";
      _text.innerHtml = linkTools.replaceLinks(_tEl.value.replaceAll("\n", " <br/>"))+"<br/>";
    } else {
      _tEl.style.display = "block";
      _text.style.display = "none";
      autoResize(null,false);
    }
  }

  void set disabled(bool ck) {
    hidden = ck;
    _tEl.disabled = ck;
  }


  void set text(String txt) {
    _tEl.value = txt;
    _text.innerHtml = linkTools.replaceLinks(txt.replaceAll("\n", " <br/>"))
        +"<br/>";
    autoResize(null);
  }

  String get text {
    return _tEl.value;
  }

  void remove () {
    _ev.forEach((StreamSubscription ev) => ev.cancel());
    _tEl.remove();
    _title.remove();
    _text.remove();
    _inDiv.remove();
  }

  void autoResize(Event ev,[bool mod]) {
    if (mod == null) mod = true;
    _tEl.style.height = "auto";
    int sh = _tEl.scrollHeight;
    if ((sh > minH) && (sh > _tEl.clientHeight)) _tEl.style.height = (sh+10).toString()+"px";
    if (sh < minH) _tEl.style.height = minH.toString()+"px";
    if (mod) _act.modify();
  }

  void linkClick([MouseEvent ev]) {
    AnchorElement anEl ;
    try { anEl = (ev.target as AnchorElement); } catch (err) { return; }

    if ((anEl != null) && (linkTools.linkRegEx.allMatches(anEl.innerHtml).length > 0)) {
      ev.preventDefault(); ev.stopImmediatePropagation();
      openLink(anEl.innerHtml);
      //Have http reference
    }
  }

  void openLink(String link) {
    window.open(link, "My link");
  }

  void showHideContainer([MouseEvent ev]) {
    if (ev != null) { ev.preventDefault(); ev.stopImmediatePropagation(); }
    if (_hidden) hidden = false;
    else hidden = true;
  }

  bool get okMod => _act.okMod;
  bool get saved => _act.saved;
  void zeroMod() => _act.zeroMod();

  CommentBox (String title, String comId, {bool hid, String fCol, String bCol, bool linkHandler : true}) {

    //Init parameters
    if (hid == null) hid = false;
    if (fCol == null) fCol = "";
    if (bCol == null) bCol = "";
    this.fCol = fCol;
    this.bCol = bCol;

    _title
      ..id = "commTitle"+comId
      ..text = title
      ..className = "com-title";

    _text
      ..id = "commText"+comId
      ..className = "com-title"
      ..style.display = "none";

    if (linkHandler) _ev.add(_text.onClick.listen(linkClick));

    _tEl
      ..style.width = "100%"
      ..style.height = "auto";

    _inDiv
      ..className = "contextCaller"
      ..style.width = "98%";

    if (title != "") {
      _inDiv.append(_title);
    }
    _inDiv.append(_text);
    if (title != "") _inDiv.appendHtml("<br/>");
    _inDiv.append(_tEl);

    _ev.add(_tEl.onChange.listen(autoResize));
    _ev.add(_tEl.onInput.listen(autoResize));
    _ev.add(_title.onClick.listen(showHideContainer));
    _act.zeroMod();

    hidden = hid;
  }
}
