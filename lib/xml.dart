library xmlbuilder;

import 'package:xml/xml.dart' as xml;

class XML {

  xml.XmlBuilder builder;
  xml.XmlDocument document;

  XML(String target, String text) {
    builder = new xml.XmlBuilder();
    builder.processing(target, text);
  }

  XML.fromMap(String target, String text, Map<String, dynamic> data) {
    builder = new xml.XmlBuilder();
    builder.processing(target, text);
    data.forEach(_toXml);
  }

  XML.fromString(String data) {
    document = xml.parse(data);
  }

  void _toXml(String k, dynamic v) {
    if (v == null)
      return;

    //var parts = k.split('|');
    Map parts;
    if ((v is List) && (v.length == 2) && (v[0] is Map) &&
        (v[0]['attributes'] == 1)) {
      //These are attributes
      parts = v[0] as Map;
      parts.remove('attributes');
      v = v[1];
    }

    if (v is List) {
      v.forEach((dynamic t) => _toXml(k, t));
    } else {
      builder.element(k, nest: () {
        if (parts != null)
          for (dynamic attr in parts.keys)
            builder.attribute(attr as String, parts[attr]);
        (v is Map<String, dynamic>) ? v.forEach(_toXml) : builder.text(v);
      });
    }
  }

  dynamic _toMap(Map<String, dynamic> attrMap, List childs,
      {bool rEmpty: false, bool simplify: false }) {
    dynamic _build(xml.XmlElement node) {
      //print(node.toString());
      Map<String, dynamic> rMap = {}; //Initialize new map
      if (node.attributes.length > 0) {
        for (var n in node.attributes)
          rMap[n.name.toString()] = n.value;
         rMap['attributes'] = 1;
      }

      if (node.descendants.length > 1)
        return _toMap(rMap, node.children, rEmpty: rEmpty, simplify: simplify);
      else
        if (simplify) {
          if (rMap.length > 0){
            rMap["data"] = node.text;
            return rMap;
          } //If Attribute map is not empty
          else
            return node.text; //if attribute map is empty return single element
        } else {
          if (rMap.length > 0) //If Attribute map is not empty
            //rMap["data"] = node.text; //Add inner data as data and return
            return [rMap, node.text];
          else
            return node.text; //if attribute map is empty return single element
        }
    }

    void _addval(Map m, String name, dynamic val) {
      //print(m.toString()+"|"+name+"|"+val);
      if (m[name] == null)
        m[name] = val;
      else {
        if (m[name] is List) {
          if ((m[name].length == 2) && (m[name][0] is Map) &&
              (m[name][0]['attributes'] == 1)) {
            //This is list but [attributes, values]
            dynamic temp = m[name];
            m[name] = new List<dynamic>()
            ..add(temp)..add(val);
          } else m[name].add(val);
        }
        else {
          dynamic temp = m[name];
          m[name] = new List<dynamic>()
            ..add(temp)..add(val);
        }
      }
    }

    Map m = new Map<String, dynamic>();
    for (var node in childs) {
      String val = (node.text as String).trim();

      if ((node is xml.XmlText) && val.isNotEmpty) _addval(m, 'text', val);
      if ((node is xml.XmlComment) && val.isNotEmpty) _addval(m, 'comment', val);
      if (node is xml.XmlCDATA) _addval(m, 'cdata', val);

      if (node is xml.XmlElement)
        _addval(m, node.name.toString(), _build(node));
    }

    if (rEmpty) {
      List<String> toRemove = new List();
      for (dynamic n in m.keys)
        if ((m[n] == null) || (m[n] == "")) toRemove.add(n as String);
      for (String n in toRemove)
        m.remove(n);
    }

    //if simplify then combine atrribute map with other map
    if (simplify) {
      if (attrMap.length > 0) attrMap.remove('attributes');
      attrMap.forEach((String n, dynamic v) => _addval(m, n, v));
      attrMap.clear();
    }

    //If no attributes return only values
    if (attrMap.length == 0) {
      if ((simplify) && (m.length == 1))
        for (dynamic n in m.keys)
          return m[n as String];
      else
        return m;
    }
    else {
      //If attribute map is not empty. Return list of [Map<attributes>, values]
      if  (m.length == 1)
        for (dynamic n in m.keys)
          return [attrMap, m[n as String]];
      else
        return [attrMap, m];
    }

    return {};
  }

  dynamic toMap([String path, bool rEmpty = false, bool simplify = false]) =>
      path != null ?
      _toMap(new Map<String, dynamic>(), document
          .findAllElements(path)
          .first
          .children, rEmpty: rEmpty, simplify: simplify) :
      _toMap(new Map<String, dynamic>(),
          document.children[1].children, rEmpty: rEmpty,
          simplify: simplify);

  String toString() => document.toString();
}