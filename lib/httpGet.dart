library http_get;

import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:utf/utf.dart' as utf;

Function LogError = print;
Function LogData = print;

enum Encoding {utf8, utf16, cp1251}
enum Compression {zlib, gzip, none }

class DataRes {
  String result;
  List<Cookie> cookies;
  HttpClientResponse resp;

  DataRes(this.result, this.cookies, this.resp);

  Map<String, String> get cookieData {
    final Map<String, String> retData = {};
    cookies.forEach((c) => retData[c.name] = c.value);
    return retData;
  }

  Map<String, dynamic> toMap() => {'result': result, 'cookies': cookieData };
  Map<String, dynamic> toJson() => toMap();
  String toString() => '$runtimeType ${toMap()}';
}

class DataGet {
  static const int maxTries = 8;
  static const int maxConnRetry = 8;
  //Maximum seconds to wait until timeout of connection
  static const int maxTimeOut = 40;
  static const Duration waitRetry = const Duration (seconds : 3);
  static const int maxConnections = 30; //Maximum number of connections

  static int _connections = 0;
  static final List<HttpClient> _clients = [];

  static Future<void> delay(Duration dur) => new Future<void>.delayed(dur);

  static Future<DataRes> _dGetData(String url,
      { Map<String, dynamic> hdr, Encoding enc = Encoding.utf8,

        Map<String,String> cookies, dynamic post,
        String clientCertFile, String clientKeyFile, bool postJson = false,
        Compression comp = Compression.none, Duration wait }) {

    final Completer<DataRes> completer = new Completer();

    if (_connections >= maxConnections) {
      //Schedule retry for later if max connections are reached
      new Timer(waitRetry, () async {
        completer.complete(
            await _dGetData(url, hdr: hdr, enc: enc, cookies: cookies,
                post: post, clientCertFile: clientCertFile,
                clientKeyFile: clientKeyFile, postJson: postJson,
                comp: comp, wait: wait));
      });
    } else {
      _connections++; //Take a connection

      if (wait == null)
        return _getData(url, hdr: hdr,
            enc: enc,
            cookies: cookies,
            post: post,
            clientCertFile: clientCertFile,
            clientKeyFile: clientKeyFile,
            postJson: postJson,
            comp: comp);
      else {
        //Complete with delay
        new Timer(wait, () async {
          completer.complete(await _getData(url, hdr: hdr,
              enc: enc,
              cookies: cookies,
              post: post,
              clientCertFile: clientCertFile,
              clientKeyFile: clientKeyFile,
              postJson: postJson,
              comp: comp));
        });
      }
    }
    //Return completer when ready
    return completer.future;
  }

  static Future<DataRes> _getData(String url, {
    Map<String, dynamic> hdr, Encoding enc = Encoding.utf8,
    Map<String,String> cookies, dynamic post,
    String clientCertFile, String clientKeyFile,
    bool postJson = false, Compression comp = Compression.none }) async {

    final SecurityContext context = new SecurityContext();
    if (clientCertFile != null)
      context
        ..useCertificateChain(clientCertFile)
        ..usePrivateKey(clientKeyFile);

    final HttpClient client = new HttpClient(context:context)
      ..badCertificateCallback = (certificate, host, callbackPort) => true;
    _clients.add(client);

    HttpClientRequest res;
    HttpClientResponse resp;

    bool connected = false;
    bool sent = false;
    int tries = 0;

//Try maxtries times to connect
    while (((!connected) || (!sent)) && (tries < maxTries)) {
      try {
        if (post == null)
          res = await client.getUrl(Uri.parse(url))
              .timeout(const Duration(seconds: maxTimeOut));
        else
          res = await client.postUrl(Uri.parse(url))
              .timeout(const Duration(seconds: maxTimeOut));

        if (res != null) connected = true;
        else {
          tries++;
          LogError('Timeout connecting to address: $tries times.');
        }
      } catch (err) {
        tries++;
        LogError('$url:$err: $tries times.');
        await res?.close();
        client?.close();
        await delay(waitRetry);
      }

      //If connection is made try to send headers and data
      if (connected) {
        //Set custom user agent
        res.headers.removeAll(HttpHeaders.userAgentHeader);
        res.headers.add(HttpHeaders.userAgentHeader, 'Nightmare browser 1.0');

        if (hdr != null)
          hdr.forEach((n, v) => res.headers.add(n, v));

        if (cookies != null)
          cookies.forEach((n, v) => res.cookies.add(new Cookie(n, v)));

        if (post != null) {
          if (post is Map) {
            //Set http headers to urlencoded
            res.headers
              ..removeAll('Content-Type')
              ..add('Content-Type', 'application/x-www-form-urlencoded')..add(
                'Referer', url);

            final StringBuffer send = new StringBuffer();

            if (!postJson) {
              post.forEach((n, v) {
                if (send.isEmpty) send.write('&');
                send..write(Uri.encodeComponent(n))..write('=')..write(
                    Uri.encodeComponent(v));
              });
            } else
              send.write(json.encode(post));
            //print(send);

            res.write(send.toString());
          } else if (post is String) {
            res.headers
              ..removeAll('Content-Type')
              ..add('Content-Type', 'application/text')
              ..add('Referer', url);
            res.write(post);
          }
        }

        try {
          resp = await res.close();
          client.close();
          _clients.remove(client);
          sent = true;
        }
        catch (err) {
          tries++;
          LogError('$url:$err ($tries times)');
          await delay(waitRetry);
        }
      }
    }

    if (resp != null) {
      //Collect stream data
      final List<Cookie> cookies = resp.cookies;

      final List<List<int>> oRes = await resp.toList();
      List<int> allRes = [];
      if (oRes != null)
        oRes.forEach((cRes) {
          if (cRes != null) allRes.addAll(cRes);
        });

      //UnCompress data if needed
      switch (comp) {
        case Compression.zlib: allRes = zlib.decode(allRes);
        break;
        case Compression.gzip: allRes = gzip.decode(allRes);
        break;
        default:
      }

      _connections--; //Connection ready release it
      switch (enc) {
        case Encoding.utf8:
          return new DataRes(utf8.decode(allRes,
              allowMalformed: true), cookies, resp);
        case Encoding.cp1251:
          return new DataRes(new Cp1251Decoder().convert(allRes),
              cookies, resp);
        case Encoding.utf16:
          return new DataRes(utf.decodeUtf16(allRes), cookies, resp);
        default: return new DataRes(utf8.decode(allRes,
            allowMalformed: true), cookies, resp);
      }
    } else return null;
  }

  static Future<DataRes> getData(String url,
      {Map<String, dynamic> hdr, Encoding enc = Encoding.utf8,
        Map<String,String> cookies, dynamic post,
        String clientCertFile, String clientKeyFile, bool postJson = false,
        Compression comp = Compression.none, Duration wait }) async {
    int tries = 0;
    DataRes res;

    while ((res == null) && (tries < maxConnRetry)) {
      if (tries > 0) wait = waitRetry;
      // Set wait time to retry time if already tried
      try {
        res = await _dGetData(url, hdr:hdr, enc:enc,
            cookies: cookies, post:post, clientCertFile:clientCertFile,
            clientKeyFile: clientKeyFile, postJson: postJson,
            comp: comp, wait:wait);
      } catch (err) {
        tries++;
        LogError('$err: $tries times.');
      }
    }

    return res ?? new DataRes('',null, null); //Return empty string if null
  }

  static void cleanConnections() {
    for (final cl in _clients)
      cl.close(force: true);
    _clients.clear();
  }

}

Future<String> getData(String url, { Map<String, dynamic> hdr,
  Encoding enc = Encoding.utf8, Map<String,String> cookies,
  dynamic post, String clientCertFile,
  String clientKeyFile, bool postJson = false,
  Compression comp = Compression.none, Duration wait}) {

  int tries = 0;

  while (tries < DataGet.maxTries)
    try {
      return DataGet.getData(url, hdr: hdr,
          enc: enc,
          cookies: cookies,
          post: post,
          clientCertFile: clientCertFile,
          clientKeyFile: clientKeyFile,
          postJson: postJson,
          comp: comp,
          wait: wait).then((r) => r.result);
    } catch(err) {
      tries++;
      LogError('Error getting url: \n $err \n ($tries times)');
      wait = DataGet.waitRetry; //Set wait retry on second chance
    }

  LogError('Max retry count reached! $url');
  return new Future.value('');
}

Future<DataRes> getDataCookies(String url, {
  Map<String, dynamic> hdr, Encoding enc = Encoding.utf8,
  Map<String,String> cookies, dynamic post,
  String clientCertFile, String clientKeyFile, bool postJson = false,
  Compression comp = Compression.none, Duration wait }) {

  int tries = 0;

  while (tries < DataGet.maxTries)
    try {
      return DataGet.getData(url, hdr: hdr,
          enc: enc,
          cookies: cookies,
          post: post,
          clientCertFile: clientCertFile,
          clientKeyFile: clientKeyFile,
          postJson: postJson,
          comp: comp,
          wait: wait);
    } catch(err) {
      tries++;
      LogError('Error getting url: \n $err \n ($tries times)');
      wait = DataGet.waitRetry; //Set wait retry on second chance
    }

  LogError('Max retry count reached! $url');
  //throw new Exception("HERE");
  return new Future.value(null);
}

class Cp1251Decoder extends Converter<List<int>, String> {

  String convert(List<int> input, [int start = 0, int end]) {
    // Allow the implementation to intercept and specialize based on the type
    // of codeUnits.

    final length = input.length;
    RangeError.checkValidRange(start, end, length);
    end ??= length;

    final List<int> outCodes = [];
    for (int i=0; i < input.length;i++) {
      final int c = input[i];
      if (c < 192) outCodes.add(c);
      else
      if (c >= 192)
        outCodes.add(c+848);
    }

    return new String.fromCharCodes(outCodes);
  }

}

