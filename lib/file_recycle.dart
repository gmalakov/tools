library file_recycle;

import 'dart:async';
import 'dart:io';

class FileRecycle {
  static const int rmTime = 5; //5 minutes

  final List<String> _files = [];
  final List<DateTime> _times = [];
  Timer _tim;

  static FileRecycle instance = new FileRecycle._();
  factory FileRecycle() => instance;

  FileRecycle._() {
    _tim = new Timer.periodic(const Duration(minutes: 1), (_) {
      int i = 0;
      final now = new DateTime.now();
      while (i < _files.length)
        if (now.difference(_times[i]).inMinutes > rmTime) {
          final String name = _files[i];
          try {
            final f =  new File(name);
            if (f.existsSync()) f.delete(recursive: true);
          } catch (err) {}

          _files.removeAt(i);
          _times.removeAt(i);
          //No need to move to next element we are shifting the list
        } else
          i++;
    });
  }

  void addFile(String name) {
    final idx = _files.indexOf(name);
    if (idx == -1) {
      _files.add(name);
      _times.add(new DateTime.now());
    } else
      _times[idx] = new DateTime.now(); //Refresh datetime of usage only
  }

  void remove() => _tim.cancel();
}
