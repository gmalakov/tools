library linktools;

final RegExp linkRegEx = new RegExp(r'((ftp|http|https):\/\/)(\w+)?(\S+)?');

String replaceLinks(String inP) {
  return inP.replaceAllMapped(linkRegEx, (Match match) => "<a>"+match.group(0)+"</a>");
}
