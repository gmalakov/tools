import 'dart:io';
import 'dart:async';

class Logger {
  static const String logPath = "../log";
  static const bool logConsole = false;
  static const String infoLog = logPath+"/stdout.log";
  static const String errorLog = logPath+"/stderr.log";
  static bool console = false;

  static String bgDateFull(DateTime inDate) {
    List<String> iDate = inDate.toString().split(" ");
    List<String> spDate = iDate[0].split("-");
    return spDate[2]+"."+spDate[1]+"."+spDate[0]+" "+iDate[1];
  }

  IOSink fErr;
  IOSink fData;

  List<String> messages = new List();
  List<int> msgType = new List();

  void LogData(String inPut, {int level:0})  {
    if (console) print (inPut);
     else {
      messages.insert(0, inPut); //Put message at first position
      msgType.insert(0, level); //Put msgType ar first position
      if (messages.length == 1) _logData(); //runqueue if this the only message
    }
  }

  Future<bool> _logData() async {
    if (messages.length == 0) return false; //Exit on empty queue

    String inPut = messages.last; //Get last message
    messages.removeLast();
    int level = msgType.last; //Get last type
    msgType.removeLast();

    //level = 0 nginx.conf / level = 1 error
    String dtLog = "["+bgDateFull(new DateTime.now())+"]";

    if (!logConsole) {
      fErr ??= new File(errorLog).openWrite(mode:FileMode.append);
      fData ??= new File(infoLog).openWrite(mode:FileMode.append);
      IOSink f;
      if (level == 0) f = fData;
      if (level == 1) f = fErr;

      try {
        f?.write("$dtLog -> $inPut \n");
        await f?.done;
      }
      catch (err) { print(err.toString() + "\n $f "); }

    } else print("$dtLog -> $inPut");

    if (messages.isEmpty) {
      //End of queue
      await fErr?.close(); fErr = null;
      await fData?.close(); fData = null;
    }

    //Call myself for the next message
    if (messages.isNotEmpty) return await _logData(); //Wait for the next message to send
     else return true;
  }

  static Logger _logger = new Logger();
  static Logger get instance => _logger;
}

void LogData(String inPut) => Logger.instance.LogData(inPut, level:0);
void LogError(String inPut) => Logger.instance.LogData(inPut, level:1);