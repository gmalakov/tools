library Pool;

import 'package:postgres/postgres.dart';
import 'package:sqljocky5/sqljocky.dart';
import 'package:tools/tools.dart';
import 'dart:async';
import 'sql_tools.dart';

abstract class GenPool<P> {
  static const CheckTime = const Duration(minutes: 1);

  int maxConn;
  String _url;
  int _port;
  String _dbName;
  String _user;
  String _password;

  int _waitingConn = 0;
  int get connCount => _freeConnections.length + _busyConnections.length + _waitingConn;


  List<Function> waitCallbacks = [];
  List<P> _busyConnections = [];

  List<P> _freeConnections = [];
  List<DateTime> _freeConnTimes = [];
  Timer _tim;

  P _getFreeConn() {
    _freeConnTimes.removeAt(0);
    return _freeConnections.removeAt(0);
  }

  void _addFreeConn(P) {
    _freeConnections.add(P);
    _freeConnTimes.add(new DateTime.now());
  }

  GenPool(this._url, this._port, this._dbName, this._user, this._password, [ this.maxConn = 3 ]) {
    _tim = new Timer.periodic(CheckTime, (_) {
       int i = 0; int r = 0;
       if (_freeConnections.length > 0)
         do {
           var now = new DateTime.now();
           num diff = now.difference(_freeConnTimes[i]).inMinutes;
           if (diff > 1) {
             var conn = _freeConnections.removeAt(i);
             _freeConnTimes.removeAt(i);
             removeConn(conn);
             r++;
           } else i++;
         } while (i < _freeConnections.length);

      if (r > 0) LogData('Removed old connections ($r).');
    });
  }

  Future<bool> removeConn(P conn);

  void release(P conn) {
    if (_busyConnections.contains(conn)) {

      //If no waiting connections
      if (waitCallbacks.isEmpty) {
        _busyConnections.remove(conn);
        _addFreeConn(conn);
      }
       else {
         //Get first waiting for connection
         Function tmp = waitCallbacks.first;
         waitCallbacks.removeAt(0);
         tmp(conn); //Callback to the free connection
      }
    }
  }

  Future<P> getConn();
  FutureOr<bool> stop() {
    _tim.cancel();
    return true;
  }

  bool isWrite(String query) {
    if (query == null) return false;
    String myQ = query.toLowerCase();
    if (myQ.contains('delete') || myQ.contains('update')
              || myQ.contains('insert') || myQ.contains('into')) return true;
      else return false;
  }

}

class PgPool extends GenPool<PostgreSQLConnection> {
  PgPool backPool;

  PgPool(String url, int port, String dbName, String user, String password, [ int maxConn = 3, this.backPool ]):
      super(url, port, dbName, user, password, maxConn);

  Future<bool> removeConn(PostgreSQLConnection conn) {
    return conn.close().then((_) => true);
  }

  Future<PostgreSQLConnection> getConn() {
    Completer<PostgreSQLConnection> comp = new Completer();
    if (_freeConnections.length > 0) {
      PostgreSQLConnection conn = _getFreeConn();

      //PostgreSQLConnection is not ready ??? Get another connection
      if (conn.isClosed)
      { //(conn.state == ConnectionState.busy)
        LogError("PostgreSQLConnection is closed!");
        getConn().then((PostgreSQLConnection conn) => comp.complete(conn));
      }
      else {
        _busyConnections.add(conn); //Add to busy connections and return
        //print("Successfully got connection!");
        comp.complete(conn);
      }
    } else {
      if (connCount < maxConn) { //Not reached maximum connections
        _waitingConn++; //Add a waiting connection
        PostgreSQLConnection conn =
        new PostgreSQLConnection(_url, _port, _dbName, username: _user, password: _password);
        conn.open().then((_) {
          _waitingConn--; //Remove waiting connection on success
          _busyConnections.add(conn); //Add to busy connections
          comp.complete(conn); //Return connection
        })
            .catchError((dynamic onError) {
          _waitingConn--; //Remove waiting connection on error
          LogError("Error connecting to Pool. Retrying in 500ms. " + onError.toString());
          Timer tim;
          tim = new Timer(const Duration(milliseconds: 500), () {
            tim.cancel();
            getConn().then((PostgreSQLConnection conn) => comp.complete(conn));
          });
        });
      } else {
        //LogData("Not enough free connections. Delaying connection.");
        waitCallbacks.add((PostgreSQLConnection conn) => comp.complete(conn));
      }
    }
    return comp.future;
  }

  Future<bool> stop() async {
    super.stop();
    for (int i = 0; i < _busyConnections.length; i++) await _busyConnections[i].close();
    _busyConnections.clear();
    while (_freeConnections.length > 0) {
      PostgreSQLConnection conn = _getFreeConn();
      await conn.close();
    }

    if (backPool != null) await backPool.stop();

    return true;
  }

  Future<List<List<dynamic>>> query(
      String sql, ErrFunc onError, Map<String, dynamic> subPar) async {
    //Execute backup pool query if is write and we have backupPool
    if (backPool != null && isWrite(sql)) backPool.query(sql, onError, subPar);

    PostgreSQLConnection conn = await getConn(); // Get connection if needed
    List<List<dynamic>> res;
    try {
      res = await conn.query(sql, substitutionValues: subPar, timeoutInSeconds:60);
    } catch (err) {
      String strErr = err.toString();
      if (onError != null) await onError(strErr);
      if (strErr.indexOf("duplicate key") == -1) //Remove duplicate key warnings (dgImport needed)
        LogError("PGRE Query Error: $strErr \n $sql");
    }
    release(conn);

    return res;
  }

}

class MyPool extends GenPool<MySqlConnection> {
  MyPool backPool;

  Future<bool> removeConn(MySqlConnection conn) {
    return conn.close().then((_) => true);
  }

  MyPool(String url, int port, String dbName, String user, String password, [ int maxConn = 3, this.backPool ]):
        super(url, port, dbName, user, password, maxConn);

  Future<MySqlConnection> getConn() {
    Completer<MySqlConnection> comp = new Completer();
    if (_freeConnections.length > 0) {
      //Get free connection
      MySqlConnection conn = _getFreeConn();

        _busyConnections.add(conn); //Add to busy connections and return
        comp.complete(conn);
    } else {
      if (connCount < maxConn) { //Not reached maximum connections
        _waitingConn++;
        //print('connection try! -> $_waitingConn');
        MySqlConnection.connect(new ConnectionSettings(host:_url, db: _dbName, port:_port, user:_user, password: _password))
         .then((conn) {
           _waitingConn--; //Remove waiting connection on success
          _busyConnections.add(conn); //Add to busy connections
          comp.complete(conn); //Return connection
        })
            .catchError((dynamic onError) {
           _waitingConn--; //Remove waiting connection on error
          LogError("Error connecting to Pool. Retrying in 500ms. $_url:$_port -> $_dbName \n" + onError.toString());
          Timer tim;
          tim = new Timer(const Duration(milliseconds: 500), () {
            tim.cancel();
            getConn().then((MySqlConnection conn) => comp.complete(conn));
          });
        });
      } else {
        //LogData("Not enough free connections. Delaying connection.");
        waitCallbacks.add((MySqlConnection conn) => comp.complete(conn));
      }
    }
    return comp.future;
  }

  Future<bool> stop() async {
    super.stop();
    for (int i = 0; i < _busyConnections.length; i++) await _busyConnections[i].close();
    _busyConnections.clear();
    while (_freeConnections.length > 0) {
      MySqlConnection conn = _getFreeConn();
      await conn.close();
    }

    if (backPool != null) await backPool.stop();

    return true;
  }

  Future<StreamedResults> prepareExecute(String sql, ErrFunc onError,
      [List<String> par]) async {
    MySqlConnection conn = await getConn(); //Get a free connection
    StreamedResults res;
    try {
      if (par != null && par.isNotEmpty)
         res = await conn.prepared(sql,par);
        else
         res = await conn.execute(sql); //Exec query

      //Send to backup pool if not null and is a write
      if (backPool != null && isWrite(sql)) backPool.query(sql, onError);
    } catch (err) {
      LogError("Mysql query:"+err.toString());
      if (onError != null) await onError(err.toString());
      LogError(sql);
    }
    release(conn); //Release connection
    return res; //Return results
  }

  Future<StreamedResults> query(String sql, ErrFunc onError,
          [List<String> par]) =>
      prepareExecute(sql,onError, par);

}
