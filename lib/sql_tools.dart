library sqlTools;

import 'dart:async';
import 'dart:io';
import 'dart:convert';

import 'package:sqljocky5/sqljocky.dart' as mysql;
import 'pool.dart';
//import 'package:postgres/postgres.dart' as pgre;
import 'package:tools/tools.dart';
import 'clone.dart';

part 'sql_tools/qcreator.dart';
part 'sql_tools/select_creator.dart';
part 'sql_tools/insert_creator.dart';
part 'sql_tools/update_creator.dart';
part 'sql_tools/q_executor.dart';
part 'sql_tools/gen_mapper.dart';
part 'sql_tools/def_mapper.dart';
part 'sql_tools/table_loader.dart';
part 'sql_tools/table_builder.dart';
part 'sql_tools/alter_builder.dart';

Function LogError = print;
Function LogData = print;

String rmUnSafe(String inStr) {
  if (QExecutor.sqlDriver == SqlType.mysql)
  return inStr
      .replaceAll("\\", "")
      .replaceAll("'", "\\'")
      .replaceAll('"', "\\'");
  else
    return inStr
        .replaceAll("\\", "")
        .replaceAll("'", "''")
        .replaceAll('"', "''")
    //    .replaceAll('&', '')
        .replaceAll('@', '@@');
}
