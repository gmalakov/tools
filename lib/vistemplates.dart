library vistemplates;

import 'dart:html';
import 'tools.dart';
import "dart:convert";
import "dart:async";
import "dart:collection";
import "keyCodes.dart";
import "linktools.dart" as linkTools;

part 'src/vistemplates/saverestrict.dart';
part 'src/vistemplates/validators.dart';
part 'src/vistemplates/app_elements.dart';
part 'src/vistemplates/dwindow.dart';
part 'src/vistemplates/win_animation.dart';
part 'src/vistemplates/context_menu.dart';
part 'src/vistemplates/start_menu.dart';
part 'src/vistemplates/taskbar.dart';
part 'src/vistemplates/message_box.dart';
part 'src/vistemplates/yn_box.dart';
part 'src/vistemplates/date_picker.dart';
part 'src/vistemplates/suggestion_menus.dart';
part 'src/vistemplates/signed_options.dart';
part 'src/vistemplates/track_elements.dart';
part 'src/vistemplates/input_extension.dart';
part 'src/vistemplates/signed_checkbox.dart';
part 'src/vistemplates/comment_box.dart';
part 'src/vistemplates/form_types.dart';
part 'src/vistemplates/gen_table.dart';
part 'src/vistemplates/tab_bar.dart';
part 'src/vistemplates/log_view.dart';
part 'src/vistemplates/tooltip.dart';

typedef SearchCallBack(Map retMap);
enum EscAction { EscClose, EscMinimize, None }

