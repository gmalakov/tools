part of tools;

class Pass {
  static String pwEncode(String password) {
    String seed = "";
    Random rnd = new Random();
    for (int i = 1; i <= 10; i++) {
      int rndI = rnd.nextInt(15);
      seed += "0123456789ABCDEF".substring(rndI,rndI+1);
    }

    List<int> bytes;
    try { bytes = sha1.convert(latin1.encode(seed+password+seed)).bytes; } catch (err) {}
    String outStr =   hex.encode(bytes)+seed;
    return outStr;
  }

  static bool pwCheck(String password, String storedValue) {
    //Exit if no password or stored value defined
    if ((password == null) || (storedValue == null)) return false;

    if (storedValue.length != 50) return false;
    String storedSeed = storedValue.substring(40,50);
    List<int> bytes;
    try { bytes = sha1.convert(latin1.encode(storedSeed+password+storedSeed)).bytes;} catch(err) {
      bytes = new List();
      print('pwCheck:'+err.toString());
    }
    String outStr = hex.encode(bytes)+storedSeed;
    //print("storedSeed:"+storedSeed);
    //print("O:"+outStr);
    //print("I:"+storedValue);
    if (storedValue == outStr) return true;
    else return false;
  }

}
