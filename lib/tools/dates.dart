part of tools;

//Dates-----------
class Dates {
  static DateTime parseDate(dynamic inDate) {
    //Determine input format
    if (inDate is DateTime) return inDate; //Return datetime what is input
    //If integer then return milliseconds since epoch
    if (inDate is int) inDate = new DateTime.fromMicrosecondsSinceEpoch(inDate as int);

    //Parse the string if it's a string
    if (inDate is String) {
      DateTime outDate = null;
      //Return null if no input date
      if ((inDate == null) || (inDate == "") || (inDate == "null")) return null;

      //Split date by space and try to determine if it's bulgarian or english datetime
      List<String> spDate = (inDate as String).split(" ");
      //If first digits are separated with dots and second ones with : then it's bulgarian date d.m.y h:i:s
      if (spDate[0].indexOf(".") > -1) {
        if ((spDate.length == 2) && (spDate[1].indexOf(":") > -1))
          inDate = engDateFull(inDate as String);
        else
          inDate = engDate(inDate as String);
      }

      //Finally parse resulting datetime
      try {
        outDate = DateTime.parse(inDate as String);
      }
      catch (err) {
        outDate = null;
      }

      if ((outDate != null) && (outDate.year < 1900)) outDate = null;
      return outDate;
    }

    return null; //Input is not recognized
  }

  static String validDate(String engDate) {
    if ((engDate != null) && (engDate != "") && (engDate != "null") && (engDate != "-0001-11-30 00:00:00.000"))
      return engDate;
    else return "";
  }

  static String trimDotZero(String time) {
    if (time.contains(".")) time = time.substring(0,time.lastIndexOf("."));
    return time;
  }

  static String trimTime(String date) {
    if (date == null) return '';
    if (date.contains(" ")) date = date.substring(0, date.indexOf(" "));
    return date;
  }

  static List<String> correctDate(List<String> spDate) {
    if (spDate.length < 3) return []; // Not a valid date
    if (spDate[2].length == 2) spDate[2] = "20"+spDate[2]; //add 20..
    if (spDate[1].length == 1) spDate[1] = "0"+spDate[1];
    if (spDate[0].length == 1) spDate[0] = "0"+spDate[0];

    return spDate;
  }

  static String correctTime(String time) {
    time = time.trim()
        .replaceAll(";", ":")
        .replaceAll(".", ":")
        .replaceAll(" ", ":");

    List<String> spTime = time.split(":");
    if (spTime.length < 3) return ""; //Invalid time
    for (int i = 0; i < spTime.length; i++)
      if (spTime[i].length == 1) spTime[i] = "0"+spTime[i];

    return spTime.join(":"); //Return corrected time
  }

  static String bgDateFull(dynamic iengDate) {
    if (iengDate is DateTime) iengDate = iengDate.toString();

    //Return null if datetime type is not matched
    //if (!(iengDate is String)) return null;
    if ((iengDate == null) || (iengDate == "") || (iengDate == "null")) return "";
    List<String> spDate = (iengDate as String).split(" ");
    String time = "";
    try { time = trimDotZero(spDate[1]); } catch(err) { time = ""; }
    String rDate = spDate[0];
    spDate.clear();

    spDate = rDate.split("-");
    String tmp = spDate[2];
    spDate[2] = spDate[0];
    spDate[0] = tmp;
    //Remove quirks
    spDate = correctDate(spDate);

    try { rDate = spDate[0]+"."+spDate[1]+"."+spDate[2]; }
    catch(err) { rDate = ""; }

    if (time != "") rDate += " "+correctTime(time);
    return rDate;
  }

  static String bgDate(dynamic iengDate) {
    String rDate = bgDateFull(iengDate);
    List<String> spDate = rDate.split(" ");
    return spDate[0];
  }

  static String engDateFull(String ibgDate) {
    List<String> spDate = ibgDate.split(" ");
    String time = "";
    try { time = trimDotZero(spDate[1]); } catch(err) { time = ""; }
    String rDate = spDate[0];
    spDate.clear();
    spDate = rDate.split(".");

    //Correct quirks
    spDate = correctDate(spDate);

    try { rDate = spDate[2]+"-"+spDate[1]+"-"+spDate[0]; }
    catch (err) { rDate = ""; }
    if (time != "") rDate += " "+correctTime(time);
    return rDate;

  }

  static String engDate(String ibgDate) {
    String rDate = engDateFull(ibgDate);
    List<String> spDate = rDate.split(" ");
    return spDate[0];
  }

}
