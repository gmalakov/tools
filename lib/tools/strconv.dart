part of tools;

class Cp1251Decoder extends Converter<List<int>, String> {

  String convert(List<int> codes, [int start = 0, int end]) {
    // Allow the implementation to intercept and specialize based on the type
    // of codeUnits.

    int length = codes.length;
    RangeError.checkValidRange(start, end, length);
    if (end == null) end = length;

    List<int> outCodes = new List();
    for (int i=0;i < codes.length;i++) {
      int c = codes[i];
      if (c < 192) outCodes.add(c);
      else
      if (c >= 192)
        outCodes.add(c+848);
    }

    return (new String.fromCharCodes(outCodes));
  }

}

class StrConv {
  static List<String> splitFileName(String fName) {
    int lIndex = fName.lastIndexOf(".");
    String fFirst; String fExt;
    if (lIndex > -1) {
      fFirst = fName.substring(0, lIndex);
      fExt = fName.substring(lIndex+1,fName.length);
    } else { fFirst = fName; fExt = ""; }
    return [fFirst,fExt];
  }

  static String cp2Utf (String cpStr) {
    if (cpStr == null) return "";
    cpStr = cpStr.replaceAll("\\'", "'");
    cpStr = cpStr.replaceAll("\\\"", "\"");

    List<int> codes;
    try { codes = latin1.encode(cpStr); } catch (err) {  codes = cpStr.codeUnits; }

    List<int> outCodes = new List();
    for (int i=0;i < cpStr.length;i++) {
      int c = codes[i];
      if (c < 192) outCodes.add(c);
      else
      if (c >= 192) {
        outCodes.add(c+848);
      }

    }
    return (new String.fromCharCodes(outCodes));
    //return cpStr;
  }


  static String utf2Cp (String cpStr) {
    if (cpStr == null) return "";
    //cpStr = cpStr.replaceAll("'", "\\'");
    //cpStr = cpStr.replaceAll("\"", "\\\"");

    List<int> codes = cpStr.codeUnits;
    List<int> outCodes = new List();

    for (int i=0;i < cpStr.length;i++) {
      int c = codes[i];
      if (c < 192) outCodes.add(c);
      else
      if ((c >= 1040) && (c <= 1103)) outCodes.add(c-848);
    }
    return (new String.fromCharCodes(outCodes));
  }

  static double calcCrc (String str) {
    List<int> ret = sha256.convert(str.codeUnits).bytes;
    double res = 0.0;
    ret.forEach((el) => res += el);
    return res;
  }

  static String sformat(String inStr, List param) {
    List<String> words = inStr.split(" ");
    int cPar = 0;
    for (int i = 0; i < words.length; i++) {
      if (words[i].indexOf("%") == 0) {
        //This is a %xx definition
        // %s -> String .15positions .-15positions
        // %d -> digit  3d -> 3 digits || 03d -> 3 digits leading zero
        // %f -> float  3f -> 3 digits || 03f -> 3 digits leading zero
        // %f -> 03.2f -> 3 digits leading zero 2 digits after dot

        String def = words[i].substring(1);

        if (def.indexOf("s") > -1) {
          //This is string
          int sIndex = def.indexOf("s");
          String addOn = def.substring(sIndex+1);
          def = def.substring(0, sIndex);

          bool startFirst = true;
          if (def.indexOf(".") == 0) def = def.substring(1);//Remove leading dot if any

          if (def.indexOf("-") == 0) {
            startFirst = false; //Start from the end cutting
            def = def.substring(1); //Cut -
          }

          int size = 0;
          //print(def);
          try { size = int.parse(def); } catch (err) {}
          words[i] = upscale(param[cPar].toString(), size, " ", first: startFirst)+addOn;
        } else {
          bool lz = false;

          if (def.substring(0,1) == '0') {
            lz = true;
            def = def.substring(1); //Remove leading zero
          }

          if (def.indexOf("d") > -1) {
            int sIndex = def.indexOf("d");
            String addOn = def.substring(sIndex+1);
            def = def.substring(0, sIndex);

            int size;
            try { size = int.parse(def); } catch (err) { size = 0; }

            words[i] = upscale(param[cPar].toString(), size, lz ? "0": " ")+addOn;
          } else
            if (def.indexOf("f") > -1) {
              int sIndex = def.indexOf("f");
              String addOn = def.substring(sIndex+1);
              def = def.substring(0, sIndex);

              //Check for dot
              List<String> defs =  def.split(".");

              int radix = 2;
              if (defs.length > 1) {
                //We have dot
                try { radix = int.parse(defs[1])+1; } catch (err) { radix = 2; }
               }

              int size;
              try { size = int.parse(defs[0]); } catch (err) { size = 0; }

              //print(radix);
              words[i] = upscale((param[cPar] as num).toStringAsPrecision(radix),size, lz ? "0":" ")+addOn;
            }
         }

        cPar++;
        if (cPar > param.length) throw(new Exception("not enough parameters!"));
      }

    }
    if (cPar != param.length) throw(new Exception("wrong parameter count!"));
    return words.join(" ").replaceAll("\\%", "%");
  }

  static String upscale(String inStr, int size, String fillChar, {bool first : true}) {
    //Reset size to default if zero
    if (size == 0) size = inStr.length;

    //print(inStr+"!"+size.toString()+"!"+fillChar);
    if (size < inStr.length) {
      if (first) inStr = inStr.substring(0, size);
       else inStr = inStr.substring(inStr.length-size);
    } else
      inStr = inStr.padLeft(size, fillChar);

    return inStr;
  }

}

class StrComp {
  static String normalizeStr(String s) =>
      s.replaceAll(new RegExp(r'\s+'), " ").trim();

  static int _levenStein(String s, int len_s, String t, int len_t) {
    int cost;
    if (len_s == 0) return len_t;
    if (len_t == 0) return len_s;
    if (s[len_s-1] == t[len_t-1]) cost = 0;
     else cost = 1;

    List<int> costArr = [_levenStein(s, len_s-1, t, len_t)+1,
    _levenStein(s, len_s, t, len_t-1)+1,
    _levenStein(s, len_s -1 , t, len_t-1)+ cost];

    costArr.sort();

    return costArr[0];
  }

  static int levenStein(String s, String t) =>
    _levenStein(s, s.length, t, t.length);

  static Map<String,int> getTermFMap (List<String> terms) {
    Map<String, int> termFMap = new Map();
    for (String term in terms) {
      int n = termFMap[term];
      n = (n == null) ? 1 : ++n;
      termFMap[term] = n;
    }

    return termFMap;
  }

  static num similarity(String text1, String text2) {
    Map<String, int> a = getTermFMap(normalizeStr(text1).split(" "));
    Map<String, int> b = getTermFMap(normalizeStr(text2).split(" "));

    List<String> interSection = a.keys.toList();
    int i = 0;
    while (i < interSection.length) {
      if (!b.containsKey(interSection[i])) interSection.removeAt(i); //Key is not contained in b -> remove it
       else i++; //Key is contained in b -> move to next element
    }

    num dotProduct = 0; num magnitudeA = 0; num magnitudeB = 0;
    for (String item in interSection)
      dotProduct += a[item] * b[item];

    for (String k in a.keys)
      magnitudeA += pow(a[k], 2);

    for (String k in b.keys)
      magnitudeB += pow(b[k], 2);


    return dotProduct/ sqrt(magnitudeA*magnitudeB);
  }


}