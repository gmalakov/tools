part of tools;

bool compare(dynamic a, dynamic b, [List<String> dif]) {
  if ((a == null) && (b == null)) return true;
  if (a == null) {
    if (dif != null) dif.add("A is null");
    return false;
  }

  if (b == null) {
    if (dif != null) dif.add("B is null");
    return false;
  }

    //Simple data type
    if ((a is int) || (a is num) || (a is String) || (a is DateTime) || (a is bool)) {
      //Rounds numbers for comparsion
      if ((a is num) && (b is num)) {
        if (a.toStringAsPrecision(3) == b.toStringAsPrecision(3))  return true;
         else {
          if (dif != null) dif.add("Simple var а=${a.toStringAsPrecision(3)}, b=${b.toStringAsPrecision(3)}");
          return false;
        }
      }

      if (a == b)
        return true;
      else {
        if (dif != null) dif.add("Simple var а=$a, b=$b");
        return false;
       }
    }

    if (a is List) {
      if (b is! List) {
        if (dif != null) dif.add("a is List, b is not");
        return false;
      }
      //Not the same
      if (a.length != (b as List).length) {
        if (dif != null) dif.add("a and b lists are different lengths.");
        return false;
      }
      for (int i = 0; i < a.length; i++)
        if (!compare(a[i], b[i], dif)) {
          if (dif != null) dif.add("$i ->"+a[i].toString()+"|"+b[i].toString());
          return false;
        }
      return true;
    } //All comparsions are OK then all is OK

    if (a is Map) {
      if (b is! Map) {
        if (dif != null) dif.add("a is Map, b is not");
        return false;
      }//Not the same
   /*   if (a.length != (b as Map).length) {
        if (dif != null) dif.add("a and b maps are different lengths.");
        //Different length maps will be excluded due to null elements acceptance
        return false;
      } //Different size */
      for (dynamic key in a.keys)
        if (!compare(a[key], b[key], dif)) {
          if (dif != null) dif.add("$key ->"+a[key].toString()+"|"+b[key].toString());
          return false;
        }
      return true;
    } //All comparsions are OK then all is OK

    if (dif != null) dif.add("???? a=($a) b=($b)");
    return false;
   //Shouldn't go to there
}

List combine(List inL, {bool recurse : false}) {
  List<dynamic> out = new List<dynamic>();
  inL.forEach((dynamic el) {
    if (el is List) {
      if (recurse) out.addAll(combine(el, recurse:true));
       else out.addAll(el);
    }
    else out.add(el);
  });
  return out;
}
