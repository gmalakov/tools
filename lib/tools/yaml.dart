part of tools;

class Yaml {
  static Map<String, dynamic> readYaml(String inS) {
    inS = inS.replaceAll('\t', '  ');
    final List<String> inArr = inS.split('\n');

    int findNextSection(List<String> inArr, int i, String indent) {
      int j = i;
      while (inArr[j].indexOf(indent) == 0 && j < inArr.length-1) j++;
      return j;
    }

    Map<String,dynamic> readEl(List<String> inArr,[String indent]) {
      indent ??= '';
      //print('"$indent"');
      final Map<String,dynamic> oMap = {};

      for (int i = 0; i < inArr.length; i++) {
        final String el = inArr[i].trim();
        if (!el.startsWith('#')) {
          //print(el);
          if (el.endsWith(':')) {
            //Read subsection
            final String n = el.substring(0, el.length - 1); //Remove :
            //Is list ?
            if (inArr[i + 1].trim().startsWith('-')) {
              //List
              final List<String> l = [];
              i++;
              while (i < inArr.length && inArr[i].trim().startsWith('-')) {
                l.add(inArr[i++].trim().substring(1).trim());
                //Skip eventual commented lines
                while (i < inArr.length && inArr[i].trim().startsWith('#')) i++;
              }
              i--; //run one step back -> will be increased
                                 // on the next for step
              oMap[n] = l;
            } else {
              //no
              final int j = findNextSection(inArr, i + 1,'$indent  ');
              oMap[n] = readEl(inArr.sublist(i + 1, j), '$indent  ');
              //Move to next section
              if (inArr[j].contains('$indent  '))
                i = j;
              else
                i = j - 1;
            }
          } else {
            //Read element
            if (el.isNotEmpty && el.indexOf('#') != 0) {
              final int idx = el.indexOf(':');
              final String k = el.substring(0, idx).trim();
              String v = el.substring(idx + 1).trim();

              if (v.contains('"') || v.contains("'")) {
                //it's string
                v = v.replaceAll('"', '').replaceAll("'", '');
                oMap[k] = v; //Put into map
                //print(vals[1]);
              } else {
                //Boolean
                if (v.toLowerCase() == 'true') oMap[k] = true;
                if (v.toLowerCase() == 'false') oMap[k] = false;
                if (v.toLowerCase() == 'null') oMap[k] = null;

                //Try to read number
                try {
                  //Integer
                  final RegExp intReg = new RegExp(r'^[0-9]$');
                  if (intReg.hasMatch(v))
                    oMap[k] = int.parse(v);

                  //float
                  final RegExp flReg = new RegExp(r'^\d+$');
                  if (flReg.hasMatch(v)) oMap[k] = num.parse(v);
                } catch (err) {}

                //Not recognized -> it's string
                if (!oMap.containsKey(k)) oMap[k] = v;
              }
            }
          }
        }
      }
      return oMap;
    }

    return readEl(inArr);
  }

  static String writeYaml(Map<String,dynamic> inMap, [String indent='']) {
    final res = new StringBuffer();
    for (var k in inMap.keys) {
      res
        ..write(indent)
        ..write(k.toString())
        ..write(':');

      if (inMap[k] is Map) res
        ..write('\n')
        ..write(writeYaml(inMap[k],'$indent  '));
      else if (inMap[k] is List) {
        final List l = inMap[k];
        res.write('\n');
        l.forEach((el) => res
          ..write(indent)
          ..write('- ')
          ..write(el.toString().trim())
          ..write('\n'));
      } else {
        String cRes = inMap[k].toString();
        if (inMap[k] is String) cRes = '"$cRes"';
        res
          ..write(cRes)
          ..write('\n');
      }
    }
    return res.toString();
  }

  static Map<String,dynamic> decode(String yaml) =>
      readYaml(yaml);
  static String encode(Map inMap) =>
      writeYaml(inMap.cast<String,dynamic>());
}
