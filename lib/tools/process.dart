part of tools;

//Simple debouncer
class DeBounce {
  Duration dur;
  Function _exec;
  Timer _timer;

  DeBounce(this._exec, {this.dur : const Duration(seconds:1)});
  void exec([dynamic val]) {
    if (_exec == null) return; //Exit if no function specified

    if (_timer != null) _timer.cancel(); //Reset timer if needed
    _timer = new Timer(dur, () {
      _exec(val); //Execute command
      _timer.cancel(); _timer = null; //Reset timer
    });
  }
}

class ProcessDescr {
  final Function _toDo;
  Function callBack;

  Function get toDo => _toDo;

  ProcessDescr(this._toDo, this.callBack);
}

//Process flow control
class ProcessControl {
  int _maxJobs = 1;
  int _curJobs = 0;
  Queue<ProcessDescr> _queue = new Queue();

  ProcessControl(this._maxJobs);

  void execCmd() {
    if ((_queue.length > 0) && (_curJobs < _maxJobs)) {
      ProcessDescr pd = _queue.last;
      _queue.removeLast();

      _curJobs++;

      //Function is to be executed
      if (pd.toDo != null)
        pd.toDo().then((dynamic _) {
          //print('new job started');
          if (pd.callBack != null) pd.callBack();
          if (_curJobs > 0) _curJobs--; //Decrease current jobs on ready. Shouldn't be below zero
          if (_queue.length > 0) execCmd();
        });
    }
  }

  void addQueue(Function toDo, [Function callBack]) {
    _queue.addFirst(new ProcessDescr(toDo, callBack));
    //Execute command on queue add if not executing now
    //print('call command');
    execCmd();
  }

  Future<bool> finishJobs() {
    Completer<bool> comp = new Completer();

    ProcessDescr lJob = _queue.first;
    Function cB = lJob.callBack;
    if (cB != null) lJob.callBack = (() {
         cB();
         comp.complete(true);
       });
     else lJob.callBack = (() {
         comp.complete(true);
       });

    return comp.future;
  }

}

//Retry function
typedef Future<Null> FFunc();

class RetryFunction {
  static int retries = 3; //By default we have three retrie times;
  int _retLeft = retries;
  Function log;

  Duration retryTime;
  Duration retryTimeLeft;

  int retryFactor;
  FFunc _exec;

  RetryFunction(this._exec, {this.retryTime = const Duration(seconds:10),
    this.retryFactor = 1}) {

    //Set retry times that are left to maximum
    retryTimeLeft = retryTime;
    //Start execution
    exec();
  }

  Future<Null> exec() {
    if (_exec == null) return null;
    Completer comp = new Completer<dynamic>();
    _exec().then((dynamic val) => comp.complete(val))
        .catchError((dynamic err) {
      if (log != null) log(err.toString());
      new Timer(retryTimeLeft, () async {
        _retLeft--;
        retryTimeLeft = retryTime*retryFactor;
        //Self call after timeout if there is an error and more retries are left
        if (_retLeft >= 0) comp.complete(await exec());
        else //If retries are allready done
          comp.completeError(err); //Return error that has occured
      });
    });

    return comp.future; //Return completer's future
  }

}
