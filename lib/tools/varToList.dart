part of tools;

class BinTools {
  static const binHdr = "B1";
  static const String $rtype = '\$runtimeType';
  static const String $value = '\$value';

  static final Map<String, Function()> _registry = {};
  static void registerObject(String rType, Function() f) =>
    _registry[rType] = f;

  static List<int> _toBytes(int i) {
    List<int> obytes = new List();
    while (i > 0) {
      obytes.add(i & 0xFF);
      i = i >> 8;
    }

    return obytes;
  }

  static int _fromBytes(List<int> bytes) {
    int out = 0;
    for (int i = bytes.length-1; i >= 0; i--)
      out = (out << 8) | bytes[i];
    return out;
  }

  static List<int> varToList(dynamic val, [List<int> retList]) {
    //Start from the beggining
    if (retList == null) {
      retList = new List()
        ..addAll(binHdr.codeUnits);
    }


    int type;
    if ((val is num) || (val is double)) type = 1;
    if (val is int) type = 0;
    if (val is DateTime) {
      type = 5;
      val = (val as DateTime).millisecondsSinceEpoch;
    }

    if (val is String) type = 2; //String
    if (val is List) type = 3; // Ordinary list
    if (val is Map) type = 4; //Map
    if (val is Uint8List) type = 2; //Binary String

    //Encode boolean
    if (val is bool) {
      type = 6;
      if (val as bool) val = 1;
      else val = 0;
      retList.add((type << 5) | (val as int));
    }

    if (val == null) { type = 7; val = 0; } //Null type

    //Value is not recognized then try to perform toMap. Or encode the error like string
    if (type == null) {
      try {
        type = 4;
        //Encode as reverse decodable object
        if (_registry.containsKey(val.runtimeType.toString()))
          val = { $rtype : val.runtimeType, $value: val.toMap() };
         else
          val = val.toMap();
      } catch (err) {
        val = val.toString(); type = 2;
      }
    }

      int iSize = 0;
      if ((type == 0) || (type == 5) || (type == 7)) {
        bool neg = false;
        if ((val as int) < 0) {
          neg = true;
          val = -val;
        }
        if (type != 7) {
          List<int> bt = _toBytes(val as int);
          iSize = bt.length;
          iSize = ((neg) ? 0x10 : 00) | (iSize & 0x0F);
          retList.add((type << 5) | (iSize & 0x1F)); //Add types and size
          retList.addAll(bt); //Add value
        } else  retList.add(type << 5); //Add type only
      }

      if (type == 1) {
        bool neg = false;
        if ((val as num) < 0) {
          neg = true;
          val = -val;
        }

        int ctr = 0;
        double valRnd = (val as num).toDouble();
        while ((valRnd % 1) > 0) {
          valRnd = valRnd * 10;
          ctr++;
        }

        int intRnd = valRnd.floor(); //Should be the same as valRnd
        ctr = ((neg) ? 0x10 : 00) | (ctr & 0x0F);
        retList.add((type << 5) | (ctr & 0x1F)); //type and decimal counter
        List<int> bt = _toBytes(intRnd);
        retList.add(bt.length); //Add int length
        retList.addAll(bt); //Add bytedata
      }

      if (type == 2) { //String or binary string List<byte>
        if (val is String) {
          List<int> strLst = utf8.encode(val);
          retList.add((type << 5) | 0x00); //Only type for a string
          retList.addAll(strLst);
          retList.add(0); //Add zero at the end of the string
        }

        if (val is Uint8List) {
          List<int> bt = _toBytes(val.length);
          int iSize = bt.length;

          retList.add((type << 5) | (iSize & 0x1F)); //Add int size
          retList.addAll(bt); //Add list size

          retList.addAll(val.toList()); //Add values
        }
      }

      if (type == 3) {
        int lSize = (val as List).length;
        List<int> bt = _toBytes(lSize);
        int iSize = bt.length;

        retList.add((type << 5) | (iSize & 0x1F)); //Only type for a list
        retList.addAll(bt);

        for (dynamic n in (val as List))
          retList = varToList(n, retList); //Add values to list
      }

      if (type == 4) {
        int mSize = (val as Map).length;
        List<int> bt = _toBytes(mSize);
        int iSize = bt.length;

        retList.add((type << 5) | (iSize & 0x1F)); //Only type for a list
        retList.addAll(bt);

        for (dynamic key in (val as Map).keys) {
          retList = varToList(key, retList); //Add key
          retList = varToList(val[key], retList); //Add value
        }

      }

      return retList;
  }

  static List<dynamic> _listToVar(List<int> inList, [ int offset = 0]) {
    if (offset >= inList.length) {
        print('unexpected end of input offset:$offset, len:${inList.length}');
        return null;
      }
      if (offset == 0) {
        //Just starting
        List<int> beg = inList.getRange(0, binHdr.length).toList();
        if (new String.fromCharCodes(beg) != binHdr) return <dynamic>[null,0]; //Not out binary format
        offset = binHdr.length;
      }

      int val = inList[offset];
      offset++;
      int type = val >> 5; //Value type
      int len = val & 0x1F; //Integer length or 00

      //Null type
      if (type == 7) return <dynamic>[null, offset];

      if ((type == 0) || (type == 5)) {
        bool neg = false;
        //Integer value
        neg = ((len & 0x10) == 0x10); //5tith bit is set ???
        len = (len & 0x0F); //Remove bit
        List<int> bytes = inList.getRange(offset, offset+len).toList();
        offset += len; //Correct offset
        int ret = _fromBytes(bytes);
        //Return datetime if datetime is expected
        if (type == 5) return <dynamic>[new DateTime.fromMillisecondsSinceEpoch(ret), offset];
        if (neg) ret = -ret;
        //print(ret.toString()+' '+neg.toString()+" "+(len & 0x10).toString());
        return <dynamic>[ret, offset];
      }

      if (type == 1) {
        bool neg = false;
        neg = ((len & 0x10) == 0x10); //5tith bit is set ???
        len = (len & 0x0F); //Remove bit

        int dec = len; //Decimal symbols
        len = inList[offset]; //Integer length
        offset++;
        int intRnd = _fromBytes(inList.getRange(offset, offset+len).toList());
        offset += len;

        double retVal = intRnd.toDouble();
        for (int i = 0; i < dec; i++)
          retVal = retVal / 10;

        if (neg) retVal = -retVal;
        return <dynamic>[retVal, offset];
      }

      if (type == 2) {
        if (len == 0) {
          //If string asciiz (len must be zero)
          List<int> str = new List();
          while (inList[offset] != 0) {
            str.add(inList[offset]);
            offset++; //Correct offset
          }
          offset++; //Correct offset for zero
          return <dynamic>[utf8.decode(str, allowMalformed: true), offset
          ]; //Return string
        } else {
          //List decoding (uint8list -> binary string
          List<int> retList = [];
          List<int> bt  = inList.getRange(offset, offset+len).toList();
          offset += len; //Correct offset
          int isize = _fromBytes(bt);

          retList.addAll(inList.getRange(offset, offset+isize));
          offset += isize;

          return <dynamic>[new Uint8List.fromList(retList), offset];
        }
      }

      if (type == 3) {
        //List
        List<dynamic> retList = [];
        List<int> bt = inList.getRange(offset, offset+len).toList();
        offset += len; //Correct offset
        int size = _fromBytes(bt);

        for (int i = 0; i < size; i++) {
          List<dynamic> res = _listToVar(inList,offset);
          if (res != null) {
            retList.add(res[0]);
            offset = res[1] as int;
          } else print(retList);
        }

        return <dynamic>[retList, offset];
      }

      if (type == 4) {
        //Map
        Map<dynamic,dynamic> retMap = new Map<dynamic, dynamic> ();
        List<int> bt = inList.getRange(offset, offset+len).toList();
        offset += len; //Correct offset
        int size = _fromBytes(bt);

        for (int i = 0; i < size; i++) {
          List<dynamic> res = _listToVar(inList,offset);
          if (res != null) {
            dynamic key = res[0]; //Read key
            offset = res[1] as int;
            res = _listToVar(inList, offset); //Read value
            if (res != null) {
              dynamic val = res[0];
              offset = res[1] as int;
              retMap[key] = val; //Settle into map
            } else print(retMap);
          }
        }

        //Try to decode encoded object
        if (retMap.containsKey($rtype) &&
            _registry.containsKey(retMap[$rtype])) try {
          final obj = _registry[retMap[$rtype]](); //create the object
          obj.fromMap(retMap[$value]); //settle init data
          return <dynamic>[obj, offset];
        } catch(err) {};

        return <dynamic>[retMap, offset];
      }

    //Decode boolean
    if (type == 6)
      if (len == 1) return <dynamic>[true,offset];
      else return <dynamic>[false, offset];

      return null;
    } //Shouldn't end up here nothing is read

  static dynamic listToVar(List<int> bytes) {
    if (bytes == null) return null; //Nothing is accepted
    return _listToVar(bytes)[0];
  }

  static List<int> _unreadable(List<int> bytes) {
    //List<int> oList = new List(bytes.length); //oList fixed length list
    int half = bytes.length ~/ 2;
    int len = bytes.length-1;

    void halfBytes() {
      for (int i = 0; i < half; i++) {
        int ul = bytes[i] >> 5;
        int ll = bytes[i] & 0x1F;

        int ur = bytes[len - i] & 0xF8; //Wont' be shifting
        int lr = bytes[len - i] & 0x07;

        bytes[i] = (lr << 5) | ll; //Lower right becomes upper left
        bytes[len - i] = ur | ul; //Upper Left becomes lower right
      }
    }

    void wholeBytes() {
      for (int i = 0; i < half; i++) {
        int tmp = bytes[i];
        bytes[i] = bytes[half + i];
        bytes[half + i] = tmp;
      }
    }

    if (compare(bytes.getRange(0, binHdr.length).toList(), binHdr.codeUnits)) { //Not encrypted
      halfBytes(); //First swap half bytes
      wholeBytes(); //then swap whole bytes
    } else {
      //It's encrypted -> we need to do things in the opposite direction
      wholeBytes();
      halfBytes();
    }

    return bytes;
  }

  static List<int> encode(dynamic val, {bool unreadable : false}) {
    if (unreadable) return _unreadable(varToList(val));
    else return varToList(val);
  }

  static dynamic decode(List<int> inList, {bool unreadable : false}) {
    if (unreadable) return _listToVar(_unreadable(inList))[0];
    else return _listToVar(inList)[0];
  }
/*
  header - BIN
  0 - int
  ---  11110000 -> first 3 bits for number are flags
  1 - num
  2 - string
  3 - list
  4 - map
  5 - datetime
  6 - boolean
  7 - null type
  ---  -> second 5 bits for number are length

  data itself -> number or string
  string is ASCIIZ

  Map -> id -> value
         id -> value

  List -> value, value, ....

  ---

 */
}
