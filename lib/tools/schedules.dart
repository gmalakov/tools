part of tools;

//Executes exec until list is empty
class DelayExec {
  Duration interval;
  //Future exec(par)
  Function exec;
  List param;
  int ctr = 0;
  List results = new List<dynamic>();
  Completer comp = new Completer<List>();

  DelayExec(this.interval, this.exec, this.param, DateTime startDate) {
    DateTime now = new DateTime.now();
    Duration dur = startDate.difference(now);
    if (dur.inSeconds > 0) _schedule(dur);
    else _schedule(null);
  }

  void _execProc() {
    if (exec != null) exec(param[ctr]).then((dynamic res) {
      ctr++;
      results.add(res);

      //Schedule next
      if (param.length > ctr) _schedule(interval);
      else comp.complete(results);
    });


  }

  Future<List> execute() {
    return (comp.future as Future<List>);
  }


  void _schedule(Duration dur) {
    if (dur != null) {
      Timer tim;
      tim = new Timer(dur, () {
        tim.cancel();
        _execProc();

      });
    } else _execProc();
  }
}

enum SchedType { Hourly, Daily, Weekly, Monthly, Yearly, None }

class Schedule {
  static int cId = 0;

  SchedType sched;
  Function callBack;
  bool weekends = true;
  DateTime _startDate, _endDate, _stepDate;
  int id;
  Map<String, dynamic> param;

  DateTime get startDate => (_startDate != null) ? _startDate.toLocal() : null;
  DateTime get stepDate => (_stepDate != null) ? _stepDate.toLocal() : null;
  DateTime get endDate => (_endDate != null) ? _endDate.toLocal() : null;

  static const List<int> months = const [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
  static const Map<int,String> schTypes = const {0:"Hourly", 1:"Daily", 2:"Weekly", 3:"Monthly", 4:"Yearly", 5:"None" };

  Timer _tim;

  Map toMap() {
    Map schedule = new Map<String, dynamic>();
    if (weekends == null) weekends = true;
    //assign next id if null
    if (id == null) {
      cId ++; id = cId;
    }

    schedule["id"] = id;
    schedule["schType"] = schTypes[sched.index];
    schedule["weekends"] = weekends;
    if (_startDate != null) schedule["startDate"] = _startDate.toLocal().toString();
    if (_endDate != null) schedule["endDate"] = _endDate.toLocal().toString();
    schedule["param"] = param;

    return schedule;
  }

  Map toJson() => toMap();

  String get when {
    return json.encode(toMap());
  }

  void set when(String sSchedule) {
    Map schedule;
    //exit on error decoding json
    try { schedule = json.decode(sSchedule) as Map; } catch (err) { return; }
    fromMap(schedule);
  }

  void fromMap(Map schedule) {
    //Decode parameters
    for (int i = 0; i < schTypes.length; i ++)
      if (schedule["schType"] == schTypes[i]) sched = SchedType.values[i];

    id = schedule["id"] as int;
    weekends = schedule["weekends"] as bool;
    _startDate = Dates.parseDate(schedule["startDate"] as String).toUtc();
    _endDate = Dates.parseDate(schedule["endDate"] as String).toUtc();
    param = schedule["param"] as Map<String, dynamic>;

    //Set first schedule
    setSchedule();
  }


  //Return scheduled date/times for the calendar
  List<DateTime> getSchedules(DateTime sDate, DateTime eDate) {
    sDate = sDate.toUtc();
    eDate = eDate.toUtc();

    List<DateTime> dList = new List();
    //Exit if endDate is smaller than startDate
    if ((sDate == null) || (eDate == null)) return dList;
    if (eDate.isBefore(sDate)) return dList;
    DateTime cDate = sDate;

    if (sched != SchedType.None)
      do  {
        //Do not add step which is after end date
        cDate = nextSched(cDate);
        if (cDate.isBefore(eDate)) dList.add(cDate);
      } while (cDate.isBefore(eDate));
    else dList.add(sDate);

    return dList;
  }

  DateTime _weekendCorr(DateTime cDate) {
    //Default weekends flag = true
    if (weekends == null) weekends = true;
    if (!weekends) {
      if (cDate.weekday == 6) cDate = cDate.add(const Duration(days:2));
      if (cDate.weekday == 7) cDate = cDate.add(const Duration(days:1));
    }
    return cDate;
  }

  DateTime nextSched(DateTime cDate) {

    cDate = cDate.toUtc();
    DateTime now = new DateTime.now().toUtc();

    //if current step is before start date current step = startDate
    if ((cDate == null) || (cDate.isBefore(_startDate))) cDate = _startDate;

    //if now is before stepDate return stepDate
    if (now.isBefore(cDate)) {
      //Correct if set to run on weekend and weekend flag is false
      if (sched == SchedType.Daily) cDate = _weekendCorr(cDate);
      return cDate;
    }

    switch (sched) {
      case SchedType.Hourly:
        cDate = cDate.add(const Duration(hours:1));
        break;
      case SchedType.Daily:
        cDate = cDate.add(const Duration(days:1));
        //If not scheduled to run on weekends skip them
        cDate = _weekendCorr(cDate);
        break;
      case SchedType.Weekly:
        cDate = cDate.add(const Duration(days:7));
        break;
      case SchedType.Monthly:
        if (cDate.month < 12) cDate = new DateTime(cDate.year, cDate.month+1, cDate.day,
        cDate.hour, cDate.minute, cDate.second);
        else cDate = new DateTime(cDate.year+1, 1, cDate.day, cDate.hour, cDate.minute, cDate.second);
        break;
      case SchedType.Yearly: cDate = new DateTime(cDate.year+1, cDate.month, cDate.day,
      cDate.hour, cDate.minute, cDate.second);
      break;
      case SchedType.None: cDate = null;
      break;
    }

    return cDate;
  }

  Duration calcNext() {
    Duration dur;
    _stepDate = nextSched(_stepDate);
    dur = _stepDate.difference(new DateTime.now().toUtc());
    return dur;
  }

  void _ourCallBack() {
    if (callBack == null) return;
    if (param == null) callBack();
    else callBack(param);
  }

  void setSchedule() {
    //Remove early schedules
    try {
      if (_tim != null) _tim.cancel();
      _tim = null;
    } catch (err) {}

    //if stepDate is not defined get last step date before now (Works on Monthly/Yearly)
    if ((_stepDate == null) &&
    (_startDate != null)) {
      List<DateTime> gS = getSchedules (_startDate, new DateTime.now().toUtc());
      if (gS.length > 0) _stepDate = gS.last;
      else _stepDate = _startDate;
    }

    Duration dur = calcNext();
    //Exit without next schedule if schedule type is none! (only on first date calc will return non null
    if (dur == null) return;
    DateTime tDate = new DateTime.now().toUtc();
    tDate = tDate.add(dur); //Add calculated duration
    //Exit without next schedule if endDate is reached!
    if ((_endDate != null) && (_endDate.difference(tDate).inSeconds < 0)) return;

    _tim = new Timer(dur, () {
      _ourCallBack();
      //Set next schedule after starting callBack
      new Timer(const Duration(seconds:1), () => setSchedule()); //delay next sechedule to avoid cycling
    });
  }

  Schedule(Map schedule, this.callBack) {
    fromMap(schedule);
  }

  Schedule.json(String jSchedule, this.callBack) {
    when = jSchedule;
  }

  Schedule.hourly(DateTime this._startDate, DateTime this._endDate, this.callBack, [this.param]) {
    if (_startDate != null) _startDate = _startDate.toUtc();
    if (_endDate != null) _endDate = _endDate.toUtc();
    this.sched = SchedType.Hourly;
    setSchedule();
  }
  Schedule.daily(DateTime this._startDate, DateTime this._endDate, this.callBack, [this.weekends, this.param]) {
    if (_startDate != null) _startDate = _startDate.toUtc();
    if (_endDate != null) _endDate = _endDate.toUtc();
    this.sched = SchedType.Daily;
    setSchedule();
  }
  Schedule.weekly(DateTime this._startDate, DateTime this._endDate, this.callBack, [this.param]) {
    if (_startDate != null) _startDate = _startDate.toUtc();
    if (_endDate != null) _endDate = _endDate.toUtc();
    this.sched = SchedType.Weekly;
    setSchedule();
  }
  Schedule.monthly(DateTime this._startDate, DateTime this._endDate, this.callBack, [this.param]) {
    if (_startDate != null) _startDate = _startDate.toUtc();
    if (_endDate != null) _endDate = _endDate.toUtc();
    this.sched = SchedType.Monthly;
    setSchedule();
  }

  Schedule.Yearly(DateTime this._startDate, DateTime this._endDate, this.callBack, [this.param]) {
    if (_startDate != null) _startDate = _startDate.toUtc();
    if (_endDate != null) _endDate = _endDate.toUtc();
    this.sched = SchedType.Yearly;
    setSchedule();
  }

  Schedule.startAt(DateTime this._startDate, DateTime this._endDate, this.sched, this.callBack, [this.weekends, this.param]) {
    if (_startDate != null) _startDate = _startDate.toUtc();
    if (_endDate != null) _endDate = _endDate.toUtc();
    if (weekends == null) weekends = true;
    //mon = sch.month;

    setSchedule();
  }

  Schedule.executeAt(DateTime sch, this.callBack) {
    Duration dur = sch.difference(new DateTime.now());
    _tim = new Timer(dur, () {
      _ourCallBack();
      _tim.cancel();
    });
  }


  void remove() {
    if (_tim != null)
      try { _tim.cancel(); _tim = null; } catch(err) {}
  }

}
