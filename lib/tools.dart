library tools;

import 'dart:async';
import 'dart:math';
import 'dart:convert';
import 'dart:collection';
import 'dart:typed_data';
import 'package:crypto/crypto.dart';
import 'package:convert/convert.dart';
//import 'package:uuid/uuid.dart';

part 'tools/strconv.dart';
part 'tools/schedules.dart';
part 'tools/dates.dart';
part 'tools/pass.dart';
part 'tools/process.dart';
part 'tools/compare.dart';
part 'tools/varToList.dart';
part 'tools/yaml.dart';

typedef ErrFunc = FutureOr<void> Function(String);

