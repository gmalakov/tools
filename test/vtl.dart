import 'package:tools/tools.dart';
import 'dart:io';
import 'dart:async';
import 'dart:typed_data';

class GenClass {
  int a;
  int b;
  String str;
  GenClass(this.a, this.b, this.str);

  Map<String, dynamic> toMap() => {
    'a': a,
    'b': b,
    'str':str
  };

  void fromMap(Map inMap) {
    a = inMap['a'];
    b = inMap['b'];
    str = inMap['str'];
  }

  String toString() => '$runtimeType ${toMap()}';
}

Future<dynamic> main() async {
  final r = new GenClass(5, 15, 'test string');
  BinTools.registerObject(r.runtimeType.toString(),
          () => new GenClass(null,null,null));

  List<int> res = BinTools.encode({"key": "Бай иван", 10 : r,
    "20": [-1, -100, -2000, 23.232,23.343,32, 2312, 12121.23213, 0.00002,'hello worlds'],
    34:new DateTime.now(), 'alabala Portokala': null,
  'uint8': new Uint8List.fromList([1,2,3,4,5,6,7,8,9,10])}, unreadable: true);
  await new File('test.dat').writeAsBytes(res);
  List<int> myRes = await new File('test.dat').readAsBytes();
  final bt = BinTools.decode(myRes, unreadable: true);
  print(bt);

  await sleep(const Duration(seconds: 30));
  await new File('test.dat').delete();
}