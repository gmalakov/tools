import "package:tools/conf_reader.dart";
import "dart:async";

Future<Null> main() async {
/*  DHCPConf d = new DHCPConf("dhcpd.conf");
  if (await d.readFile()) {
    SubNet sub =  d.networks["eth0"].subNets["192.168.0.0"];
    sub.delHost("vasileva");
    sub.addHost(new Host("myhost", sub.firstIp(), "a4:34:21:34:af:ff")
     ..comment = "new host added here");
    print (sub.availableIps());

    sub = d.networks["eth0"].subNets["192.168.5.0"];
    sub.addHost(new Host("host2", sub.firstIp(),"aa:22:16:33:66:77")
     ..comment = "test1\ndsakdjaskjdka\ntest3");
    //print(sub);
//    print (sub.firstIp());
    print (sub.availableIps());

     await d.writeFile('out.conf');
     //print(d.params.toString());
  } else print ("error reading config!"); */

/*
  var sh = new SHReader('ips.sh');
  await sh.readFile();
  sh.vars["id"] = 321;
  sh.vars["controlips"][2] = "10.10.10.10";
  await sh.writeFile("ipo.sh"); */

  var smb = new SMBReader('smb.conf');
  await smb.readFile();
  smb.sections.forEach((name, m) {
    print("Section -> $name");
    if (m["hosts allow"] != null) print(m["hosts allow"]);
  });
  smb.writeFile('osmb.conf');
}