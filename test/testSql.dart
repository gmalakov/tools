import 'dart:async';
import '../lib/sql_tools.dart';

Future<Null> main() async {
  Map<String, dynamic> data = <String, dynamic> {
    "id": 0,
    "name": "Бай иван е на български",
    "created": new DateTime.now(),
    "test": 124.43,
    "js": { "a": 123, "b": 434 },
    "ls": [1,2,3,4]
  };

  QExecutor.vars = <String, dynamic>{
/*    "pgreUser": "postgres",
    "pgrePass": "postgres",
    "pgreHost": "localhost", */
    "pgreUser": "fly",
    "pgrePass": "maniac1234",
    "pgreHost": "192.168.0.252",
    "pgrePort": 5432,
    "pgreDb": "testdb",
    "sqluser": "root",
    "sqlpass": "Intruder",
    "sqlport": 3306,
    "sqldb": "testdb",
    "sqlhost": "localhost"
  };

  var sqType = SqlType.postgre;
  //Both executor and mapper must be set to postgres
  QExecutor.sqlDriver = sqType;
  GenMapper.parseDates = true;
  QCreator.noConv = true; //No conversion to CP1251!

  var tb = new TableBuilder(data, "id", "test", true, keys: "created", sqType: sqType);


  await QExecutor.instance.query(tb.toString(), null);
  //print(tb.toString());
  DefMapper dm = tb.getMapper();
  print(await dm.saveData(data));
  List<Map> rData = await dm.findAll();
  for (int i = 0; i < rData.length; i++)
    print(rData[i].toString()+"\n");
  await QExecutor.instance.remove(); //Close all connections

  //Mysql try

  sqType = SqlType.mysql;
  //Both executor and mapper must be set to postgres
  QExecutor.sqlDriver = sqType;
  QExecutor.reset();
  GenMapper.parseDates = true;
  QCreator.noConv = true; //No conversion to CP1251!

  tb = new TableBuilder(data, "id", "test", true, keys: "created", sqType: sqType);

  await QExecutor.instance.query(tb.toString(), null);
  dm = tb.getMapper();
  print(await dm.saveData(data));
  rData = await dm.findAll();
  for (int i = 0; i < rData.length; i++)
    print(rData[i].toString()+"\n");
  await QExecutor.instance.remove(); //Close all connections

}
