import 'dart:async';
import 'dart:convert';
import 'dart:io';
import '../lib/tools.dart';

Future main() async {
  String s = await new File('analysis_options.yaml').readAsString();
  Map yml = Yaml.readYaml(s);
  print(json.encode(yml));
  print(Yaml.writeYaml(yml));
}