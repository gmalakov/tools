import 'package:tools/tools.dart';

void main() {
  Schedule sch;
  DateTime now = new DateTime.now();

  sch = new Schedule.hourly(now.subtract(const Duration(seconds:20)), null, () {
    print ("called hourly!  stepDate="+sch.stepDate.toString());
  });
  print ("hourly: stepDate="+sch.stepDate.toString());
  print (sch.when);

  sch = new Schedule.daily(now.subtract(const Duration(seconds:20)), null, () {
    print ("called daily!  stepDate="+sch.stepDate.toString());
  }, false);
  print ("daily: stepDate="+sch.stepDate.toString());
  print (sch.when);

  sch = new Schedule.startAt(DateTime.parse("2015-03-29 12:15:00"), null, SchedType.Monthly, (Map par) {
    print ("called! stepDate="+sch.stepDate.toString());
    print(par.toString());
  }, true, { "test1":1, "test2": 2 }); // ignore: strong_mode_implicit_dynamic_map_literal

  print ("Monthly: stepDate="+sch.stepDate.toString());
  print (sch.when);
}